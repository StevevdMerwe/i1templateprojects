/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.parser;

import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionGroupComponentType;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenScreenComponent;
import com.omussd.generic.db.entity.ScreenType;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.entity.TextComponent;
import com.omussd.generic.service.PropertyService;
import com.omussd.generic.service.ScreenComponentService;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.bean.DynamicIntent;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.ussdintegration.request.constant.ResponseState;
import com.omussd.ussdintegration.request.constant.ResponseType;
import com.omussd.ussdintegration.response.USSDAppResponse;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ScreenComponentService.class, PropertyService.class})
public class ScreenParserTest {

    private Session session;
    private Screen screen;

    @Autowired
    private ScreenParser screenParser;

    @MockBean
    private ScreenComponentService screenComponentService;
    @MockBean
    private PropertyService propertyService;

    @TestConfiguration
    static class ScreenParseConfig {

        @Bean
        ScreenParser getScreenParser() {
            return new ScreenParser();
        }
    }

    /**
     * Test of getAppResponseFromScreen method, of class ScreenParser.
     */
    @Test
    public void testGetAppResponseFromScreen() throws Exception {
        SessionData sessionData = new SessionData();
        List<DynamicOptionGroup> dogs = new ArrayList<>();
        dogs.add(getDynamicOptionGroup());
        sessionData.setDynamicOptionGroups(dogs);
        sessionData.addTextVariable("var", "VARIABLE");

        session = EntityIdBuilder.getEntity(Session.class, 1);
        session.setMsisdn("MSISDN");

        OptionOptionGroupComponent oogc = getOptionOptionGroupComponent();
        List<OptionOptionGroupComponent> oogcs = new ArrayList<>();
        oogcs.add(oogc);

        ScreenScreenComponent sscText = EntityIdBuilder.getEntity(ScreenScreenComponent.class, 1);
        TextComponent scText = EntityIdBuilder.getEntity(TextComponent.class, 1, true);
        scText.setName("scText");
        scText.setText("TEXT_COMPONENT ${var}");
        sscText.setScreenComponentId(scText);
        sscText.setSequence(1);

        ScreenScreenComponent sscGroup = EntityIdBuilder.getEntity(ScreenScreenComponent.class, 2);
        sscGroup.setScreenComponentId(oogc.getOptionGroupComponentId());
        sscGroup.setSequence(2);

        ScreenScreenComponent sscDyn = EntityIdBuilder.getEntity(ScreenScreenComponent.class, 3);
        sscDyn.setScreenComponentId(getDynOptionGroupComponent());
        sscDyn.setSequence(3);

        List<ScreenScreenComponent> sscs = new ArrayList<>();
        sscs.add(sscText);
        sscs.add(sscGroup);
        sscs.add(sscDyn);

        ScreenType screenType = EntityIdBuilder.getEntity(ScreenType.class, 1);
        screenType.setCode(ScreenTypeConstant.ACTION.getCode());
        screen = EntityIdBuilder.getEntity(Screen.class, 1);
        screen.setScreenTypeId(screenType);

        SessionDataCache.INSTANCE.put(session.getMsisdn(), sessionData);
        Mockito.when(screenComponentService.findOptionOptionGroupComponentsByOptionGroupComponent(Mockito.any(OptionGroupComponent.class))).thenReturn(oogcs);
        Mockito.when(screenComponentService.findScreenScreenComponentByScreenIdOrderBySequence(screen)).thenReturn(sscs);
        Mockito.when(propertyService.findValueByName(Mockito.anyString())).thenReturn(". ");

        USSDAppResponse expected = new USSDAppResponse();
        expected.setResponseType(ResponseType.String);
        expected.setState(ResponseState.NextState);
        expected.setPrompt("TEXT_COMPONENT VARIABLE\n\n1. TEXT\n\n2. TEST_TEXT\n3. TEST_TEXT_2");

        USSDAppResponse result = screenParser.getAppResponseFromScreen(session, screen);
        Assert.assertEquals(expected, result);
    }

    private DynamicOptionGroup getDynamicOptionGroup() {
        DynamicOptionGroup dog = new DynamicOptionGroup();
        dog.setName("TEST_DOG");
        dog.setParameter("TEST_PARAMETER");

        List<DynamicIntent> dynamicIntents = new ArrayList<>();
        DynamicIntent dynamicIntent = new DynamicIntent();
        dynamicIntent.setDestination("TEST_DESTINATION");
        dynamicIntent.setPurpose("TEST_PURPOSE");
        dynamicIntent.setIntentType(IntentTypeConstant.SUBMIT.getIntentType());
        dynamicIntent.setValue("TEST_VALUE");
        dynamicIntents.add(dynamicIntent);

        List<DynamicOption> dynamicOptions = new ArrayList<>();
        DynamicOption dyno = new DynamicOption();
        dyno.setNumber(2);
        dyno.setSequence(1);
        dyno.setText("TEST_TEXT");
        dyno.setValue("TEST_VALUE");

        DynamicOption dyno2 = new DynamicOption();
        dyno2.setNumber(3);
        dyno2.setSequence(2);
        dyno2.setText("TEST_TEXT_2");
        dyno2.setValue("TEST_VALUE_2");
        dyno2.setDynamicIntents(dynamicIntents);

        dynamicOptions.add(dyno);
        dynamicOptions.add(dyno2);
        dog.setDynamicOptions(dynamicOptions);

        return dog;
    }

    private OptionOptionGroupComponent getOptionOptionGroupComponent() throws Exception {
        OptionGroupComponentType ogct = EntityIdBuilder.getEntity(OptionGroupComponentType.class, 1);
        ogct.setType(OptionGroupComponentTypeConstant.STATIC.getType());

        OptionGroupComponent ogc = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        ogc.setName("NAME");
        ogc.setOptionGroupComponentTypeId(ogct);

        Option option = EntityIdBuilder.getEntity(Option.class, 1);
        option.setName("option");
        option.setText("TEXT");

        OptionOptionGroupComponent oogc = EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 1);
        oogc.setOptionNumber(1);
        oogc.setSequence(1);
        oogc.setOptionGroupComponentId(ogc);
        oogc.setOptionId(option);

        return oogc;
    }

    private OptionGroupComponent getDynOptionGroupComponent() throws Exception {
        OptionGroupComponentType ogct = EntityIdBuilder.getEntity(OptionGroupComponentType.class, 2);
        ogct.setType(OptionGroupComponentTypeConstant.DYNAMIC.getType());
        OptionGroupComponent ogc = EntityIdBuilder.getEntity(OptionGroupComponent.class, 2, true);
        ogc.setName("TEST_DOG");
        ogc.setOptionGroupComponentTypeId(ogct);
        return ogc;
    }
}
