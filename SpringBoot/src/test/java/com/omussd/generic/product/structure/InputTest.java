/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 *
 * @author XY40428
 */
public class InputTest extends AbstractJavaBeanTest<Input> {
    @Override
    protected Input getBeanInstance() {
        return new Input();
    }
    
    @Override
    public void equalsAndHashCodeContract() {
        EqualsVerifier.forClass(getBeanInstance().getClass())
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .withRedefinedSuperclass()
                .verify();
    }
}
