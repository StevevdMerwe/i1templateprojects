/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;

/**
 *
 * @author XY40428
 */
public class ProductStructureTest extends AbstractJavaBeanTest<ProductStructure> {

    @Override
    protected ProductStructure getBeanInstance() {
        return new ProductStructure();
    }
}
