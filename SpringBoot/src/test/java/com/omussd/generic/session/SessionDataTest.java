/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.session;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

/**
 *
 * @author XY40428
 */
public class SessionDataTest extends AbstractJavaBeanTest<SessionData> {

    @Override
    protected SessionData getBeanInstance() {
        return new SessionData();
    }

    @Test
    public void equalsAndHashCodeContract() {
        EqualsVerifier.forClass(getBeanInstance().getClass())
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .withIgnoredFields("sessionHistoryStack")
                .verify();
    }
}
