/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.bean.test;

import java.time.LocalDateTime;
import org.meanbean.lang.Factory;

/**
 *
 * @author XY40428
 */
public class LocalDateTimeFactory implements Factory<LocalDateTime> {
    @Override
    public LocalDateTime create() {
        return LocalDateTime.now();
    }
}
