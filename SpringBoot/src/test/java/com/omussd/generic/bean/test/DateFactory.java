/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.bean.test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.meanbean.lang.Factory;

/**
 *
 * @author XY40428
 */
public class DateFactory implements Factory<Date>{

    @Override
    public Date create() {
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTimeInMillis(0L);
        return cal.getTime();
    }
}
