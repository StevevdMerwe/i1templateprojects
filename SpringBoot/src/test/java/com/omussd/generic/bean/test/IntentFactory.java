/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.bean.test;

import com.omussd.generic.db.entity.Intent;
import org.meanbean.lang.Factory;

/**
 *
 * @author XY40428
 */
public class IntentFactory implements Factory<Intent> {

    @Override
    public Intent create() {
        Intent intent = new Intent();
        intent.setId(4);
        return intent;
    }
}
