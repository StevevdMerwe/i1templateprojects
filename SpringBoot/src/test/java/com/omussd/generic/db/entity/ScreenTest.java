/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.bean.test.DateFactory;
import com.omussd.generic.bean.test.IntentFactory;
import com.omussd.generic.bean.test.LocalDateTimeFactory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import java.time.LocalDateTime;
import java.util.Date;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.meanbean.test.BeanTester;
import org.meanbean.test.Configuration;
import org.meanbean.test.ConfigurationBuilder;

/**
 *
 * @author XY40428
 */
public class ScreenTest extends AbstractJavaBeanTest<Screen> {

    @Override
    protected Screen getBeanInstance() {
        return new Screen();
    }

    @Override
    public void equalsAndHashCodeContract() {
        try {
            EqualsVerifier.forClass(getBeanInstance().getClass())
                    .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                    .withOnlyTheseFields("id")
                    .withPrefabValues(Intent.class, EntityIdBuilder.getEntity(Intent.class, 1), EntityIdBuilder.getEntity(Intent.class, 2))
                    .withPrefabValues(Product.class, EntityIdBuilder.getEntity(Product.class, 1), EntityIdBuilder.getEntity(Product.class, 2))
                    .withPrefabValues(ScreenType.class, EntityIdBuilder.getEntity(ScreenType.class, 1), EntityIdBuilder.getEntity(ScreenType.class, 2))
                    .withPrefabValues(ScreenScreenComponent.class, EntityIdBuilder.getEntity(ScreenScreenComponent.class, 1), EntityIdBuilder.getEntity(ScreenScreenComponent.class, 2))
                    .withPrefabValues(ScreenScreenDeterminate.class, EntityIdBuilder.getEntity(ScreenScreenDeterminate.class, 1), EntityIdBuilder.getEntity(ScreenScreenDeterminate.class, 2))
                    .withPrefabValues(ScreenDeterminate.class, EntityIdBuilder.getEntity(ScreenDeterminate.class, 1), EntityIdBuilder.getEntity(ScreenDeterminate.class, 2))
                    .withPrefabValues(TextPlaceholder.class, EntityIdBuilder.getEntity(TextPlaceholder.class, 1), EntityIdBuilder.getEntity(TextPlaceholder.class, 2))
                    .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Override
    public void getterAndSetterCorrectness() throws Exception {
        final BeanTester beanTester = new BeanTester();
        beanTester.getFactoryCollection().addFactory(LocalDateTime.class, new LocalDateTimeFactory());
        beanTester.getFactoryCollection().addFactory(Date.class, new DateFactory());
        beanTester.getFactoryCollection().addFactory(Intent.class, new IntentFactory());
        Configuration conf = new ConfigurationBuilder().iterations(15).build();
        beanTester.testBean(getBeanInstance().getClass(), conf);
    }
}
