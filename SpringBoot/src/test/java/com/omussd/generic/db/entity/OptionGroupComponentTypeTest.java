/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.test.helper.EntityIdBuilder;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;

/**
 *
 * @author XY40428
 */
public class OptionGroupComponentTypeTest extends AbstractJavaBeanTest<OptionGroupComponentType> {

    @Override
    protected OptionGroupComponentType getBeanInstance() {
        return new OptionGroupComponentType();
    }
    
    @Override
    public void equalsAndHashCodeContract() {
        try {
            EqualsVerifier.forClass(getBeanInstance().getClass())
                    .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                    .withOnlyTheseFields("id")
                    .withPrefabValues(Intent.class, EntityIdBuilder.getEntity(Intent.class, 1), EntityIdBuilder.getEntity(Intent.class, 2))
                    .withPrefabValues(OptionOptionGroupComponent.class, EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 1), EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 2))
                    .withPrefabValues(OptionGroupComponent.class, EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true), EntityIdBuilder.getEntity(OptionGroupComponent.class, 2, true))
                    .withPrefabValues(ScreenScreenComponent.class, EntityIdBuilder.getEntity(ScreenScreenComponent.class, 1), EntityIdBuilder.getEntity(ScreenScreenComponent.class, 2))
                    .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
}
