/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.bean.test.DateFactory;
import com.omussd.generic.bean.test.IntentFactory;
import com.omussd.generic.bean.test.LocalDateTimeFactory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import java.time.LocalDateTime;
import java.util.Date;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.meanbean.test.BeanTester;
import org.meanbean.test.Configuration;
import org.meanbean.test.ConfigurationBuilder;

/**
 *
 * @author XY40428
 */
public class OptionOptionGroupComponentTest extends AbstractJavaBeanTest<OptionOptionGroupComponent> {

    @Override
    protected OptionOptionGroupComponent getBeanInstance() {
        return new OptionOptionGroupComponent();
    }

    @Override
    public void equalsAndHashCodeContract() {
        try {
            EqualsVerifier.forClass(getBeanInstance().getClass())
                    .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                    .withOnlyTheseFields("id")
                    .withPrefabValues(Intent.class, EntityIdBuilder.getEntity(Intent.class, 1), EntityIdBuilder.getEntity(Intent.class, 2))
                    .withPrefabValues(OptionGroupComponent.class, EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true), EntityIdBuilder.getEntity(OptionGroupComponent.class, 2, true))
                    .withPrefabValues(Option.class, EntityIdBuilder.getEntity(Option.class, 1), EntityIdBuilder.getEntity(Option.class, 2))
                    .withPrefabValues(Screen.class, EntityIdBuilder.getEntity(Screen.class, 1), EntityIdBuilder.getEntity(Screen.class, 2))
                    .withPrefabValues(ScreenComponent.class, EntityIdBuilder.getEntity(ScreenComponent.class, 1), EntityIdBuilder.getEntity(ScreenComponent.class, 2))
                    .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    @Override
    public void getterAndSetterCorrectness() throws Exception {
        final BeanTester beanTester = new BeanTester();
        beanTester.getFactoryCollection().addFactory(LocalDateTime.class, new LocalDateTimeFactory());
        beanTester.getFactoryCollection().addFactory(Date.class, new DateFactory());
        beanTester.getFactoryCollection().addFactory(Intent.class, new IntentFactory());
        Configuration conf = new ConfigurationBuilder().iterations(15).build();
        beanTester.testBean(getBeanInstance().getClass(), conf);
    }
}
