/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.test.helper.EntityIdBuilder;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;

/**
 *
 * @author XY40428
 */
public class TextComponentTest extends AbstractJavaBeanTest<TextComponent> {

    @Override
    protected TextComponent getBeanInstance() {
        return new TextComponent();
    }
    
    @Override
    public void equalsAndHashCodeContract() {
        try {
        EqualsVerifier.forClass(getBeanInstance().getClass())
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .withOnlyTheseFields("id")
                .withPrefabValues(Intent.class, EntityIdBuilder.getEntity(Intent.class, 1), EntityIdBuilder.getEntity(Intent.class, 2))
                .withPrefabValues(Product.class, EntityIdBuilder.getEntity(Product.class, 1), EntityIdBuilder.getEntity(Product.class, 2))
                .withPrefabValues(Screen.class, EntityIdBuilder.getEntity(Screen.class, 1), EntityIdBuilder.getEntity(Screen.class, 2))
                .withPrefabValues(ScreenScreenComponent.class, EntityIdBuilder.getEntity(ScreenScreenComponent.class, 1), EntityIdBuilder.getEntity(ScreenScreenComponent.class, 2))
                .withRedefinedSuperclass()
                .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
}
