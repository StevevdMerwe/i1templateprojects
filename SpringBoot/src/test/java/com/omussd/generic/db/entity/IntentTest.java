/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.bean.test.DateFactory;
import com.omussd.generic.bean.test.IntentFactory;
import com.omussd.generic.bean.test.LocalDateTimeFactory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import java.time.LocalDateTime;
import java.util.Date;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.meanbean.test.BeanTester;
import org.meanbean.test.Configuration;
import org.meanbean.test.ConfigurationBuilder;

/**
 *
 * @author XY40428
 */
public class IntentTest extends AbstractJavaBeanTest<Intent> {

    @Override
    protected Intent getBeanInstance() {
        return new Intent();
    }

    @Override
    public void equalsAndHashCodeContract() {
        try {
            EqualsVerifier.forClass(getBeanInstance().getClass())
                    .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                    .withOnlyTheseFields("id")
                    .withPrefabValues(AuditLog.class, EntityIdBuilder.getEntity(AuditLog.class, 1), EntityIdBuilder.getEntity(AuditLog.class, 2))
                    .withPrefabValues(Screen.class, EntityIdBuilder.getEntity(Screen.class, 1), EntityIdBuilder.getEntity(Screen.class, 2))
                    .withPrefabValues(IntentDestination.class, EntityIdBuilder.getEntity(IntentDestination.class, 1), EntityIdBuilder.getEntity(IntentDestination.class, 2))
                    .withPrefabValues(IntentPurpose.class, EntityIdBuilder.getEntity(IntentPurpose.class, 1), EntityIdBuilder.getEntity(IntentPurpose.class, 2))
                    .withPrefabValues(IntentType.class, EntityIdBuilder.getEntity(IntentType.class, 1), EntityIdBuilder.getEntity(IntentType.class, 2))
                    .withPrefabValues(Product.class, EntityIdBuilder.getEntity(Product.class, 1), EntityIdBuilder.getEntity(Product.class, 2))
                    .withPrefabValues(ScreenDeterminate.class, EntityIdBuilder.getEntity(ScreenDeterminate.class, 1), EntityIdBuilder.getEntity(ScreenDeterminate.class, 2))
                    .withPrefabValues(ScreenScreenDeterminate.class, EntityIdBuilder.getEntity(ScreenScreenDeterminate.class, 1), EntityIdBuilder.getEntity(ScreenScreenDeterminate.class, 2))
                    .withPrefabValues(OptionGroupComponent.class, EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true), EntityIdBuilder.getEntity(OptionGroupComponent.class, 2, true))
                    .withPrefabValues(TextPlaceholder.class, EntityIdBuilder.getEntity(TextPlaceholder.class, 1), EntityIdBuilder.getEntity(TextPlaceholder.class, 2))
                    .withPrefabValues(InputComponent.class, EntityIdBuilder.getEntity(InputComponent.class, 1, true), EntityIdBuilder.getEntity(InputComponent.class, 2, true))
                    .withPrefabValues(OptionOptionGroupComponent.class, EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 1), EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 2))
                    .withPrefabValues(Option.class, EntityIdBuilder.getEntity(Option.class, 1), EntityIdBuilder.getEntity(Option.class, 2))
                    .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
    
     @Override
    public void getterAndSetterCorrectness() throws Exception {
        final BeanTester beanTester = new BeanTester();
        beanTester.getFactoryCollection().addFactory(LocalDateTime.class, new LocalDateTimeFactory());
        beanTester.getFactoryCollection().addFactory(Date.class, new DateFactory());
        beanTester.getFactoryCollection().addFactory(Intent.class, new IntentFactory());
        Configuration conf = new ConfigurationBuilder().iterations(15).build();
        beanTester.testBean(getBeanInstance().getClass(), conf);
    }
}
