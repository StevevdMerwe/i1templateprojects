package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.test.helper.EntityIdBuilder;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;

/**
 *
 * @author XY40428
 */
public class AuditLogEventTest extends AbstractJavaBeanTest<AuditLogEvent> {
    @Override
    protected AuditLogEvent getBeanInstance() {
        return new AuditLogEvent();
    }
    
    @Override
    public void equalsAndHashCodeContract() {
        try {
        EqualsVerifier.forClass(getBeanInstance().getClass())
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .withOnlyTheseFields("id")
                .withPrefabValues(Intent.class, EntityIdBuilder.getEntity(Intent.class, 1), EntityIdBuilder.getEntity(Intent.class, 2))
                .withPrefabValues(AuditLog.class, EntityIdBuilder.getEntity(AuditLog.class, 1), EntityIdBuilder.getEntity(AuditLog.class, 2))
                .withPrefabValues(Network.class, EntityIdBuilder.getEntity(Network.class, 1), EntityIdBuilder.getEntity(Network.class, 2))
                .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
}
