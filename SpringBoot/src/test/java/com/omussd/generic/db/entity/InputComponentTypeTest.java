/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import com.omussd.generic.test.helper.EntityIdBuilder;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;

/**
 *
 * @author XY40428
 */
public class InputComponentTypeTest extends AbstractJavaBeanTest<InputComponentType> {

    @Override
    protected InputComponentType getBeanInstance() {
        return new InputComponentType();
    }

    @Override
    public void equalsAndHashCodeContract() {
        try {
            EqualsVerifier.forClass(getBeanInstance().getClass())
                    .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                    .withOnlyTheseFields("id")
                    .withPrefabValues(InputComponent.class, EntityIdBuilder.getEntity(InputComponent.class, 1, true), EntityIdBuilder.getEntity(InputComponent.class, 2, true))
                    .verify();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }
}
