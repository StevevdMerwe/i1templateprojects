/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.util;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author XY40428
 */
public class SecurityUtilTest {

    public SecurityUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of checkBcrypt method, of class SecurityUtil case true.
     */
    @Test
    public void testCheckBcryptTrue() {
        System.out.println("checkBcrypt case true");
        String plain = "ZgyA$Z*#j7/Fp2/C";
        String bcryptString = "$2a$10$ka3KdxVGznyNbcC0pW7xzONjV.6zRmWA6mYNbJdqyJ2qTWpE19mzq";
        boolean expResult = true;
        boolean result = SecurityUtil.checkBcrypt(plain, bcryptString);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of checkBcrypt method, of class SecurityUtil case false.
     */
    @Test
    public void testCheckBcryptFalse() {
        System.out.println("checkBcrypt case false");
        String plain = "ZgyA$Z*#j7/Fp2/c";
        String bcryptString = "$2a$10$ka3KdxVGznyNbcC0pW7xzONjV.6zRmWA6mYNbJdqyJ2qTWpE19mzq";
        boolean expResult = false;
        boolean result = SecurityUtil.checkBcrypt(plain, bcryptString);
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getBcryptRandomSalted method, of class SecurityUtil.
     */
    @Test
    public void testGetBcryptRandomSalted() {
        System.out.println("getBcryptRandomSalted");
        String plain = "ZgyA$Z*#j7/Fp2/C";
        int cost = 10;
        String re = "\\$2a\\$" + cost + "\\$[A-Za-z\\.0-9\\/\\\\\\-\\_]{53}";
        String hashed = SecurityUtil.getBcryptRandomSalted(plain, cost);
        Assert.assertTrue(hashed.matches(re));
    }

    @Test
    public void testGetBcryptRandomSaltedIsRandom() {
        System.out.println("getBcryptRandomSalted is Random");
        String plain = "ZgyA$Z*#j7/Fp2/C";
        int cost = 10;
        String re = "\\$2a\\$" + cost + "\\$[A-Za-z\\.0-9\\/\\\\\\-\\_]{53}";
        Set<String> usedStrings = new HashSet<>();
        for (int i = 0; i < 15; i++) {
            String hashed = SecurityUtil.getBcryptRandomSalted(plain, cost);
            if (usedStrings.contains(hashed)) {
                Assert.fail("Not random");
                break;
            } else {
                usedStrings.add(hashed);
                Assert.assertTrue(hashed.matches(re));
            }
        }
    }

    @Test
    public void testGetUsernamePasswordCombinationFromHttpBasicAuthHeader() {
        String auth = "Username:Password";
        byte[] bytes = Base64.getEncoder().encode(auth.getBytes());
        try {
            String encoded = new String(bytes, "UTF-8");
            String[] decoded = SecurityUtil.getUsernamePasswordCombinationFromHttpBasicAuthHeader("Basic " + encoded);
            String[] authSplit = auth.split(":");
            Assert.assertTrue(Arrays.equals(authSplit, decoded));
        } catch (UnsupportedEncodingException e) {
            Assert.fail("No UTF-8 encoding");
        }
    }
}
