package com.omussd.generic.util;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author XY40428
 */
public class InputValidationUtilTest {

    public InputValidationUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testValidateAlphanumericTrue() {
        System.out.println("validateAlphanumeric true");
        String input = "  1 hello123TH344s is a 11 33 345 40 t2est      1   ";
        boolean expResult = true;
        boolean result = InputValidationUtil.validateAlphanumeric(input);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateAlphanumericFalse() {
        System.out.println("validateAlphanumeric false");
        String input = "";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateAlphanumeric(input);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateNumberTrue() {
        System.out.println("validateNumber true");
        String number = "2349988";
        boolean expResult = true;
        boolean result = InputValidationUtil.validateNumber(number);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateNumberMaxExceeded() {
        System.out.println("validateNumber max exceeded");
        String number = Long.MAX_VALUE + "1";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateNumber(number);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateNumberMinExceeded() {
        System.out.println("validateNumber min exceeded");
        String number = Long.MIN_VALUE + "1";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateNumber(number);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateNumberFalse() {
        System.out.println("validateNumber false");
        String number = "123o123";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateNumber(number);
        Assert.assertEquals(expResult, result);
    }

    @Test
    public void testValidateRSAIdNumberTrue() {
        System.out.println("validateRSAIdNumber true");
        String idNumber = "8001015009087";
        boolean expResult = true;
        boolean result = InputValidationUtil.validateRSAIdNumber(idNumber);
        Assert.assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateRSAIdNumberFalse() {
        System.out.println("validateRSAIdNumber false");
        String idNumber = "8001015009057";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateRSAIdNumber(idNumber);
        Assert.assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateCellNumberFalse() {
        System.out.println("validateCellNumber false");
        String cell = "277286782s54";
        boolean expResult = false;
        boolean result = InputValidationUtil.validateCellNumber(cell);
        Assert.assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateCellNumberTrue() {
        System.out.println("validateCellNumber true");
        String cell = "27728678254";
        boolean expResult = true;
        boolean result = InputValidationUtil.validateCellNumber(cell);
        Assert.assertEquals(expResult, result);
    }
}
