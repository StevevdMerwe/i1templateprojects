/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.test.helper;

import java.lang.reflect.Field;

/**
 *
 * @author XY40428
 */
public class EntityIdBuilder {

    private EntityIdBuilder() {
    }

    public static <T extends Object> T getEntity(Class<T> clazz, int idValue, boolean userSuper)
            throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        T t = clazz.newInstance();
        Field field;
        if (!userSuper) {
            field = clazz.getDeclaredField("id");
        } else {
            field = clazz.getSuperclass().getDeclaredField("id");
        }
        field.setAccessible(true);
        field.set(t, idValue);
        return t;
    }

    public static <T extends Object> T getEntity(Class<T> clazz, int idValue)
            throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        return getEntity(clazz, idValue, false);
    }
}
