/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.test.helper;

import com.omussd.common.util.XMLUtil;
import com.omussd.generic.bean.test.AbstractJavaBeanTest;
import javax.xml.bind.JAXBException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author XY40428
 */
public abstract class JAXBTester<T> extends AbstractJavaBeanTest<T> {

    @Test
    public void testToXML() {
        T instance = getBeanInstance();
        System.out.println("toXML: " + instance.toString());
        try {
            String xml = XMLUtil.toXML(true, instance.getClass(), instance);
            if (xml == null || xml.trim().isEmpty()) {
                Assert.fail("XML is null or empty");
            }
        } catch (JAXBException e) {
            Assert.fail("XML failed to parse (toXML)");
        }
    }

    @Test
    public void fromXML() {
        T instance = getBeanInstance();
        System.out.println("fromXML: " + instance.toString());
        try {
            String xml = XMLUtil.toXML(false, instance.getClass(), instance);
            System.out.println("fromXMLNew: " + XMLUtil.fromXML(instance.getClass(), xml));
            if (!instance.equals(XMLUtil.fromXML(instance.getClass(), xml))) {
                Assert.fail("New instance does not equal original");
            }
        } catch (JAXBException e) {
            Assert.fail("XML failed to parse (fromXML)");
        }
    }
}
