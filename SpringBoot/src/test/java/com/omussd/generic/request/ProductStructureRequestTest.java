/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.request;

import com.omussd.generic.product.structure.Components;
import com.omussd.generic.product.structure.Determinate;
import com.omussd.generic.product.structure.Input;
import com.omussd.generic.product.structure.Option;
import com.omussd.generic.product.structure.OptionGroup;
import com.omussd.generic.product.structure.ProductStructure;
import com.omussd.generic.product.structure.RootDeterminates;
import com.omussd.generic.product.structure.Screen;
import com.omussd.generic.product.structure.Screens;
import com.omussd.generic.product.structure.Text;
import com.omussd.generic.product.structure.TextPlaceholder;
import com.omussd.generic.test.helper.JAXBTester;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author XY40428
 */
public class ProductStructureRequestTest extends JAXBTester<ProductStructureRequest> {

    @Override
    protected ProductStructureRequest getBeanInstance() {
        ProductStructureRequest request = new ProductStructureRequest();
        ProductStructure struct = new ProductStructure();

        RootDeterminates rootDeterminates = new RootDeterminates();
        rootDeterminates.setDestination("destination");
        rootDeterminates.setPurpose("purpose");
        rootDeterminates.setIntentValue("intentValue");
        List<Determinate> determinates = new ArrayList<>();
        Determinate d1 = new Determinate();
        d1.setScreenCode("0-0");
        d1.setValue("SOMETHING");
        Determinate d2 = new Determinate();
        d2.setScreenCode("0-1");
        d2.setValue("SOMETHING2");
        determinates.add(d1);
        determinates.add(d2);
        rootDeterminates.setDeterminates(determinates);

        Screens screens = new Screens();
        screens.setProductName("TestProduct");
        List<Screen> screensList = new ArrayList<>();

        Screen s1 = new Screen();
        s1.setCode("0-1");
        s1.setComponents(getComponents());
        s1.setFailureScreenCode("9-9");
        s1.setTextPlaceholders(getPlaceholders());
        s1.setType("screenType");
        
        Screen s2 = new Screen();
        s1.setCode("0-1");
        s1.setComponents(getComponents());
        s1.setFailureScreenCode("9-9");
        s1.setTextPlaceholders(getPlaceholders());
        s1.setType("screenType");
        
        screensList.add(s1);
        screensList.add(s2);
        
        screens.setScreenList(screensList);
        
        struct.setRootDeterminates(rootDeterminates);
        struct.setScreens(screens);
        request.setProductStructure(struct);
        return request;
    }

    private Components getComponents() {
        Components comps = new Components();

        Input input = new Input();
        input.setType("Number");
        input.setSequence(1);
        input.setName("inputName1");
        input.setMaxLength(Integer.MAX_VALUE);
        input.setMinLength(0);
        input.setParameterName("parameterName");
        comps.setInput(input);

        List<Text> texts = new ArrayList<>();
        Text t1 = new Text();
        t1.setName("t1Name");
        t1.setSequence(1);
        t1.setTextString("textString1");
        Text t2 = new Text();
        t2.setName("t2Name");
        t2.setSequence(2);
        t2.setTextString("textString2");
        texts.add(t1);
        texts.add(t2);
        comps.setTexts(texts);

        List<OptionGroup> optionGroups = new ArrayList<>();
        OptionGroup og1 = new OptionGroup();
        og1.setDestination("destination");
        og1.setPurpose("purpose");
        og1.setIntentValue("intentValue");
        og1.setParameterName("parameterName");
        og1.setName("name");
        og1.setSequence(1);
        og1.setType("og1Type");
        og1.setOptions(getOptions());
        OptionGroup og2 = new OptionGroup();
        og2.setDestination("destination");
        og2.setPurpose("purpose");
        og2.setIntentValue("intentValue");
        og2.setParameterName("parameterName");
        og2.setName("name");
        og2.setSequence(1);
        og2.setType("og2Type");
        og2.setOptions(getOptions());
        comps.setOptions(optionGroups);
        
        return comps;
    }

    private List<Option> getOptions() {
        List<Option> options = new ArrayList<>();
        Option o1 = new Option();
        o1.setName("1name");
        o1.setNum(1);
        o1.setSeq(1);
        o1.setText("1text");
        o1.setType("1type");
        o1.setValue("1value");
        Option o2 = new Option();
        o2.setName("2name");
        o2.setNum(2);
        o2.setSeq(2);
        o2.setText("2text");
        o2.setType("2type");
        o2.setValue("2value");
        options.add(o1);
        options.add(o2);

        return options;
    }

    private List<TextPlaceholder> getPlaceholders() {
        List<TextPlaceholder> placeholders = new ArrayList<>();
        
        TextPlaceholder tp1 = new TextPlaceholder();
        tp1.setDestination("destination1");
        tp1.setPurpose("purpose1");
        tp1.setValue("value1");
        tp1.setVariable("variable");
        
        TextPlaceholder tp2 = new TextPlaceholder();
        tp2.setDestination("destination2");
        tp2.setPurpose("purpose2");
        tp2.setValue("value2");
        tp2.setVariable("variable");
        
        placeholders.add(tp1);
        placeholders.add(tp2);
        
        return placeholders;
    } 
}
