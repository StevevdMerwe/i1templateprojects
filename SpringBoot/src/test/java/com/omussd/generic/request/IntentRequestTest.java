/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.request;

import com.omussd.generic.test.helper.JAXBTester;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author XY40428
 */
public class IntentRequestTest extends JAXBTester<IntentRequest> {

    @Override
    protected IntentRequest getBeanInstance() {
        IntentRequest request = new IntentRequest();
        request.setDestination("destination");
        request.setPurpose("purpose");
        request.setIntentValue("intentValue");
        request.setMsisdn("12345");

        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("key1", "value1");
        parameterMap.put("key2", "value2");

        request.setParameterMap(parameterMap);
        return request;
    }
}
