/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.InputComponentType;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.TextComponent;
import com.omussd.generic.db.repository.InputComponentRepository;
import com.omussd.generic.db.repository.InputComponentTypeRepository;
import com.omussd.generic.db.repository.OptionGroupComponentRepository;
import com.omussd.generic.db.repository.OptionGroupComponentTypeRepository;
import com.omussd.generic.db.repository.OptionOptionGroupComponentRepository;
import com.omussd.generic.db.repository.OptionRepository;
import com.omussd.generic.db.repository.OptionTypeRepository;
import com.omussd.generic.db.repository.ScreenComponentRepository;
import com.omussd.generic.db.repository.ScreenScreenComponentRepository;
import com.omussd.generic.db.repository.TextComponentRepository;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.bean.DynamicIntent;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.generic.test.helper.EntityIdBuilder;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ScreenComponentRepository.class, OptionOptionGroupComponentRepository.class, OptionGroupComponentRepository.class,
    InputComponentRepository.class, OptionRepository.class, ScreenScreenComponentRepository.class, TextComponentRepository.class,
    InputComponentTypeRepository.class, OptionGroupComponentTypeRepository.class, OptionTypeRepository.class})
public class ScreenComponentServiceTest {

    private OptionGroupComponent optionGroupComponent;
    private TextComponent textComponent;

    @Autowired
    private ScreenComponentService screenComponentService;

    @MockBean
    private ScreenComponentRepository screenComponentRepository;
    @MockBean
    private OptionOptionGroupComponentRepository optionOptionGroupComponentRepository;
    @MockBean
    private OptionGroupComponentRepository optionGroupComponentRepository;
    @MockBean
    private InputComponentRepository inputComponentRepository;
    @MockBean
    private OptionRepository optionRepository;
    @MockBean
    private ScreenScreenComponentRepository screenScreenComponentRepository;
    @MockBean
    private TextComponentRepository textComponentRepository;
    @MockBean
    private InputComponentTypeRepository inputComponentTypeRepository;
    @MockBean
    private OptionGroupComponentTypeRepository optionGroupComponentTypeRepository;
    @MockBean
    private OptionTypeRepository optionTypeRepository;

    @TestConfiguration
    static class ComponentServiceConfig {

        @Bean
        ScreenComponentService getScreenComponentService() {
            return new ScreenComponentService();
        }
    }

    @Before
    public void init() throws Exception {
        textComponent = EntityIdBuilder.getEntity(TextComponent.class, 1, true);
        textComponent.setName("name");
        textComponent.setText("text");

        optionGroupComponent = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);

        Mockito.when(optionGroupComponentRepository.findByName(Mockito.anyString())).thenReturn(optionGroupComponent);
        Mockito.when(textComponentRepository.save(Mockito.any(TextComponent.class))).thenReturn(textComponent);
        Mockito.when(inputComponentTypeRepository.findByType(Mockito.anyString())).thenReturn(getInputComponent(InputComponentTypeConstant.ALPHANUMERIC).getInputComponentTypeId());
        Mockito.when(inputComponentRepository.save(Mockito.any(InputComponent.class))).thenReturn(getInputComponent(InputComponentTypeConstant.ALPHANUMERIC));
    }

    /**
     * Test of deleteAllOptionOptionGroupComponentsHavingOptionGroupComponents
     * method, of class ScreenComponentService.
     */
    @Test
    public void testDeleteAllOptionOptionGroupComponentsHavingOptionGroupComponents() throws Exception {
        List<OptionOptionGroupComponent> oogcs = new ArrayList<>();
        OptionOptionGroupComponent oogc = EntityIdBuilder.getEntity(OptionOptionGroupComponent.class, 1);
        oogcs.add(oogc);

        List<OptionGroupComponent> ogcs = new ArrayList<>();
        OptionGroupComponent ogc = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        ogc.setOptionOptionGroupComponentCollection(oogcs);
        ogcs.add(ogc);

        screenComponentService.deleteAllOptionOptionGroupComponentsHavingOptionGroupComponents(ogcs);
    }

    /**
     * Test of deleteAllOptionGroupComponents method, of class
     * ScreenComponentService.
     */
    @Test
    public void testDeleteAllOptionGroupComponents() throws Exception {
        List<OptionGroupComponent> ogcs = new ArrayList<>();
        OptionGroupComponent ogc = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        ogcs.add(ogc);

        screenComponentService.deleteAllOptionGroupComponents(ogcs);
    }

    /**
     * Test of deleteAllInputComponentsOfProduct method, of class
     * ScreenComponentService.
     */
    @Test
    public void testDeleteAllInputComponentsOfProduct() throws Exception {
        Product product = EntityIdBuilder.getEntity(Product.class, 1);

        screenComponentService.deleteAllInputComponentsOfProduct(product);
    }

    /**
     * Test of deleteAllScreenComponentsByProduct method, of class
     * ScreenComponentService.
     */
    @Test
    public void testDeleteAllScreenComponentsByProduct() throws Exception {
        Product product = EntityIdBuilder.getEntity(Product.class, 1);

        screenComponentService.deleteAllScreenComponentsByProduct(product);
    }

    /**
     * Test of deleteScreenScreenComponentsByScreens method, of class
     * ScreenComponentService.
     */
    @Test
    public void testDeleteScreenScreenComponentsByScreens() throws Exception {
        List<Screen> screens = new ArrayList<>();
        Screen screen = EntityIdBuilder.getEntity(Screen.class, 1);
        screens.add(screen);

        screenComponentService.deleteScreenScreenComponentsByScreens(screens);
    }

    /**
     * Test of deleteAllOptionsOfProduct method, of class
     * ScreenComponentService.
     */
    @Test
    public void testDeleteAllOptionsOfProduct() throws Exception {
        Product product = EntityIdBuilder.getEntity(Product.class, 1);

        screenComponentService.deleteAllOptionsOfProduct(product);
    }

    /**
     * Test of findDynamicOptionByNumber method, of class
     * ScreenComponentService.
     */
    @Test
    public void testFindDynamicOptionByNumber() {
        DynamicOptionGroup dog = getDynamicOptionGroup();

        DynamicOption expected = dog.getDynamicOptions().get(0);
        DynamicOption result = screenComponentService.findDynamicOptionByNumber(dog, 2);

        Assert.assertEquals(expected, result);
    }

    /**
     * Test of findDynamicOptionGroupComponentContianingOptionWithNumber method,
     * of class ScreenComponentService.
     */
    @Test
    public void testFindDynamicOptionGroupComponentContianingOptionWithNumber() throws Exception {
        DynamicOptionGroup dog = getDynamicOptionGroup();
        SessionData data = new SessionData();
        data.addDynamicOptionGroup(dog);

        String msisdn = "msisdn";

        SessionDataCache.INSTANCE.put(msisdn, data);

        OptionGroupComponent result = screenComponentService.findDynamicOptionGroupComponentContianingOptionWithNumber(msisdn, 2);
        Assert.assertEquals(optionGroupComponent, result);

        result = screenComponentService.findDynamicOptionGroupComponentContianingOptionWithNumber(msisdn, 1);
        Assert.assertTrue(result == null);
    }

    /**
     * Test of isInputValid method, of class ScreenComponentService.
     */
    @Test
    public void testIsInputValid() throws Exception {
        boolean result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.NUMBER), "123");
        Assert.assertTrue(result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.NUMBER), "1");
        Assert.assertTrue(!result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.NUMBER), "123456789012345678901234567890123456789012345678901234567890");
        Assert.assertTrue(!result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.ALPHANUMERIC), "Hello");
        Assert.assertTrue(result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.RSA_LEGAL_ID), "8001015009087");
        Assert.assertTrue(result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.CELL_NUMBER), "27728678254");
        Assert.assertTrue(result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.ALPHANUMERIC), null);
        Assert.assertTrue(!result);

        result = screenComponentService.isInputValid(getInputComponent(InputComponentTypeConstant.ALPHANUMERIC), "");
        Assert.assertTrue(!result);
    }

    /**
     * Test of createTextComponent method, of class ScreenComponentService.
     */
    @Test
    public void testCreateTextComponent() throws Exception {
        TextComponent result = screenComponentService.createTextComponent(textComponent.getName(), textComponent.getText());
        Assert.assertEquals(textComponent, result);
    }

    /**
     * Test of createInputComponent method, of class ScreenComponentService.
     */
    @Test
    public void testCreateInputComponent() throws Exception {
        InputComponent expected = getInputComponent(InputComponentTypeConstant.ALPHANUMERIC);
        List<Intent> intents = new ArrayList<>();
        intents.addAll(expected.getIntentCollection());
        InputComponent result = screenComponentService.createInputComponent(expected.getName(),
                expected.getParameterName(), InputComponentTypeConstant.ALPHANUMERIC, expected.getMinimumLength(),
                expected.getMaximumLength(), intents);
        Assert.assertEquals(expected, result);
    }

    private DynamicOptionGroup getDynamicOptionGroup() {
        DynamicOptionGroup dog = new DynamicOptionGroup();
        dog.setName("TEST_DOG");
        dog.setParameter("TEST_PARAMETER");

        List<DynamicIntent> dynamicIntents = new ArrayList<>();
        DynamicIntent dynamicIntent = new DynamicIntent();
        dynamicIntent.setDestination("TEST_DESTINATION");
        dynamicIntent.setPurpose("TEST_PURPOSE");
        dynamicIntent.setIntentType(IntentTypeConstant.SUBMIT.getIntentType());
        dynamicIntent.setValue("TEST_VALUE");
        dynamicIntents.add(dynamicIntent);

        List<DynamicOption> dynamicOptions = new ArrayList<>();
        DynamicOption dyno = new DynamicOption();
        dyno.setNumber(2);
        dyno.setSequence(1);
        dyno.setText("TEST_TEXT");
        dyno.setValue("TEST_VALUE");

        DynamicOption dyno2 = new DynamicOption();
        dyno2.setNumber(3);
        dyno2.setSequence(2);
        dyno2.setText("TEST_TEXT_2");
        dyno2.setValue("TEST_VALUE_2");
        dyno2.setDynamicIntents(dynamicIntents);

        dynamicOptions.add(dyno);
        dynamicOptions.add(dyno2);
        dog.setDynamicOptions(dynamicOptions);

        return dog;
    }

    private InputComponent getInputComponent(InputComponentTypeConstant type) throws Exception {
        InputComponentType inputComponentType = EntityIdBuilder.getEntity(InputComponentType.class, 1);
        inputComponentType.setType(type.getType());
        InputComponent inputComponent = EntityIdBuilder.getEntity(InputComponent.class, 1, true);
        inputComponent.setInputComponentTypeId(inputComponentType);
        inputComponent.setMaximumLength(50);
        inputComponent.setMinimumLength(2);
        return inputComponent;
    }
}
