/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.OptionTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.IntentType;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.repository.ProductRepository;
import com.omussd.generic.product.structure.Components;
import com.omussd.generic.product.structure.Determinate;
import com.omussd.generic.product.structure.Input;
import com.omussd.generic.product.structure.Intent;
import com.omussd.generic.product.structure.Option;
import com.omussd.generic.product.structure.OptionGroup;
import com.omussd.generic.product.structure.ProductStructure;
import com.omussd.generic.product.structure.RootDeterminates;
import com.omussd.generic.product.structure.Screen;
import com.omussd.generic.product.structure.Screens;
import com.omussd.generic.product.structure.Text;
import com.omussd.generic.product.structure.TextPlaceholder;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.product.constant.IntentTypeConstant;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ScreenService.class, IntentService.class, ScreenComponentService.class,
    TextPlaceholderService.class, SessionService.class, DeterminateService.class, ProductRepository.class})
public class ProductStructureServiceTest {

    private Product product;
    private ProductStructure structure;
    private com.omussd.generic.db.entity.Intent intent;

    @Autowired
    private ProductStructureService productStructureService;

    @MockBean
    private ScreenService screenService;
    @MockBean
    private IntentService intentService;
    @MockBean
    private ScreenComponentService screenComponentService;
    @MockBean
    private TextPlaceholderService textPlaceholderService;
    @MockBean
    private SessionService sessionService;
    @MockBean
    private DeterminateService determinateService;
    @MockBean
    private ProductRepository productRepository;

    @TestConfiguration
    static class StructureServiceConfig {

        @Bean
        ProductStructureService getProductStructureService() {
            return new ProductStructureService();
        }
    }

    @Before
    public void init() throws Exception {
        product = EntityIdBuilder.getEntity(Product.class, 1);
        List<com.omussd.generic.db.entity.Screen> screens = new ArrayList<>();
        screens.add(EntityIdBuilder.getEntity(com.omussd.generic.db.entity.Screen.class, 1));
        product.setScreenCollection(screens);

        structure = getProductStructure();
        
        IntentType intentType = EntityIdBuilder.getEntity(IntentType.class, 1);
        intentType.setType(IntentTypeConstant.DETERMINATE.getIntentType());
        
        intent = new com.omussd.generic.db.entity.Intent();
        intent.setIntentTypeId(intentType);

        Mockito.when(screenService.createScreen(Mockito.any(Product.class), Mockito.anyString(), Mockito.any(com.omussd.generic.db.entity.Intent.class), Mockito.any(ScreenTypeConstant.class))).thenReturn(EntityIdBuilder.getEntity(com.omussd.generic.db.entity.Screen.class, 1));
        Mockito.when(screenComponentService.findOptionGroupComponentByName("!Null")).thenReturn(EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true));
        Mockito.when(screenComponentService.createOptionGroupComponent(Mockito.anyString(), Mockito.anyString(), Mockito.any(com.omussd.generic.db.entity.Intent.class), Mockito.any(OptionGroupComponentTypeConstant.class))).thenReturn(EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true));
        Mockito.when(intentService.createOrGetIntent(Mockito.any(Product.class), Mockito.any(com.omussd.generic.constant.IntentTypeConstant.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(intent);
    }

    /**
     * Test of replaceProductStructure method, of class ProductStructureService.
     */
    @Test
    public void testReplaceProductStructure() throws Exception {
        productStructureService.replaceProductStructure(product, structure);
    }

    private ProductStructure getProductStructure() {
        RootDeterminates rootDeterminates = new RootDeterminates();
        rootDeterminates.setDestination("rdDestination");
        rootDeterminates.setPurpose("rdPurpose");
        rootDeterminates.getDeterminates().add(getDeterminate());

        Input input = new Input();
        input.getIntents().add(getIntent(IntentTypeConstant.SUCCESS));
        input.getIntents().add(getIntent(IntentTypeConstant.DETERMINATE));
        input.getIntents().add(getIntent(IntentTypeConstant.SUBMIT));
        input.getIntents().add(getIntent(IntentTypeConstant.FAILURE));
        input.getIntents().add(getIntent(IntentTypeConstant.VALIDATION_FAILURE));
        input.setMaxLength(5);
        input.setMinLength(1);
        input.setParameterName("ParamName");
        input.setType(InputComponentTypeConstant.NUMBER.getType());

        Text text = new Text();
        text.setSequence(1);
        text.setTextString("TEXT");

        OptionGroup options = new OptionGroup();
        options.setSequence(2);
        options.setDestination("destination");
        options.setPurpose("purpose");
        options.setIntentValue("value");
        options.setParameterName("paramName");
        options.setType(OptionGroupComponentTypeConstant.STATIC.getType());
        options.getOptions().add(getOption(OptionTypeConstant.STATIC));
        options.getOptions().add(getOption(OptionTypeConstant.RETRY));
        options.getOptions().add(getOption(OptionTypeConstant.BACK));
        options.getOptions().add(getOption(OptionTypeConstant.EXIT));
        options.getOptions().add(getOption(OptionTypeConstant.HOME));

        Components components = new Components();
        components.setInput(input);
        components.getTexts().add(text);
        components.getOptions().add(options);

        TextPlaceholder placeholder = new TextPlaceholder();
        placeholder.setDestination("destination");
        placeholder.setPurpose("purpose");
        placeholder.setValue("value");
        placeholder.setVariable("variable");

        List<TextPlaceholder> placeholders = new ArrayList<>();
        placeholders.add(placeholder);

        Screen screen = new Screen();
        screen.setCode("0-0");
        screen.setFailureScreenCode("9-9");
        screen.setType(ScreenTypeConstant.ACTION.getCode());
        screen.setComponents(components);
        screen.setTextPlaceholders(placeholders);

        Screens screens = new Screens();
        screens.setProductName("TEST_PRODUCT");
        screens.getScreenList().add(screen);

        ProductStructure structure = new ProductStructure();
        structure.setRootDeterminates(rootDeterminates);
        structure.setScreens(screens);
        return structure;
    }

    private Intent getIntent(IntentTypeConstant type) {
        Intent intent = new Intent();
        if (IntentTypeConstant.SUCCESS.equals(type)) {
            intent.setScreenCode("0-0");
        } else {
            intent.setDestination("destination");
            intent.setPurpose("purpose");
            intent.setValue("value");
        }

        if (IntentTypeConstant.DETERMINATE.equals(type)) {
            intent.getDeterminates().add(getDeterminate());
        }
        return intent;
    }

    private Option getOption(OptionTypeConstant type) {
        Option option = new Option();

        option.setSeq(1);
        option.setNum(1);
        option.setText("Text");
        option.setType(type.getType());
        option.setValue("value");
        option.getIntents().add(getIntent(IntentTypeConstant.SUBMIT));

        return option;
    }

    private Determinate getDeterminate() {
        Determinate determinate = new Determinate();
        determinate.setScreenCode("0-0");
        determinate.setValue("rdValue");
        determinate.setValue("value");
        return determinate;
    }
}
