/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenType;
import com.omussd.generic.db.repository.ScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenRepository;
import com.omussd.generic.db.repository.ScreenScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenTypeRepository;
import com.omussd.generic.test.helper.EntityIdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ScreenRepository.class, ScreenTypeRepository.class,
    ProductService.class, ScreenDeterminateRepository.class,
    ScreenScreenDeterminateRepository.class})
public class ScreenServiceTest {

    private Intent intent;
    private Product product;
    private ScreenType screenType;
    private Screen screen;

    @Autowired
    private ScreenService screenService;

    @MockBean
    private ScreenRepository screenRepository;
    @MockBean
    private ScreenTypeRepository screenTypeRepository;
    @MockBean
    private ProductService productService;
    @MockBean
    private ScreenDeterminateRepository screenDeterminateRepository;
    @MockBean
    private ScreenScreenDeterminateRepository screenScreenDeterminateRepository;

    @TestConfiguration
    static class ScreenServiceConfig {

        @Bean
        ScreenService getScreenService() {
            return new ScreenService();
        }
    }

    @Before
    public void init() throws Exception {
        intent = EntityIdBuilder.getEntity(Intent.class, 1);

        screenType = EntityIdBuilder.getEntity(ScreenType.class, 1);
        screenType.setCode(ScreenTypeConstant.ACTION.getCode());

        product = EntityIdBuilder.getEntity(Product.class, 1);

        screen = EntityIdBuilder.getEntity(Screen.class, 1);
        screen.setCode(SpecialScreenCodesConstant.UNRECOGNISED_ROOT.getCode());
        screen.setProductId(product);
        screen.setScreenTypeId(screenType);
        screen.setIntentId(intent);

        Mockito.when(screenTypeRepository.findByCode(ScreenTypeConstant.ACTION.getCode())).thenReturn(screenType);
        Mockito.when(screenRepository.save(Mockito.any(Screen.class))).thenReturn(screen);
        Mockito.when(screenRepository.deleteAllByProduct(product)).thenReturn(1);
        Mockito.when(screenRepository.findByCodeAndProductIdIsNull(SpecialScreenCodesConstant.UNRECOGNISED_ROOT.getCode())).thenReturn(screen);
        Mockito.when(screenRepository.findByCodeAndProductId(Mockito.any(), Mockito.any(Product.class))).thenReturn(screen);
        Mockito.when(productService.getProductByName(Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString())).thenReturn(product);
    }

    /**
     * Test of createScreen method, of class ScreenService.
     */
    @Test
    public void testCreateScreen() {
        Screen result = screenService.createScreen(product, SpecialScreenCodesConstant.UNRECOGNISED_ROOT.getCode(),
                intent, ScreenTypeConstant.ACTION);
        boolean customEquality = screen.getCode().equals(result.getCode())
                && screen.getIntentId().equals(result.getIntentId())
                && screen.getProductId().equals(result.getProductId())
                && screen.getScreenTypeId().equals(result.getScreenTypeId());
        Assert.assertTrue(customEquality);
    }

    /**
     * Test of getScreenTypeByConstant method, of class ScreenService.
     */
    @Test
    public void testGetScreenTypeByConstant() {
        ScreenType result = screenService.getScreenTypeByConstant(ScreenTypeConstant.ACTION);
        Assert.assertTrue(screenType.getCode().equals(result.getCode()));
    }

    /**
     * Test of deleteAllScreensOfProduct method, of class ScreenService.
     */
    @Test
    public void testDeleteAllScreensOfProduct() {
        screenService.deleteAllScreensOfProduct(product);
    }

    /**
     * Test of getRootScreenForNullProduct method, of class ScreenService.
     */
    @Test
    public void testGetRootScreenForNullProduct() {
        screenService.getRootScreenForNullProduct();
    }

    /**
     * Test of getFailureScreen method, of class ScreenService.
     */
    @Test
    public void testGetFailureScreen() {
        Screen result = screenService.getFailureScreen(product);
        boolean customEquality = screen.getCode().equals(result.getCode())
                && screen.getIntentId().equals(result.getIntentId())
                && screen.getProductId().equals(result.getProductId())
                && screen.getScreenTypeId().equals(result.getScreenTypeId());
        Assert.assertTrue(customEquality);
    }

    /**
     * Test of getScreenByCodeAndProductName method, of class ScreenService.
     */
    @Test
    public void testGetScreenByCodeAndProductName() {
        Screen result = screenService.getScreenByCodeAndProductName("msisdn", screen.getCode(), "test");
        boolean customEquality = screen.getCode().equals(result.getCode())
                && screen.getIntentId().equals(result.getIntentId())
                && screen.getProductId().equals(result.getProductId())
                && screen.getScreenTypeId().equals(result.getScreenTypeId());
        Assert.assertTrue(customEquality);
    }
}
