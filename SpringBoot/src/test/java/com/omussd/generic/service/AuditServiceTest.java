/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.audit.AuditDataEntry;
import com.omussd.generic.constant.AuditDataEntryKey;
import com.omussd.generic.constant.AuditLogEvent;
import com.omussd.generic.constant.NetworkConstant;
import com.omussd.generic.db.entity.AuditLog;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Network;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.AuditLogEventRepository;
import com.omussd.generic.db.repository.AuditLogRepository;
import com.omussd.generic.db.repository.NetworkRepository;
import com.omussd.generic.test.helper.EntityIdBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {NetworkRepository.class, AuditLogRepository.class, AuditLogEventRepository.class})
public class AuditServiceTest {

    private Session session;
    private Network network;
    private Intent intent;
    private com.omussd.generic.db.entity.AuditLogEvent auditLogEvent;
    private AuditDataEntry[] dataEntries;
    private String msisdn;

    @Autowired
    private AuditService auditService;

    @MockBean
    private NetworkRepository networkRepository;

    @MockBean
    private AuditLogRepository auditLogRepository;

    @MockBean
    private AuditLogEventRepository auditLogEventRepository;

    @TestConfiguration
    static class AuditServiceConfguration {

        @Bean
        AuditService getAuditService() {
            return new AuditService();
        }
    }

    @Before
    public void init() throws Exception {
        session = EntityIdBuilder.getEntity(Session.class, 1);
        intent = EntityIdBuilder.getEntity(Intent.class, 1);

        network = EntityIdBuilder.getEntity(Network.class, 1);
        network.setName(NetworkConstant.MTN.getName());

        msisdn = "msisdn_test";

        auditLogEvent = EntityIdBuilder.getEntity(com.omussd.generic.db.entity.AuditLogEvent.class, 1);
        auditLogEvent.setEvent(AuditLogEvent.CREATED_NEW_SESSION.toString());

        dataEntries = new AuditDataEntry[]{
            AuditDataEntry.getEntry(AuditDataEntryKey.PROMPT, "prompt"),
            AuditDataEntry.getEntry(AuditDataEntryKey.SCREEN_CODE, "screen-code")
        };

        Mockito.when(networkRepository.findByName(NetworkConstant.MTN.getName())).thenReturn(network);
        Mockito.when(auditLogEventRepository.findByEvent(AuditLogEvent.CREATED_NEW_SESSION.toString())).thenReturn(auditLogEvent);
        Mockito.when(auditLogRepository.save(Mockito.any(AuditLog.class))).thenReturn(null);
    }

    @Test
    public void testLogAudit_2args() {
        auditService.logAudit(AuditLogEvent.CREATED_NEW_SESSION, session);
    }

    @Test
    public void testLogAudit_3args() {
        auditService.logAudit(AuditLogEvent.CREATED_NEW_SESSION, session, dataEntries);
    }

    @Test
    public void testLogAudit_4args() {
        auditService.logAudit(AuditLogEvent.CREATED_NEW_SESSION, intent, session, dataEntries);
    }

    @Test
    public void testLogAudit_5args_NetworkConstant() {
        auditService.logAudit(AuditLogEvent.CREATED_NEW_SESSION, intent, msisdn, NetworkConstant.MTN, dataEntries);
    }
}
