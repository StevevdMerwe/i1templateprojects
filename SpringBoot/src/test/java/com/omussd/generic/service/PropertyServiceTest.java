/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.db.entity.Property;
import com.omussd.generic.db.repository.PropertyRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {PropertyRepository.class})
public class PropertyServiceTest {

    private static final String TEST_STRING_VALUE = "TEST_STRING_VALUE";
    private static final String TEST_INTEGER_VALUE = "TEST_INTEGER_VALUE";
    private static final String TEST_LONG_VALUE = "TEST_LONG_VALUE";
    private static final String TEST_NULL_VALUE = "TEST_NULL_VALUE";

    private Property stringProperty;
    private Property integerProperty;
    private Property longProperty;

    @Autowired
    private PropertyService propertyService;

    @MockBean
    private PropertyRepository propertyRepository;

    @TestConfiguration
    static class PropertyServiceConfig {

        @Bean
        PropertyService getPropertyService() {
            return new PropertyService();
        }
    }

    @Before
    public void init() {
        stringProperty = getPropertyEntity(TEST_STRING_VALUE, "String");
        integerProperty = getPropertyEntity(TEST_INTEGER_VALUE, String.valueOf(Integer.MAX_VALUE));
        longProperty = getPropertyEntity(TEST_LONG_VALUE, String.valueOf(Long.MAX_VALUE));
        Mockito.when(propertyRepository.findByName(TEST_STRING_VALUE)).thenReturn(stringProperty);
        Mockito.when(propertyRepository.findByName(TEST_INTEGER_VALUE)).thenReturn(integerProperty);
        Mockito.when(propertyRepository.findByName(TEST_LONG_VALUE)).thenReturn(longProperty);
        Mockito.when(propertyRepository.findByName(TEST_NULL_VALUE)).thenReturn(null);
    }

    private Property getPropertyEntity(String name, String value) {
        Property prop = new Property();
        prop.setId(1);
        prop.setName(name);
        prop.setValue(value);
        prop.setDescription("This is a test");
        return prop;
    }

    @Test
    public void testFindIntegerValueByName() {
        Integer value = propertyService.findIntegerValueByName(TEST_INTEGER_VALUE);
        Assert.assertEquals(Integer.MAX_VALUE, (int) value);
    }

    @Test
    public void testFindNullIntegerValueByName() {
        Integer value = propertyService.findIntegerValueByName(TEST_NULL_VALUE);
        Assert.assertTrue(value == null);
    }

    @Test
    public void testFindLongValueByName() {
        Long value = propertyService.findLongValueByName(TEST_LONG_VALUE);
        Assert.assertEquals(Long.MAX_VALUE, (long) value);
    }

    @Test
    public void testFindNullLongValueByName() {
        Long value = propertyService.findLongValueByName(TEST_NULL_VALUE);
        Assert.assertTrue(value == null);
    }

    @Test
    public void testFindValueByName() {
        String value = propertyService.findValueByName(TEST_STRING_VALUE);
        Assert.assertEquals("String", value);
    }

    @Test
    public void testFindNullValueByName() {
        String value = propertyService.findValueByName(TEST_NULL_VALUE);
        Assert.assertTrue(value == null);
    }

}
