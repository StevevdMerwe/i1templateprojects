/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenDeterminate;
import com.omussd.generic.db.entity.ScreenScreenDeterminate;
import com.omussd.generic.db.repository.ScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenScreenDeterminateRepository;
import com.omussd.generic.test.helper.EntityIdBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ScreenDeterminateRepository.class, ScreenScreenDeterminateRepository.class})
public class DeterminateServiceTest {

    private ScreenScreenDeterminate ssd;
    private ScreenDeterminate sd;

    @Autowired
    private DeterminateService determinateService;

    @MockBean
    private ScreenDeterminateRepository screenDeterminateRepository;

    @MockBean
    private ScreenScreenDeterminateRepository screenScreenDeterminateRepository;

    @TestConfiguration
    static class DeterminateServiceConfig {

        @Bean
        DeterminateService getDetermianteService() {
            return new DeterminateService();
        }
    }

    @Before
    public void init() throws Exception {
        ssd = new ScreenScreenDeterminate();
        ssd.setIntentId(EntityIdBuilder.getEntity(Intent.class, 1));
        ssd.setScreenDeterminateId(EntityIdBuilder.getEntity(ScreenDeterminate.class, 1));
        ssd.setScreenId(EntityIdBuilder.getEntity(Screen.class, 1));

        sd = EntityIdBuilder.getEntity(ScreenDeterminate.class, 1);
        sd.setProductId(EntityIdBuilder.getEntity(Product.class, 1));

        Mockito.when(screenDeterminateRepository.findByProductIdAndValue(null, "value")).thenReturn(sd);
        Mockito.when(screenDeterminateRepository.findByProductIdAndValue(null, "valueNull")).thenReturn(null);
        Mockito.when(screenDeterminateRepository.save(Mockito.any(ScreenDeterminate.class))).thenReturn(sd);

        Mockito.when(screenScreenDeterminateRepository.save(Mockito.any(ScreenScreenDeterminate.class))).thenReturn(ssd);
    }

    @Test
    public void testCreateOrGetScreenDeterminateGet() {
        ScreenDeterminate screenDeterminate = determinateService.createOrGetScreenDeterminate(null, "value");
        Assert.assertEquals(sd, screenDeterminate);
    }

    @Test
    public void testCreateOrGetScreenDeterminateCreate() {
        ScreenDeterminate screenDeterminate = determinateService.createOrGetScreenDeterminate(null, "valueNull");
        Assert.assertEquals(sd, screenDeterminate);
    }

    @Test
    public void testCreateScreenScreenDeterminate() {
        ScreenScreenDeterminate screenScreenDeterminate = determinateService.createScreenScreenDeterminate(
                ssd.getScreenId(), ssd.getScreenDeterminateId(), ssd.getIntentId());
        Assert.assertEquals(ssd, screenScreenDeterminate);
    }
}
