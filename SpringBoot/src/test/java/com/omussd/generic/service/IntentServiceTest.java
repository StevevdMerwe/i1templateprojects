/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.util.XMLUtil;
import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.InputComponentType;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.IntentDestination;
import com.omussd.generic.db.entity.IntentPurpose;
import com.omussd.generic.db.entity.IntentType;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionGroupComponentType;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.entity.TextPlaceholder;
import com.omussd.generic.db.repository.IntentDestinationRepository;
import com.omussd.generic.db.repository.IntentPurposeRepository;
import com.omussd.generic.db.repository.IntentRepository;
import com.omussd.generic.db.repository.IntentTypeRepository;
import com.omussd.generic.db.repository.ScreenRepository;
import com.omussd.generic.db.repository.TextPlaceholderRepository;
import com.omussd.generic.product.GenericProduct;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.SessionHistory;
import com.omussd.generic.session.bean.DynamicIntent;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.product.intent.IntentResponse;
import com.omussd.product.intent.IntentResponseType;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {AuditService.class, IntentRepository.class, ScreenRepository.class,
    ScreenService.class, ScreenComponentService.class, ProductService.class, TextPlaceholderRepository.class,
    GenericProduct.class, IntentDestinationRepository.class, IntentPurposeRepository.class, IntentTypeRepository.class})
public class IntentServiceTest {

    private Screen screen;
    private Product product;
    private Intent intent;
    private Session session;
    private String msisdn;

    @Autowired
    private IntentService intentService;

    @MockBean
    private AuditService auditService;
    @MockBean
    private IntentRepository intentRepository;
    @MockBean
    private ScreenRepository screenRepository;
    @MockBean
    private ScreenService screenService;
    @MockBean
    private ScreenComponentService screenComponentService;
    @MockBean
    private ProductService productService;
    @MockBean
    private TextPlaceholderRepository textPlaceholderRepository;
    @MockBean
    private GenericProduct genericProduct;
    @MockBean
    private IntentDestinationRepository destinationRepository;
    @MockBean
    private IntentPurposeRepository purposeRepository;
    @MockBean
    private IntentTypeRepository intentTypeRepository;

    @TestConfiguration
    static class IntentServiceConfig {

        @Bean
        IntentService getIntentService() {
            return new IntentService();
        }
    }

    @Before
    public void init() throws Exception {
        msisdn = "MSISDN_TEST";
        intent = EntityIdBuilder.getEntity(Intent.class, 1);
        screen = EntityIdBuilder.getEntity(Screen.class, 1);
        screen.setIntentId(intent);
        product = EntityIdBuilder.getEntity(Product.class, 1);
        session = EntityIdBuilder.getEntity(Session.class, 1);
        session.setMsisdn(msisdn);

        Mockito.when(screenService.getScreenByCodeAndProduct(Mockito.anyString(), Mockito.eq(product))).thenReturn(screen);
        Mockito.when(intentRepository.findNonSubmitIntentOfInputComponentOfType(Mockito.any(InputComponent.class),
                Mockito.eq(IntentTypeConstant.VALIDATION_FAILURE))).thenReturn(intent);
    }

    /**
     * Test of processRedirectIntent method, of class IntentService.
     */
    @Test
    public void testProcessRedirectIntent() {
        Screen result = intentService.processRedirectIntent(intent, session, product);
        Assert.assertEquals(screen, result);
    }

    /**
     * Test of processValidationFailureIntent method, of class IntentService.
     */
    @Test
    public void testProcessValidationFailureIntent() throws Exception {
        Screen result = intentService.processValidationFailureIntent(screen,
                EntityIdBuilder.getEntity(InputComponent.class, 1, true),
                session, product);
        Assert.assertEquals(screen, result);

        result = intentService.processValidationFailureIntent(screen,
                EntityIdBuilder.getEntity(InputComponent.class, 2, true),
                session, product);
        Assert.assertEquals(screen, result);
    }

    /**
     * Test of processPreIntents method, of class IntentService.
     */
    @Test
    public void testProcessPreIntents() throws Exception {
        Intent successIntent = EntityIdBuilder.getEntity(Intent.class, 5);
        IntentType intentType = new IntentType();
        intentType.setType(IntentTypeConstant.SUCCESS.getIntentType());
        successIntent.setIntentTypeId(intentType);

        Intent determinateIntent = EntityIdBuilder.getEntity(Intent.class, 5);
        IntentType intentType2 = new IntentType();
        intentType2.setType(IntentTypeConstant.DETERMINATE.getIntentType());
        determinateIntent.setIntentTypeId(intentType2);

        List<Intent> staticSubmitIntents = new ArrayList<>();
        Intent submitIntent1 = EntityIdBuilder.getEntity(Intent.class, 3);
        Intent submitIntent2 = EntityIdBuilder.getEntity(Intent.class, 4);
        staticSubmitIntents.add(submitIntent1);
        staticSubmitIntents.add(submitIntent2);

        IntentResponse successIntentResponse = new IntentResponse();
        successIntentResponse.setType(com.omussd.product.constant.IntentTypeConstant.SUCCESS);
        successIntentResponse.setIntentResponseType(IntentResponseType.SUCCESS);
        successIntentResponse.setValue("value");

        Mockito.when(intentRepository.findSubmitIntentsOfOptionOnScreen(screen,
                1)).thenReturn(staticSubmitIntents);
        Mockito.when(intentRepository.findSubmitIntentsOfInputComponentOnScreen(screen))
                .thenReturn(staticSubmitIntents);
        Mockito.when(productService.callProductIntent(Mockito.any(Product.class), Mockito.any(Intent.class), Mockito.anyString()))
                .thenReturn(successIntentResponse);
        Mockito.when(intentRepository.findNonSubmitIntentOfInputComponentOfType(
                Mockito.any(InputComponent.class),
                Mockito.eq(IntentTypeConstant.SUCCESS))).thenReturn(successIntent);
        Mockito.when(intentRepository.findIntentOfOptionOnScreenOfType(Mockito.eq(screen),
                Mockito.anyInt(), Mockito.eq(IntentTypeConstant.SUCCESS))).thenReturn(successIntent);
        Mockito.when(intentRepository.findIntentOfInputComponentOnScreenOfType(Mockito.eq(screen),
                Mockito.eq(IntentTypeConstant.SUCCESS))).thenReturn(successIntent);

        SessionData sd = new SessionData();
        sd.addSessionHistory(new SessionHistory());
        sd.addDynamicOptionGroup(getDynamicOptionGroup());
        SessionDataCache.INSTANCE.put(msisdn, sd);

        OptionGroupComponentType type = EntityIdBuilder.getEntity(OptionGroupComponentType.class, 1);
        type.setType(OptionGroupComponentTypeConstant.STATIC.getType());
        OptionGroupComponent optionGroupComponent = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        optionGroupComponent.setOptionGroupComponentTypeId(type);

        //Static option group
        Screen result = intentService.processPreIntents(true, session, product,
                screen, "1", optionGroupComponent);
        Assert.assertEquals(screen, result);
        result = intentService.processPreIntents(true, session, product,
                screen, "2", optionGroupComponent);
        Assert.assertEquals(screen, result);

        //Dynamic option group
        type.setType(OptionGroupComponentTypeConstant.DYNAMIC.getType());
        OptionGroupComponent dynOptionGroupComponent = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        dynOptionGroupComponent.setOptionGroupComponentTypeId(type);
        result = intentService.processPreIntents(true, session, product,
                screen, "1", dynOptionGroupComponent);
        Assert.assertEquals(screen, result);
        result = intentService.processPreIntents(true, session, product,
                screen, "2", dynOptionGroupComponent);
        Assert.assertEquals(screen, result);

        //Input component
        InputComponent inputComponent = new InputComponent();
        inputComponent.setMinimumLength(1);
        inputComponent.setMaximumLength(1);
        InputComponentType ict = new InputComponentType();
        ict.setType(InputComponentTypeConstant.NUMBER.getType());
        inputComponent.setInputComponentTypeId(ict);
        result = intentService.processPreIntents(true, session, product,
                screen, "1", inputComponent);
        Assert.assertEquals(screen, result);
        result = intentService.processPreIntents(true, session, product,
                screen, "2", inputComponent);
        Assert.assertEquals(screen, result);

        Mockito.when(intentRepository.findIntentOfInputComponentOnScreenOfType(Mockito.eq(screen),
                Mockito.eq(IntentTypeConstant.DETERMINATE))).thenReturn(determinateIntent);
        Mockito.when(intentRepository.findSubmitIntentsOfInputComponentOnScreen(screen))
                .thenReturn(null);
        Mockito.when(screenRepository.findByScreenScreenDeterminateCollectionIntentIdAndScreenScreenDeterminateCollectionScreenDeterminateIdProductIdAndScreenScreenDeterminateCollectionScreenDeterminateIdValue(
                Mockito.any(Intent.class), Mockito.any(Product.class), Mockito.anyString())).thenReturn(screen);
        result = intentService.processPreIntents(true, session, product,
                screen, "1", inputComponent);
        Assert.assertEquals(screen, result);

    }

    private DynamicOptionGroup getDynamicOptionGroup() {
        DynamicOptionGroup dog = new DynamicOptionGroup();
        dog.setName("TEST_DOG");
        dog.setParameter("TEST_PARAMETER");

        List<DynamicIntent> dynamicIntents = new ArrayList<>();
        DynamicIntent dynamicIntent = new DynamicIntent();
        dynamicIntent.setDestination("TEST_DESTINATION");
        dynamicIntent.setPurpose("TEST_PURPOSE");
        dynamicIntent.setIntentType(IntentTypeConstant.SUBMIT.getIntentType());
        dynamicIntent.setValue("TEST_VALUE");
        dynamicIntents.add(dynamicIntent);

        List<DynamicOption> dynamicOptions = new ArrayList<>();
        DynamicOption dyno = new DynamicOption();
        dyno.setNumber(1);
        dyno.setSequence(1);
        dyno.setText("TEST_TEXT");
        dyno.setValue("TEST_VALUE");

        DynamicOption dyno2 = new DynamicOption();
        dyno2.setNumber(2);
        dyno2.setSequence(2);
        dyno2.setText("TEST_TEXT_2");
        dyno2.setValue("TEST_VALUE_2");
        dyno2.setDynamicIntents(dynamicIntents);

        dynamicOptions.add(dyno);
        dynamicOptions.add(dyno2);
        dog.setDynamicOptions(dynamicOptions);

        return dog;
    }

    /**
     * Test of processPostIntents method, of class IntentService.
     */
    @Test
    public void testProcessPostIntents() throws Exception {
        List<OptionGroupComponent> ogcs = new ArrayList<>();
        OptionGroupComponentType type = EntityIdBuilder.getEntity(OptionGroupComponentType.class, 1);
        type.setType(OptionGroupComponentTypeConstant.STATIC.getType());
        OptionGroupComponent optionGroupComponent = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        optionGroupComponent.setOptionGroupComponentTypeId(type);
        optionGroupComponent.setIntentId(intent);
        ogcs.add(optionGroupComponent);

        IntentResponse successIntentResponse = new IntentResponse();
        successIntentResponse.setType(com.omussd.product.constant.IntentTypeConstant.SUCCESS);
        successIntentResponse.setIntentResponseType(IntentResponseType.SUCCESS);
        successIntentResponse.setValue(XMLUtil.toXML(false, DynamicOptionGroup.class, getDynamicOptionGroup()));

        List<TextPlaceholder> placeholders = new ArrayList<>();
        TextPlaceholder placeholder = EntityIdBuilder.getEntity(TextPlaceholder.class, 1);
        placeholder.setIntentId(intent);
        placeholder.setVariable("variable");
        placeholders.add(placeholder);

        Mockito.when(screenComponentService.getDynamicOptionGroupComponentsByScreen(Mockito.any(Screen.class)))
                .thenReturn(ogcs);
        Mockito.when(productService.callProductIntent(Mockito.any(Product.class), Mockito.any(Intent.class), Mockito.anyString()))
                .thenReturn(successIntentResponse);
        Mockito.when(textPlaceholderRepository.findByScreenId(Mockito.any(Screen.class))).thenReturn(placeholders);

        SessionData sd = new SessionData();
        sd.addSessionHistory(new SessionHistory());
        sd.addDynamicOptionGroup(getDynamicOptionGroup());
        SessionDataCache.INSTANCE.put(msisdn, sd);

        intentService.processPostIntents(screen, session, product);
    }

    /**
     * Test of processRootDeterminateForProduct method, of class IntentService.
     */
    @Test
    public void testProcessRootDeterminateForProduct() throws Exception {
        IntentResponse successIntentResponse = new IntentResponse();
        successIntentResponse.setType(com.omussd.product.constant.IntentTypeConstant.SUCCESS);
        successIntentResponse.setIntentResponseType(IntentResponseType.SUCCESS);
        successIntentResponse.setValue(XMLUtil.toXML(false, DynamicOptionGroup.class, getDynamicOptionGroup()));

        Mockito.when(screenService.getRootScreenForNullProduct()).thenReturn(screen);
        Mockito.when(screenService.getFailureScreen(Mockito.any(Product.class))).thenReturn(screen);
        Mockito.when(productService.callProductIntent(Mockito.any(Product.class), Mockito.any(Intent.class), Mockito.anyString()))
                .thenReturn(successIntentResponse);
        Mockito.when(screenRepository.findByScreenScreenDeterminateCollectionIntentIdAndScreenScreenDeterminateCollectionScreenDeterminateIdProductIdAndScreenScreenDeterminateCollectionScreenDeterminateIdValue(
                Mockito.any(Intent.class), Mockito.any(Product.class), Mockito.anyString())).thenReturn(screen);

        Screen result = intentService.processRootDeterminateForProduct(product, session);
        Assert.assertEquals(screen, result);
        result = intentService.processRootDeterminateForProduct(null, session);
        Assert.assertEquals(screen, result);
    }

    /**
     * Test of createOrGetIntent method, of class IntentService.
     */
    @Test
    public void testCreateOrGetIntent() throws Exception {
        IntentType type = EntityIdBuilder.getEntity(IntentType.class, 1);
        type.setType(IntentTypeConstant.SUCCESS.getIntentType());
        
        Mockito.when(intentRepository.findIntentByProductAndDestinationAndPurposeAndValueAndType(
                Mockito.any(Product.class), Mockito.eq("!Null"), Mockito.anyString(), Mockito.anyString(),
                Mockito.any(IntentTypeConstant.class))).thenReturn(intent);
        Mockito.when(destinationRepository.findByName(Mockito.anyString())).thenReturn(EntityIdBuilder.getEntity(IntentDestination.class, 1));
        Mockito.when(purposeRepository.findByPurpose(Mockito.anyString())).thenReturn(EntityIdBuilder.getEntity(IntentPurpose.class, 1));
        Mockito.when(intentTypeRepository.findByType(Mockito.anyString())).thenReturn(type);
        Mockito.when(intentRepository.save(Mockito.any(Intent.class))).thenReturn(intent);
       
        Intent result = intentService.createOrGetIntent(product, IntentTypeConstant.SUCCESS, "!Null", "Purpose", "Value");
        Assert.assertEquals(intent, result);
       
        result = intentService.createOrGetIntent(product, IntentTypeConstant.SUCCESS, "Null", "Purpose", "Value");
        Assert.assertEquals(intent, result);
    }
}
