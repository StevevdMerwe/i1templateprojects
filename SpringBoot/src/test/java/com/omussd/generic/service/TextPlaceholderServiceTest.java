/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.TextPlaceholder;
import com.omussd.generic.db.repository.TextPlaceholderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {TextPlaceholderRepository.class})
public class TextPlaceholderServiceTest {

    private Screen screen;
    private String placeholderVariable;
    private Intent intent;
    private TextPlaceholder placeholder;

    @Autowired
    private TextPlaceholderService textPlaceholderService;

    @MockBean
    private TextPlaceholderRepository textPlaceholderRepository;

    @TestConfiguration
    static class TextPlaceholderServiceConfig {

        @Bean
        TextPlaceholderService getPlaceholderService() {
            return new TextPlaceholderService();
        }
    }

    @Before
    public void init() {
        screen = new Screen();
        screen.setId(1);

        intent = new Intent();
        intent.setId(1);

        placeholderVariable = "var";

        placeholder = new TextPlaceholder();
        placeholder.setIntentId(intent);
        placeholder.setScreenId(screen);
        placeholder.setVariable(placeholderVariable);

        Mockito.when(textPlaceholderRepository.deleteByScreenId(screen)).thenReturn(1);
        Mockito.when(textPlaceholderRepository.save(placeholder)).thenReturn(placeholder);
    }

    /**
     * Test of createTextPlaceholder method, of class TextPlaceholderService.
     */
    @Test
    public void testCreateTextPlaceholder() {
        TextPlaceholder textPlaceholder = textPlaceholderService.createTextPlaceholder(placeholderVariable, intent, screen);
        Assert.assertEquals(placeholder, textPlaceholder);
    }

    /**
     * Test of deleteByScreenId method, of class TextPlaceholderService.
     */
    @Test
    public void testDeleteByScreenId() {
        try {
            textPlaceholderService.deleteByScreenId(screen);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

}
