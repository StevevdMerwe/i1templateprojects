/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.ProductProperty;
import com.omussd.generic.db.repository.ProductPropertyRepository;
import com.omussd.generic.db.repository.ProductRepository;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.SessionHistory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {ProductRepository.class, ProductPropertyRepository.class})
@TestPropertySource(properties = {"ussd.product.root.name=ROOT"})
public class ProductServiceTest {

    private ProductProperty productProperty;
    private Product product;
    private String msisdn;
    private String nonRootProductNameInDb;
    private String nonRootProductNameNoDb;

    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private ProductPropertyRepository productPropertyRepository;

    @Value("${ussd.product.root.name}")
    private String rootProductName;

    @TestConfiguration
    static class ProductServiceConfig {

        @Bean
        ProductService getProductService() {
            return new ProductService();
        }
    }

    @Before
    public void init() throws Exception {
        nonRootProductNameInDb = "NOT_ROOT";
        nonRootProductNameNoDb = "NOT_ROOT_NO_DB";

        productProperty = EntityIdBuilder.getEntity(ProductProperty.class, 1);
        productProperty.setUrl("URL");

        product = EntityIdBuilder.getEntity(Product.class, 1);
        product.setName(rootProductName);
        product.setProductPropertyId(productProperty);

        msisdn = "MSISDN_TEST";
        SessionData data = new SessionData();
        SessionHistory history = new SessionHistory();
        history.setProductName(rootProductName);
        data.addSessionHistory(history);
        data.addParameter("TEST", "TEST");
        data.addToSessionMap(SessionDataConstant.PRODUCT_NAME, nonRootProductNameInDb);
        SessionDataCache.INSTANCE.put(msisdn, data);

        Mockito.when(productRepository.findByName(rootProductName)).thenReturn(null);
        Mockito.when(productRepository.findByName(nonRootProductNameNoDb)).thenReturn(null);
        Mockito.when(productRepository.findByName(nonRootProductNameInDb)).thenReturn(product);
        Mockito.when(productPropertyRepository.findByProductCollection(Mockito.any(Product.class))).thenReturn(productProperty);
    }

    /**
     * Test of getProductByNameFromDb method, of class ProductService.
     */
    @Test
    public void testGetProductByNameFromDb() {
        Product result = productService.getProductByNameFromDb(nonRootProductNameInDb);
        Assert.assertEquals(product.getName(), result.getName());
        result = productService.getProductByNameFromDb(nonRootProductNameNoDb);
        Assert.assertTrue(result == null);
    }

    /**
     * Test of getProductByName method, of class ProductService.
     */
    @Test
    public void testGetProductByName() {
        Product result = productService.getProductByName(true, msisdn, rootProductName);
        Assert.assertEquals(product.getName(), result.getName());
        result = productService.getProductByName(false, msisdn, rootProductName);
        Assert.assertTrue(result == null);
        result = productService.getProductByName(true, msisdn, nonRootProductNameNoDb);
        Assert.assertEquals(product.getName(), result.getName());
        result = productService.getProductByName(false, msisdn, nonRootProductNameNoDb);
        Assert.assertTrue(result == null);
        result = productService.getProductByName(true, msisdn, nonRootProductNameInDb);
        Assert.assertEquals(product.getName(), result.getName());
    }

    /**
     * Test of findPropductPropertyByProduct method, of class ProductService.
     */
    @Test
    public void testFindPropductPropertyByProduct() {
        ProductProperty result = productService.findPropductPropertyByProduct(null);
        Assert.assertTrue(result == null);
        result = productService.findPropductPropertyByProduct(product);
        Assert.assertEquals(productProperty, result);
    }

    /**
     * Test of findProductUrlByProduct method, of class ProductService.
     */
    @Test
    public void testFindProductUrlByProduct() {
        String result = productService.findProductUrlByProduct(product);
        Assert.assertEquals(productProperty.getUrl(), result);
    }

    /**
     * Test of isExpired method, of class ProductService.
     */
    @Test
    public void testIsExpired() {
        ProductProperty prop = new ProductProperty();
        Calendar endCal = GregorianCalendar.getInstance();

        //Expired
        endCal.set(Calendar.YEAR, endCal.get(Calendar.YEAR) - 1); //not hardcoded just in case we go back in time
        prop.setEndTime(endCal.getTime());
        boolean result = productService.isExpired(prop);
        Assert.assertTrue(result);

        //Not Expired
        endCal = GregorianCalendar.getInstance();
        endCal.set(Calendar.YEAR, endCal.get(Calendar.YEAR) + 1);
        prop.setEndTime(endCal.getTime());
        result = productService.isExpired(prop);
        Assert.assertTrue(!result);

        //Null case
        result = productService.isExpired(productProperty);
        Assert.assertTrue(!result);
    }

    /**
     * Test of hasStarted method, of class ProductService.
     */
    @Test
    public void testHasStarted() {
        ProductProperty prop = new ProductProperty();
        Calendar startCal = GregorianCalendar.getInstance();

        //Started
        startCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR) - 1); //not hardcoded just in case we go back in time
        prop.setStartTime(startCal.getTime());
        boolean result = productService.hasStarted(prop);
        Assert.assertTrue(result);

        //Not Started
        startCal = GregorianCalendar.getInstance();
        startCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR) + 2);
        prop.setStartTime(startCal.getTime());
        result = productService.hasStarted(prop);
        Assert.assertTrue(!result);

        //Null case
        result = productService.hasStarted(productProperty);
        Assert.assertTrue(result);
    }
}
