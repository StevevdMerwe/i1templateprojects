/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.OptionTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionGroupComponentType;
import com.omussd.generic.db.entity.OptionType;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.ScreenType;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.OptionRepository;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.SessionHistory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.ussdintegration.request.USSDAppRequest;
import com.omussd.ussdintegration.request.constant.Network;
import com.omussd.ussdintegration.request.constant.Type;
import com.omussd.ussdintegration.util.GenericIntegrationUtil;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = {SessionService.class, ProductService.class, ScreenService.class,
    IntentService.class, ScreenComponentService.class, OptionRepository.class})
public class USSDServiceTest {

    private Session session;
    private Product product;
    private Screen screen;

    @Autowired
    private USSDService ussdService;

    @MockBean
    private SessionService sessionService;
    @MockBean
    private ProductService productService;
    @MockBean
    private ScreenService screenService;
    @MockBean
    private IntentService intentService;
    @MockBean
    private ScreenComponentService screenComponentService;
    @MockBean
    private OptionRepository optionRepository;

    @TestConfiguration
    static class ussdServiceConfig {

        @Bean
        USSDService getUssdService() {
            return new USSDService();
        }
    }

    @Before
    public void init() throws Exception {
        com.omussd.generic.db.entity.Network networkEntity = EntityIdBuilder.getEntity(com.omussd.generic.db.entity.Network.class, 1);
        networkEntity.setName(Network.Telkom.name());

        session = EntityIdBuilder.getEntity(Session.class, 1);
        session.setMsisdn("1234");
        session.setNetworkId(networkEntity);

        product = EntityIdBuilder.getEntity(Product.class, 1);
        product.setName("TEST_PRODUCT");

        ScreenType screenType = EntityIdBuilder.getEntity(ScreenType.class, 1);
        screenType.setCode(ScreenTypeConstant.ACTION.getCode());

        screen = EntityIdBuilder.getEntity(Screen.class, 1);
        screen.setCode("0-0");
        screen.setProductId(product);
        screen.setScreenTypeId(screenType);

        List<Product> workingProducts = new ArrayList<>();
        workingProducts.add(product);

        Mockito.when(productService.getProductByName(Mockito.eq(Boolean.TRUE), Mockito.eq(session.getMsisdn()), Mockito.eq("TEST_PRODUCT"))).thenReturn(product);
        Mockito.when(productService.getAllWorkingProducts()).thenReturn(workingProducts);
        Mockito.when(productService.findProductFromSession(Mockito.anyString())).thenReturn(product);
        Mockito.when(intentService.processRootDeterminateForProduct(Mockito.any(Product.class), Mockito.any(Session.class))).thenReturn(screen);
        Mockito.when(intentService.processPreIntents(Mockito.anyBoolean(), Mockito.any(Session.class), Mockito.any(Product.class), Mockito.any(Screen.class), Mockito.anyString(), Mockito.any(ScreenComponent.class))).thenReturn(screen);
        Mockito.when(screenService.getScreenByCodeAndProduct(Mockito.anyString(), Mockito.any(Product.class))).thenReturn(screen);
        Mockito.when(screenComponentService.findOptionGroupComponentCotainingOptionWithNumberOnScreen(Mockito.anyInt(), Mockito.any(Screen.class))).thenReturn(getOptionGroupComponent());
        Mockito.when(screenComponentService.findOptionOnScreenWithNumber(Mockito.any(Screen.class), Mockito.eq(1))).thenReturn(getStaticOption());
        Mockito.when(screenComponentService.findOptionOnScreenWithNumber(Mockito.any(Screen.class), Mockito.eq(2))).thenReturn(getBackOption());
        Mockito.when(screenComponentService.findOptionOnScreenWithNumber(Mockito.any(Screen.class), Mockito.eq(3))).thenReturn(getRetryOption());
        Mockito.when(screenComponentService.findOptionOnScreenWithNumber(Mockito.any(Screen.class), Mockito.eq(4))).thenReturn(getHomeOption());
        Mockito.when(screenComponentService.findOptionOnScreenWithNumber(Mockito.any(Screen.class), Mockito.eq(5))).thenReturn(getExitOption());
        Mockito.when(optionRepository.findOptionBelongingToOptionGroupComponentWithNumber(Mockito.any(OptionGroupComponent.class), Mockito.anyInt())).thenReturn(getStaticOption());
        Mockito.when(sessionService.assignProductNameToSessionMapFromProductChoiceParameter(Mockito.any(SessionData.class))).thenReturn(getOldSessionData());
    }

    /**
     * Test of processUSSDRequest method, of class USSDService.
     */
    @Test
    public void testProcessUSSDRequestInit() throws Exception {
        //Initial, new, with higher than or equal to 10 range
        USSDAppRequest initRequest = getInitRequest(12);
        putSessionData(session.getMsisdn(), getEmptySessionData());
        Screen result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", initRequest);
        Assert.assertEquals(screen, result);

        //Initial, new, with lower than 10 range
        initRequest = getInitRequest(1);
        putSessionData(session.getMsisdn(), getEmptySessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", initRequest);
        Assert.assertEquals(screen, result);

        //Initial, old
        initRequest = getInitRequest(12);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", initRequest);
        Assert.assertEquals(screen, result);
    }

    @Test
    public void testProcessUSSDRequestAction() {
        //Action, no product
        USSDAppRequest actionRequest = getActionRequest(1, 1);
        putSessionData(session.getMsisdn(), getEmptySessionData());
        Screen result = ussdService.processUSSDRequest(session, "ROOT", actionRequest);
        Assert.assertEquals(screen, result);

        //Action, with product, STATIC Option
        actionRequest = getActionRequest(1, 1);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", actionRequest);
        Assert.assertEquals(screen, result);

        //Action, with product, BACK Option
        actionRequest = getActionRequest(1, 2);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", actionRequest);
        Assert.assertEquals(screen, result);

        //Action, with product, RETRY Option
        actionRequest = getActionRequest(1, 3);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", actionRequest);
        Assert.assertEquals(screen, result);

        //Action, with product, HOME Option
        actionRequest = getActionRequest(1, 4);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", actionRequest);
        Assert.assertEquals(screen, result);

        //Action, with product, EXIT Option
        actionRequest = getActionRequest(1, 5);
        putSessionData(session.getMsisdn(), getOldSessionData());
        result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", actionRequest);
        Assert.assertEquals(screen, result);
    }

    @Test
    public void testProcessUSSDRequestAbort() {
        USSDAppRequest abortRequest = getAbortRequest();
        putSessionData(session.getMsisdn(), getEmptySessionData());
        Screen result = ussdService.processUSSDRequest(session, "TEST_PRODUCT", abortRequest);
        Assert.assertTrue(result == null);
    }

    private OptionGroupComponent getOptionGroupComponent() throws Exception {
        OptionGroupComponentType ogct = EntityIdBuilder.getEntity(OptionGroupComponentType.class, 1);
        ogct.setType(OptionGroupComponentTypeConstant.STATIC.getType());

        OptionGroupComponent ogc = EntityIdBuilder.getEntity(OptionGroupComponent.class, 1, true);
        ogc.setName("NAME");
        ogc.setOptionGroupComponentTypeId(ogct);

        return ogc;
    }

    private Option getStaticOption() throws Exception {
        OptionType optionType = EntityIdBuilder.getEntity(OptionType.class, 1);
        optionType.setType(OptionTypeConstant.STATIC.getType());

        return getOption(optionType);
    }

    private Option getBackOption() throws Exception {
        OptionType optionType = EntityIdBuilder.getEntity(OptionType.class, 1);
        optionType.setType(OptionTypeConstant.BACK.getType());

        return getOption(optionType);
    }

    private Option getRetryOption() throws Exception {
        OptionType optionType = EntityIdBuilder.getEntity(OptionType.class, 1);
        optionType.setType(OptionTypeConstant.RETRY.getType());

        return getOption(optionType);
    }

    private Option getExitOption() throws Exception {
        OptionType optionType = EntityIdBuilder.getEntity(OptionType.class, 1);
        optionType.setType(OptionTypeConstant.EXIT.getType());

        return getOption(optionType);
    }

    private Option getHomeOption() throws Exception {
        OptionType optionType = EntityIdBuilder.getEntity(OptionType.class, 1);
        optionType.setType(OptionTypeConstant.HOME.getType());

        return getOption(optionType);
    }

    private Option getOption(OptionType optionType) throws Exception {
        Option option = EntityIdBuilder.getEntity(Option.class, 1);
        option.setName("option");
        option.setText("TEXT");
        option.setOptionTypeId(optionType);
        return option;
    }

    private USSDAppRequest getInitRequest(int range) {
        USSDAppRequest initRequest = new USSDAppRequest();
        initRequest.setParameter("parameter");
        initRequest.setCode(String.valueOf(range));
        initRequest.setMsisdn(session.getMsisdn());
        initRequest.setName("Name");
        initRequest.setNetwork(Network.Telkom);
        initRequest.setSessionId("SESSION_ID_TEST");
        initRequest.setType(Type.initial);
        GenericIntegrationUtil.setUserInputFromUSSDAppRequest(initRequest, "123");
        return initRequest;
    }

    private USSDAppRequest getActionRequest(int range, int userInput) {
        USSDAppRequest actionRequest = new USSDAppRequest();
        actionRequest.setParameter("parameter");
        actionRequest.setCode(String.valueOf(range));
        actionRequest.setMsisdn(session.getMsisdn());
        actionRequest.setName("Name");
        actionRequest.setNetwork(Network.Telkom);
        actionRequest.setSessionId("SESSION_ID_TEST");
        actionRequest.setType(Type.action);
        GenericIntegrationUtil.setUserInputFromUSSDAppRequest(actionRequest, String.valueOf(userInput));
        return actionRequest;
    }

    private USSDAppRequest getAbortRequest() {
        USSDAppRequest abortRequest = new USSDAppRequest();
        abortRequest.setParameter("parameter");
        abortRequest.setCode("12");
        abortRequest.setMsisdn(session.getMsisdn());
        abortRequest.setName("Name");
        abortRequest.setNetwork(Network.Telkom);
        abortRequest.setSessionId("SESSION_ID_TEST");
        abortRequest.setType(Type.abort);
        GenericIntegrationUtil.setUserInputFromUSSDAppRequest(abortRequest, "123");
        return abortRequest;
    }

    private SessionData getEmptySessionData() {
        SessionData sd = new SessionData();
        return sd;
    }

    private SessionData getOldSessionData() {
        SessionData sd = getEmptySessionData();
        SessionHistory sh = new SessionHistory();
        sh.setProductName("ROOT");
        sh.setScreenCode("0-0");
        sd.addSessionHistory(sh);
        sd.addToSessionMap(SessionDataConstant.INITIAL_DIALED_RANGE, "-1");
        return sd;
    }

    private void putSessionData(String msisdn, SessionData sessionData) {
        SessionDataCache.INSTANCE.put(msisdn, sessionData);
    }
}
