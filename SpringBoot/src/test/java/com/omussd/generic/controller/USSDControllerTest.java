/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.controller;

import com.google.gson.Gson;
import com.omussd.generic.constant.NetworkConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenType;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.parser.ScreenParser;
import com.omussd.generic.service.AuditService;
import com.omussd.generic.service.ScreenService;
import com.omussd.generic.service.SessionService;
import com.omussd.generic.service.USSDService;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionHistory;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.ussdintegration.request.USSDAppRequest;
import com.omussd.ussdintegration.request.constant.Network;
import com.omussd.ussdintegration.request.constant.ResponseState;
import com.omussd.ussdintegration.request.constant.ResponseType;
import com.omussd.ussdintegration.request.constant.Type;
import com.omussd.ussdintegration.response.USSDAppResponse;
import com.omussd.ussdintegration.util.GenericIntegrationUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(USSDController.class)
@AutoConfigureMockMvc
public class USSDControllerTest {

    private Session session;
    private USSDAppResponse response;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private USSDService ussdService;
    @MockBean
    private SessionService sessionService;
    @MockBean
    private ScreenParser screenParser;
    @MockBean
    private ScreenService screenService;
    @MockBean
    private AuditService auditService;

    @Before
    public void init() throws Exception {
        response = new USSDAppResponse();
        response.setPrompt("Hello");
        response.setResponseType(ResponseType.String);
        response.setState(ResponseState.NextState);

        session = new Session();
        session.setMsisdn("msisdn");

        Screen screen = EntityIdBuilder.getEntity(Screen.class, 1);
        ScreenType screenType = EntityIdBuilder.getEntity(ScreenType.class, 1);
        screenType.setCode(ScreenTypeConstant.ACTION.getCode());
        screen.setScreenTypeId(screenType);

        Mockito.when(sessionService.getSessionData(Mockito.any(Session.class))).thenReturn(getOldSessionData());
        Mockito.when(ussdService.processUSSDRequest(Mockito.any(Session.class), Mockito.anyString(), Mockito.any(USSDAppRequest.class))).thenReturn(screen);
        Mockito.when(screenParser.getAppResponseFromScreen(Mockito.any(Session.class), Mockito.any(Screen.class))).thenReturn(response);
        Mockito.when(sessionService.getSessionForMsisdnAndNetwork(Mockito.anyString(), Mockito.any(NetworkConstant.class))).thenReturn(session);
    }

    /**
     * Test of getUSSdAppResponse method, of class USSDController.
     */
    @Test
    public void testGetUSSdAppResponse() throws Exception {
        USSDAppRequest appRequest = getActionRequest(2, 123);
        Gson gson = new Gson();

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/ussd/TEST_PRODUCT")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(appRequest));
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().json(gson.toJson(response)));
    }

    private USSDAppRequest getActionRequest(int range, int userInput) {
        USSDAppRequest actionRequest = new USSDAppRequest();
        actionRequest.setParameter("parameter");
        actionRequest.setCode(String.valueOf(range));
        actionRequest.setMsisdn("msisdn");
        actionRequest.setName("Name");
        actionRequest.setNetwork(Network.Telkom);
        actionRequest.setSessionId("SESSION_ID_TEST");
        actionRequest.setType(Type.action);
        GenericIntegrationUtil.setUserInputFromUSSDAppRequest(actionRequest, String.valueOf(userInput));
        return actionRequest;
    }

    private SessionData getOldSessionData() {
        SessionData sd = getEmptySessionData();
        SessionHistory sh = new SessionHistory();
        sh.setProductName("ROOT");
        sh.setScreenCode("0-0");
        sd.addSessionHistory(sh);
        return sd;
    }

    private SessionData getEmptySessionData() {
        SessionData sd = new SessionData();
        return sd;
    }
}
