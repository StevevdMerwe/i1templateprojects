/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.controller;

import com.omussd.common.util.XMLUtil;
import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.OptionTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.product.structure.Components;
import com.omussd.generic.product.structure.Determinate;
import com.omussd.generic.product.structure.Input;
import com.omussd.generic.product.structure.Intent;
import com.omussd.generic.product.structure.Option;
import com.omussd.generic.product.structure.OptionGroup;
import com.omussd.generic.product.structure.ProductStructure;
import com.omussd.generic.product.structure.RootDeterminates;
import com.omussd.generic.product.structure.Screen;
import com.omussd.generic.product.structure.Screens;
import com.omussd.generic.product.structure.Text;
import com.omussd.generic.product.structure.TextPlaceholder;
import com.omussd.generic.request.ProductStructureRequest;
import com.omussd.generic.response.ProductStructureResponse;
import com.omussd.generic.service.ProductService;
import com.omussd.generic.service.ProductStructureService;
import com.omussd.generic.test.helper.EntityIdBuilder;
import com.omussd.product.constant.IntentTypeConstant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author XY40428
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;
    @MockBean
    private ProductStructureService productStructureService;

    @Before
    public void init() throws Exception {
        Product product = EntityIdBuilder.getEntity(Product.class, 1);
        product.setProductKey("$2a$10$ka3KdxVGznyNbcC0pW7xzONjV.6zRmWA6mYNbJdqyJ2qTWpE19mzq"); //ZgyA$Z*#j7/Fp2/C
        Mockito.when(productService.getProductByNameFromDb("TEST")).thenReturn(product);
    }

    /**
     * Test of registerProduct method, of class ProductController.
     */
    @Test
    public void testRegisterProduct() throws Exception {
        ProductStructure structure = getProductStructure();
        ProductStructureRequest request = new ProductStructureRequest();
        request.setProductStructure(structure);

        ProductStructureResponse successResponse = new ProductStructureResponse();
        successResponse.setSuccess(true);
        String successXml = XMLUtil.toXML(true, ProductStructureResponse.class, successResponse);

        ProductStructureResponse failureResponse = new ProductStructureResponse();
        failureResponse.setSuccess(false);
        String failureXml = XMLUtil.toXML(true, ProductStructureResponse.class, failureResponse);

        //Without xml header, correct password
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/product/structure")
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .header("Authorization", getAuthorizationHeader(true, true))
                .content(XMLUtil.toXML(false, ProductStructureRequest.class, request));
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.content().xml(successXml));

        //With xml header, correct password
        builder = MockMvcRequestBuilders.post("/product/structure")
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .header("Authorization", getAuthorizationHeader(true, true))
                .content(XMLUtil.toXML(true, ProductStructureRequest.class, request));
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.content().xml(successXml));

        //wrong password
        builder = MockMvcRequestBuilders.post("/product/structure")
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .header("Authorization", getAuthorizationHeader(true, false))
                .content(XMLUtil.toXML(true, ProductStructureRequest.class, request));
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.content().xml(failureXml));

        //null product
        builder = MockMvcRequestBuilders.post("/product/structure")
                .accept(MediaType.APPLICATION_XML)
                .contentType(MediaType.APPLICATION_XML)
                .header("Authorization", getAuthorizationHeader(false, true))
                .content(XMLUtil.toXML(true, ProductStructureRequest.class, request));
        mvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isUnauthorized())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
                .andExpect(MockMvcResultMatchers.content().xml(failureXml));
    }

    private String getAuthorizationHeader(boolean fulfilledProduct, boolean correctPassword) {
        String product = fulfilledProduct ? "TEST" : "NO";
        String password = correctPassword ? "ZgyA$Z*#j7/Fp2/C" : "NO";
        String authCombo = product + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(authCombo.getBytes());
    }

    private ProductStructure getProductStructure() {
        RootDeterminates rootDeterminates = new RootDeterminates();
        rootDeterminates.setDestination("rdDestination");
        rootDeterminates.setPurpose("rdPurpose");
        rootDeterminates.getDeterminates().add(getDeterminate());

        Input input = new Input();
        input.getIntents().add(getIntent(IntentTypeConstant.SUCCESS));
        input.getIntents().add(getIntent(IntentTypeConstant.DETERMINATE));
        input.getIntents().add(getIntent(IntentTypeConstant.SUBMIT));
        input.getIntents().add(getIntent(IntentTypeConstant.FAILURE));
        input.getIntents().add(getIntent(IntentTypeConstant.VALIDATION_FAILURE));
        input.setMaxLength(5);
        input.setMinLength(1);
        input.setParameterName("ParamName");
        input.setType(InputComponentTypeConstant.NUMBER.getType());

        Text text = new Text();
        text.setSequence(1);
        text.setTextString("TEXT");

        OptionGroup options = new OptionGroup();
        options.setSequence(2);
        options.setDestination("destination");
        options.setPurpose("purpose");
        options.setIntentValue("value");
        options.setParameterName("paramName");
        options.setType(OptionGroupComponentTypeConstant.STATIC.getType());
        options.getOptions().add(getOption(OptionTypeConstant.STATIC));
        options.getOptions().add(getOption(OptionTypeConstant.RETRY));
        options.getOptions().add(getOption(OptionTypeConstant.BACK));
        options.getOptions().add(getOption(OptionTypeConstant.EXIT));
        options.getOptions().add(getOption(OptionTypeConstant.HOME));

        Components components = new Components();
        components.setInput(input);
        components.getTexts().add(text);
        components.getOptions().add(options);

        TextPlaceholder placeholder = new TextPlaceholder();
        placeholder.setDestination("destination");
        placeholder.setPurpose("purpose");
        placeholder.setValue("value");
        placeholder.setVariable("variable");

        List<TextPlaceholder> placeholders = new ArrayList<>();
        placeholders.add(placeholder);

        Screen screen = new Screen();
        screen.setCode("0-0");
        screen.setFailureScreenCode("9-9");
        screen.setType(ScreenTypeConstant.ACTION.getCode());
        screen.setComponents(components);
        screen.setTextPlaceholders(placeholders);

        Screens screens = new Screens();
        screens.setProductName("TEST_PRODUCT");
        screens.getScreenList().add(screen);

        ProductStructure structure = new ProductStructure();
        structure.setRootDeterminates(rootDeterminates);
        structure.setScreens(screens);
        return structure;
    }

    private Intent getIntent(IntentTypeConstant type) {
        Intent intent = new Intent();
        if (IntentTypeConstant.SUCCESS.equals(type)) {
            intent.setScreenCode("0-0");
        } else {
            intent.setDestination("destination");
            intent.setPurpose("purpose");
            intent.setValue("value");
        }

        if (IntentTypeConstant.DETERMINATE.equals(type)) {
            intent.getDeterminates().add(getDeterminate());
        }
        return intent;
    }

    private Option getOption(OptionTypeConstant type) {
        Option option = new Option();

        option.setSeq(1);
        option.setNum(1);
        option.setText("Text");
        option.setType(type.getType());
        option.setValue("value");
        option.getIntents().add(getIntent(IntentTypeConstant.SUBMIT));

        return option;
    }

    private Determinate getDeterminate() {
        Determinate determinate = new Determinate();
        determinate.setScreenCode("0-0");
        determinate.setValue("rdValue");
        determinate.setValue("value");
        return determinate;
    }

}
