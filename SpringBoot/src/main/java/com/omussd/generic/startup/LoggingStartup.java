/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.startup;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import com.omussd.common.log.LoggingUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author XY40428
 */
@Component
public class LoggingStartup {

    private final Logger logger = LoggerFactory.getLogger(LoggingStartup.class);

    @Value("${logging.properties}")
    private String loggingProperties;

    public void initialiseLogging(String applicationFolder) throws IOException, JoranException {
        String loggingConf = applicationFolder + File.separator + loggingProperties;
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        try (InputStream configStream = new FileInputStream(loggingConf)) {
            configurator.setContext(loggerContext);
            configurator.doConfigure(configStream); // loads logback file
        }
        LoggingUtil.logInfo(logger, "Logging initialised");
    }
}
