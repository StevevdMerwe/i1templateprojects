/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.startup;

import com.omussd.generic.constant.PropertyConstant;
import com.omussd.generic.runnable.SessionCleanupRunnable;
import com.omussd.generic.service.PropertyService;
import com.omussd.generic.service.SessionService;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

/**
 *
 * @author XY40428
 */
@Component
public class ScheduleService {

    @Autowired
    private PropertyService propertyService;
    @Autowired
    private SessionService sessionService;
    
    private final List<ScheduledFuture> globalScheduledFutures = new ArrayList<>();

    public void startGlobalSchedulers() {
        long sessionCleanupDelayPeriod = propertyService.findLongValueByName(
                PropertyConstant.SESSION_CLEANUP_DELAY_PERIOD.toString());

        Date nowPlusPeriod = GregorianCalendar.getInstance().getTime();
        nowPlusPeriod.setTime(nowPlusPeriod.getTime() + sessionCleanupDelayPeriod);
        
        TaskScheduler taskScheduler = new ConcurrentTaskScheduler();
        ScheduledFuture sessionCleanup = taskScheduler.scheduleWithFixedDelay(
                new SessionCleanupRunnable(sessionService, propertyService),
                nowPlusPeriod, sessionCleanupDelayPeriod);

        globalScheduledFutures.add(sessionCleanup);
    }

    public void cancelGlobalSchedulers() {
        for (ScheduledFuture scheduledFuture : globalScheduledFutures) {
            scheduledFuture.cancel(false);
        }
        globalScheduledFutures.clear();
    }
}
