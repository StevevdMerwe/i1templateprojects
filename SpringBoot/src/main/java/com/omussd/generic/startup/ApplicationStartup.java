package com.omussd.generic.startup;

import ch.qos.logback.core.joran.spi.JoranException;
import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.liquibase.startup.LiquibaseStartup;
import com.omussd.generic.service.SessionService;
import java.io.IOException;
import java.sql.SQLException;
import liquibase.exception.LiquibaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author XY40428
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationStartup.class);

    @Autowired
    private ConfigurableApplicationContext context;
    @Autowired
    private LoggingStartup loggingStartup;
    @Autowired
    private LiquibaseStartup liquibaseStartup;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private SessionService sessionService;

    @Value("${application.folder}")
    private String applicationFolder;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent e) {
        //This is before logs, so use println
        System.out.println("Applicaton Startup initiated");
        try {
            initialiseLogging();
        } catch (IOException | JoranException ex) {
            //Don't try to use logger here, the logs failed to start up
            System.out.println("Initialising logs failed.");
            ex.printStackTrace(System.out);
            context.close();
        }

        LoggingUtil.logInfo(logger, "Application Startup in progress (post logging initialisation)");

        try {
            initialiseLiquibase();
        } catch (SQLException | LiquibaseException ex) {
            LoggingUtil.logError(logger, ex, "Failed to run USSD Generic liquibase");
            context.close();
            return;
        }

        scheduleService.startGlobalSchedulers();

        sessionService.deleteAll();

        LoggingUtil.logInfo(logger, "Application Startup Complete");
    }

    private void initialiseLogging() throws IOException, JoranException {
        loggingStartup.initialiseLogging(applicationFolder);
    }

    private void initialiseLiquibase() throws SQLException, LiquibaseException {
        liquibaseStartup.initialiseLiquibaseUpdates();
    }
}