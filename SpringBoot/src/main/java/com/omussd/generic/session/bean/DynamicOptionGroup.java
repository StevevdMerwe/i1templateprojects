/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.session.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XY40428
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DynamicOptionGroup {

    @XmlElement
    private String name;
    @XmlElement
    private String parameter;
    @XmlElement(name = "dynamicOpton")
    private List<DynamicOption> dynamicOptions;

    public DynamicOptionGroup() {
        dynamicOptions = new ArrayList<>();
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public List<DynamicOption> getDynamicOptions() {
        return dynamicOptions;
    }

    public void setDynamicOptions(List<DynamicOption> dynamicOptions) {
        this.dynamicOptions = dynamicOptions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DynamicOptionGroup{parameter=" + parameter + ", dynamicOption=" + dynamicOptions + '}';
    }
}
