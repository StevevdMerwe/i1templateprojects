/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.session.bean;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DynamicIntent {

    @XmlElement
    private String destination;
    @XmlElement
    private String purpose;
    @XmlElement
    private String value;
    @XmlElement
    private String intentType;
    @XmlElement(name = "determinate")
    private List<DynamicDeterminate> determinates;
}
