package com.omussd.generic.session;

import com.omussd.common.xml.MapAdapter;
import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@ToString
@EqualsAndHashCode(doNotUseGetters = true, exclude = "sessionHistoryStack")
@XmlRootElement(name = "sessionData")
@XmlAccessorType(XmlAccessType.FIELD)
public class SessionData implements Serializable {

    @XmlElement
    private Deque<SessionHistory> sessionHistoryStack;

    @XmlJavaTypeAdapter(MapAdapter.class)
    private final Map<String, String> parameters;

    @XmlJavaTypeAdapter(MapAdapter.class)
    private Map<String, String> sessionMap;

    @XmlElement
    private List<DynamicOptionGroup> dynamicOptionGroups;

    @XmlJavaTypeAdapter(MapAdapter.class)
    private Map<String, String> textVariables;

    public SessionData() {
        sessionMap = new HashMap<>();
        parameters = new HashMap<>();
        sessionHistoryStack = new ArrayDeque<>();
        dynamicOptionGroups = new ArrayList<>();
        textVariables = new HashMap<>();
    }
    
    public String getFirstScreenCode() {
        SessionHistory history = sessionHistoryStack.getLast();
        return history.getScreenCode();
    }
    
    public void addTextVariable(String name, String value) {
        textVariables.put(name, value);
    }

    public void addDynamicOptionGroup(DynamicOptionGroup dynamicOptionGroup) {
        dynamicOptionGroups.add(dynamicOptionGroup);
    }

    public Map<String, String> getParamtersAsMap() {
        return parameters;
    }

    public List<DynamicOptionGroup> getDynamicOptionGroups() {
        return dynamicOptionGroups;
    }

    public void setDynamicOptionGroups(List<DynamicOptionGroup> dynamicOptionGroups) {
        this.dynamicOptionGroups = dynamicOptionGroups;
    }

    public Map<String, String> getTextVariables() {
        return textVariables;
    }

    public boolean hasEmptyHistory() {
        return sessionHistoryStack.isEmpty();
    }
    
    public int getHistorySize() {
        return sessionHistoryStack.size();
    }

    public void addToSessionMap(SessionDataConstant sdc, String value) {
        sessionMap.put(sdc.toString(), value);
    }

    public String getFromSessionMap(SessionDataConstant sdc) {
        return sessionMap.get(sdc.toString());
    }

    public void removeFromSessionMap(SessionDataConstant sdc) {
        sessionMap.remove(sdc.toString());
    }
    
    public void addSessionHistory(SessionHistory sessionHistory) {
        sessionHistoryStack.push(sessionHistory);
    }

    public SessionHistory popSessionHistory() {
        SessionHistory sh = sessionHistoryStack.pop();
        List<String> params = sh.getParameters();
        for (String param: params) {
            parameters.remove(param);
        }
        return sh;
    }

    public SessionHistory peekSessionHistory() {
        return sessionHistoryStack.peek();
    }

    public void addParameter(String parameter, String value) {
        SessionHistory history = peekSessionHistory();
        history.addParameter(parameter);

        parameters.put(parameter, value);
    }
    
    public String getParameter(String parameter) {
        return parameters.get(parameter);
    }

    public void overrideParameters(Map<String, String> newParameters) {
        Map<String, String> difference = new HashMap<>();
        difference.putAll(newParameters);
        difference.putAll(parameters);
        difference.entrySet().removeAll(parameters.entrySet());
        
        for (Map.Entry<String, String> entry: difference.entrySet()) {
            addParameter(entry.getKey(), entry.getValue());
        }
        
        parameters.putAll(newParameters);
    }

    public void clearData() {
        sessionHistoryStack.clear();
        sessionMap.clear();
        dynamicOptionGroups.clear();
        parameters.clear();
    }

    public void clearHistoryStack() {
        sessionHistoryStack.clear();
        parameters.clear();
    }

    public void clearTextVariables() {
        textVariables.clear();
    }

    public void clearDynamicOptionGroups() {
        dynamicOptionGroups.clear();
    }
}
