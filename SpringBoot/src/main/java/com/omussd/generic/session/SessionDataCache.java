/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.session;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author XY40428
 */
public enum SessionDataCache {
    INSTANCE;
    
    private final Map<String, SessionData> cache;
    
    private SessionDataCache() {
        cache = new HashMap<>(100);
    }
    
    public void put(String msisdn, SessionData sessionData) {
        cache.put(msisdn, sessionData);
    }
    
    public SessionData getSessionData(String msisdn) {
        return cache.get(msisdn);
    }
    
    public void removeSessionData(String msisdn) {
        cache.remove(msisdn);
    }
}
