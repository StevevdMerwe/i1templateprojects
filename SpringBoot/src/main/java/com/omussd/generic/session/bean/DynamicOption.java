/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.session.bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XY40428
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DynamicOption {
    @XmlElement
    private int sequence;
    @XmlElement
    private int number;
    @XmlElement
    private String text;
    @XmlElement(name = "dynamicIntent")
    private List<DynamicIntent> dynamicIntents;
    @XmlElement
    private String value;

    public DynamicOption() {
        dynamicIntents = new ArrayList<>();
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<DynamicIntent> getDynamicIntents() {
        return dynamicIntents;
    }

    public void setDynamicIntents(List<DynamicIntent> dynamicIntents) {
        this.dynamicIntents = dynamicIntents;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DynamicOption{" + "sequence=" + sequence + ", number=" + number + ", text=" + text + ", dynamicIntents=" + dynamicIntents + ", value=" + value + '}';
    }
}
