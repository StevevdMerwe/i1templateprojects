package com.omussd.generic.runnable;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.PropertyConstant;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.service.PropertyService;
import com.omussd.generic.service.SessionService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author XY40428
 */
public class SessionCleanupRunnable implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(SessionCleanupRunnable.class);

    private final SessionService sessionService;
    private final PropertyService propertyService;

    public SessionCleanupRunnable(SessionService sessionService, PropertyService propertyService) {
        this.sessionService = sessionService;
        this.propertyService = propertyService;
    }

    @Override
    public void run() {
        LoggingUtil.logInfo(logger, "SessionCleanupRunnable started");
        Long sessionTTL = propertyService.findLongValueByName(PropertyConstant.SESSION_TTL.toString());
        List<Session> sessions = sessionService.findAll();
        for (Session session : sessions) {
            if (!session.getLocked() && sessionService.isSessionExpired(session, sessionTTL)) {
                sessionService.deleteSessionByMsisdn(session.getMsisdn());
            }
        }
        LoggingUtil.logInfo(logger, "SessionCleanupRunnable exiting");
    }
}
