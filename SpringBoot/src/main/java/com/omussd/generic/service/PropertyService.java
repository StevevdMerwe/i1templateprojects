package com.omussd.generic.service;

import com.omussd.generic.db.entity.Property;
import com.omussd.generic.db.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that works with the properties stored in the database
 *
 * @author XY40428
 */
@Component
public class PropertyService {

    @Autowired
    private PropertyRepository propertyRepository;

    public Integer findIntegerValueByName(String name) {
        String value = findValueByName(name);
        if (value == null) {
            return null;
        }
        return Integer.valueOf(value);
    }

    public Long findLongValueByName(String name) {
        String value = findValueByName(name);
        if (value == null) {
            return null;
        }
        return Long.valueOf(value);
    }

    public String findValueByName(String name) {
        Property property = propertyRepository.findByName(name);
        return property == null ? null : property.getValue();
    }
}
