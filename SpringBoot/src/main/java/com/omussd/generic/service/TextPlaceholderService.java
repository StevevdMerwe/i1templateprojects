/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.repository.TextPlaceholderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processing around the text placeholders
 *
 * @author XY40428
 */
@Component
public class TextPlaceholderService {

    private static final Logger logger = LoggerFactory.getLogger(TextPlaceholderService.class);

    @Autowired
    private TextPlaceholderRepository textPlaceholderRepository;

    public com.omussd.generic.db.entity.TextPlaceholder createTextPlaceholder(String variable, com.omussd.generic.db.entity.Intent intent, com.omussd.generic.db.entity.Screen screen) {
        com.omussd.generic.db.entity.TextPlaceholder holder = new com.omussd.generic.db.entity.TextPlaceholder();
        holder.setIntentId(intent);
        holder.setVariable(variable);
        holder.setScreenId(screen);
        return textPlaceholderRepository.save(holder);
    }

    public void deleteByScreenId(Screen screen) {
        Integer rows = textPlaceholderRepository.deleteByScreenId(screen);
        LoggingUtil.logInfo(logger, "Deleted textPlaceholders by screen: ", screen, " with ", rows, " rows affected");
    }
}
