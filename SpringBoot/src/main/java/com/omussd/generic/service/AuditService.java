/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.common.util.XMLUtil;
import com.omussd.generic.audit.AuditData;
import com.omussd.generic.audit.AuditDataEntry;
import com.omussd.generic.constant.AuditLogEvent;
import com.omussd.generic.constant.NetworkConstant;
import com.omussd.generic.db.entity.AuditLog;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Network;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.AuditLogEventRepository;
import com.omussd.generic.db.repository.AuditLogRepository;
import com.omussd.generic.db.repository.NetworkRepository;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles the auditing processes
 *
 * @author XY40428
 */
@Component
public class AuditService {

    private Logger logger = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private NetworkRepository network;

    @Autowired
    private AuditLogRepository auditLogRepository;

    @Autowired
    private AuditLogEventRepository auditLogEventRepository;

    /**
     * Processes an audit log
     *
     * @param auditLogEvent The event to be logged
     * @param session The session that instigated the event
     */
    public void logAudit(AuditLogEvent auditLogEvent, Session session) {
        logAudit(auditLogEvent, null, session, null);
    }

    /**
     * Processes an audit log
     *
     * @param auditLogEvent The event to be logged
     * @param session The session that instigated the event
     * @param entries Additional data entries that is relevant to the event
     */
    public void logAudit(AuditLogEvent auditLogEvent, Session session, AuditDataEntry[] entries) {
        logAudit(auditLogEvent, null, session, entries);
    }

    /**
     * Processes an audit log
     *
     * @param auditLogEvent The event to be logged
     * @param intent The intent that belongs to the event
     * @param session The session that instigated the event
     * @param entries Additional data entries that is relevant to the event
     */
    public void logAudit(AuditLogEvent auditLogEvent, Intent intent, Session session, AuditDataEntry[] entries) {
        logAudit(auditLogEvent, intent, session.getMsisdn(), session.getNetworkId(), entries);
    }

    /**
     * Processes an audit log
     *
     * @param auditLogEvent The event to be logged
     * @param intent The intent that belongs to the event
     * @param msisdn The msisdn that made the initial request for processing
     * @param networkConstant The network that the accompanied the msisdn that
     * made the initial request for processing
     * @param entries Additional data entries that is relevant to the event
     */
    public void logAudit(AuditLogEvent auditLogEvent, Intent intent, String msisdn, NetworkConstant networkConstant, AuditDataEntry[] entries) {
        logAudit(auditLogEvent, intent, msisdn, network.findByName(networkConstant.getName()), entries);
    }

    private void logAudit(AuditLogEvent auditLogEvent, Intent intent, String msisdn, Network network, AuditDataEntry[] entries) {
        LoggingUtil.logTrace(logger, "logAudit entered. Event: ", auditLogEvent);
        AuditData data = new AuditData();
        if (entries != null) {
            data.addAuditDataEntries(entries);
        }

        Date logTime = GregorianCalendar.getInstance().getTime();

        AuditLog auditLog = new AuditLog();
        auditLog.setAuditLogEventId(auditLogEventRepository.findByEvent(auditLogEvent.toString()));
        try {
            String auditDataXml = XMLUtil.toXML(false, AuditData.class, data);
            auditLog.setAdditionalData(auditDataXml);
        } catch (JAXBException e) {
            LoggingUtil.logError(logger, e, "Failed to parse Audit Data additional data xml: ", data);
        }
        auditLog.setIntentId(intent);
        auditLog.setLogTime(logTime);
        auditLog.setMsisdn(msisdn);
        auditLog.setNetworkId(network);

        auditLogRepository.save(auditLog);
        LoggingUtil.logTrace(logger, "logAudit exiting Event: ", auditLogEvent);
    }
}
