package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.OptionTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.ScreenDeterminate;
import com.omussd.generic.db.entity.ScreenScreenComponent;
import com.omussd.generic.db.entity.ScreenScreenDeterminate;
import com.omussd.generic.db.entity.TextComponent;
import com.omussd.generic.db.repository.ProductRepository;
import com.omussd.generic.product.structure.Components;
import com.omussd.generic.product.structure.Determinate;
import com.omussd.generic.product.structure.Input;
import com.omussd.generic.product.structure.Intent;
import com.omussd.generic.product.structure.Option;
import com.omussd.generic.product.structure.OptionGroup;
import com.omussd.generic.product.structure.ProductStructure;
import com.omussd.generic.product.structure.RootDeterminates;
import com.omussd.generic.product.structure.Screen;
import com.omussd.generic.product.structure.Screens;
import com.omussd.generic.product.structure.Text;
import com.omussd.generic.product.structure.TextPlaceholder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processes around the product structure. The product
 * structure being the defined intents, text placeholders, and screens that a
 * product uses
 *
 * @author XY40428
 */
@Component
public class ProductStructureService {

    private static final Logger logger = LoggerFactory.getLogger(ProductStructureService.class);

    @Autowired
    private ScreenService screenService;
    @Autowired
    private IntentService intentService;
    @Autowired
    private ScreenComponentService screenComponentService;
    @Autowired
    private TextPlaceholderService textPlaceholderService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private DeterminateService determinateService;
    @Autowired
    private ProductRepository productRepository;

    /**
     * Replaces an existing, or adds a new product structure. This process is
     * not reversible.
     *
     * @param product The Product to which the new product structure belongs
     * @param productStructure The product structure that is to be used and act
     * as a replacement.
     */
    public synchronized void replaceProductStructure(Product product, ProductStructure productStructure) {

        //Delete current structure
        LoggingUtil.logInfo(logger, "Deleting structure for product: ", product);
        deleteStructure(product);

        //Convert the new structure into entities and save
        Screens screens = productStructure.getScreens();
        for (Screen screen : screens.getScreenList()) {
            processScreen(product, screen);
        }

        RootDeterminates rootDeterminates = productStructure.getRootDeterminates();
        com.omussd.generic.db.entity.Intent intentEntity = processRootDeterminates(product, rootDeterminates);
        product.setIntentId(intentEntity);
        productRepository.save(product);
    }

    private void deleteStructure(Product product) {
        /*
            WARNING:
            Intents may never be deleted as it is linked to the Audit Log
         */

        sessionService.deleteSessionsByProduct(product);

        Collection<com.omussd.generic.db.entity.Screen> screens = product.getScreenCollection();
        for (com.omussd.generic.db.entity.Screen screen : screens) {
            textPlaceholderService.deleteByScreenId(screen);
        }

        List<ScreenDeterminate> determinates = screenService.findScreenDeterminatesByProduct(product);
        screenService.deleteScreenScreenDeterminatesHavingScreenDeterminates(determinates);
        screenService.deleteScreenDeterminates(determinates);

        screenComponentService.deleteScreenScreenComponentsByScreens(screens);

        List<OptionGroupComponent> optionGroupComponents = screenComponentService.findAllOptionGroupComponentsByProduct(product);
        screenComponentService.deleteAllOptionOptionGroupComponentsHavingOptionGroupComponents(optionGroupComponents);
        screenComponentService.deleteAllOptionGroupComponents(optionGroupComponents);
        screenComponentService.deleteAllOptionsOfProduct(product);
        screenComponentService.deleteAllInputComponentsOfProduct(product);
        screenComponentService.deleteAllScreenComponentsByProduct(product);

        screenService.deleteAllScreensOfProduct(product);
    }

    private void processScreen(Product product, Screen screen) {
        String failureScreenCode = screen.getFailureScreenCode();
        String screenCode = screen.getCode();
        ScreenTypeConstant screenType = ScreenTypeConstant.getScreenTypeByCode(screen.getType());
        Components componentsObj = screen.getComponents();
        List<TextPlaceholder> textPlaceholders = screen.getTextPlaceholders();
        com.omussd.generic.db.entity.Intent screenFailureIntent = createOrGetIntent(product,
                getScreenFailureIntent(failureScreenCode));

        com.omussd.generic.db.entity.Screen scr = new com.omussd.generic.db.entity.Screen();
        scr.setCode(screenCode);
        scr.setIntentId(screenFailureIntent);
        scr.setScreenTypeId(screenService.getScreenTypeByConstant(screenType));
        scr.setProductId(product);
        scr = screenService.createScreen(
                product, screenCode, screenFailureIntent, screenType);

        processTextComponents(product, componentsObj.getTexts(), scr);
        processInputScreenComponent(product, componentsObj.getInput(), scr);
        processOptionComponents(product, componentsObj.getOptions(), scr);

        processTextPlaceholders(product, textPlaceholders, scr);
    }

    private List<com.omussd.generic.db.entity.TextPlaceholder> processTextPlaceholders(Product product,
            List<TextPlaceholder> placeholders,
            com.omussd.generic.db.entity.Screen screen) {
        List<com.omussd.generic.db.entity.TextPlaceholder> placeholderEntities = new ArrayList<>();

        if (placeholders == null) {
            return placeholderEntities;
        }

        for (TextPlaceholder textPlaceholder : placeholders) {
            Intent intent = new Intent();
            intent.setDestination(textPlaceholder.getDestination());
            intent.setPurpose(textPlaceholder.getPurpose());
            intent.setValue(textPlaceholder.getValue());
            intent.setType(IntentTypeConstant.TEXT_PARAMETER_VALUE.toString());
            com.omussd.generic.db.entity.Intent intentEntity = createOrGetIntent(product, intent);

            com.omussd.generic.db.entity.TextPlaceholder placeholderEntity
                    = textPlaceholderService.createTextPlaceholder(textPlaceholder.getVariable(),
                            intentEntity, screen);
            placeholderEntities.add(placeholderEntity);
        }
        return placeholderEntities;
    }

    private List<ScreenScreenComponent> processOptionComponents(Product product,
            List<OptionGroup> optionGroups, com.omussd.generic.db.entity.Screen scr) {

        if (optionGroups == null) {
            return new ArrayList<>();
        }

        String productName = product.getName();

        List<ScreenScreenComponent> sscs = new ArrayList<>();

        for (OptionGroup optionGroup : optionGroups) {
            optionGroup.setName(productName + "_OG_" + getName(optionGroup.getName()));
            OptionGroupComponent optionGroupComponent = processOptionGroup(product, optionGroup);
            ScreenScreenComponent ssc = processScreenScreenComponent(scr, optionGroupComponent, optionGroup.getSequence());
            sscs.add(ssc);
        }
        return sscs;
    }

    private OptionGroupComponent processOptionGroup(Product product, OptionGroup optionGroup) {
        com.omussd.generic.db.entity.Intent ogcEIntent = null;
        if (optionGroup.getDestination() != null) {
            Intent ogcIntent = new Intent();
            ogcIntent.setValue(optionGroup.getIntentValue());
            ogcIntent.setPurpose(optionGroup.getPurpose());
            ogcIntent.setDestination(optionGroup.getDestination());
            ogcIntent.setType(IntentTypeConstant.OPTIONS.getIntentType());
            ogcEIntent = createOrGetIntent(product, ogcIntent);
        }

        LoggingUtil.logDebug(logger, "About to process option group: ", optionGroup);
        OptionGroupComponent ogc = screenComponentService.findOptionGroupComponentByName(optionGroup.getName());
        if (ogc == null) {
            ogc = screenComponentService.createOptionGroupComponent(
                    optionGroup.getName(), optionGroup.getParameterName(), ogcEIntent,
                    OptionGroupComponentTypeConstant.findByType(optionGroup.getType()));
        }

        List<OptionOptionGroupComponent> optionEntities = new ArrayList<>();
        List<Option> options = optionGroup.getOptions();

        for (Option option : options) {
            OptionOptionGroupComponent optionEntity = processOption(product, ogc, option);
            optionEntities.add(optionEntity);
        }

        return ogc;
    }

    private OptionOptionGroupComponent processOption(Product product, OptionGroupComponent optionGroupComponent,
            Option option) {
        List<com.omussd.generic.db.entity.Intent> intentEntities = new ArrayList<>();
        List<Intent> intents = option.getIntents();
        for (Intent intent : intents) {
            com.omussd.generic.db.entity.Intent intentEntity = createOrGetIntent(product, intent);
            intentEntities.add(intentEntity);
        }

        option.setName(product.getName() + "_OPT_" + getName(option.getName()));

        LoggingUtil.logDebug(logger, "About to process option: ", option);
        com.omussd.generic.db.entity.Option optionEntity = screenComponentService.findOptionByName(option.getName());
        if (optionEntity == null) {
            optionEntity = screenComponentService.createOption(option.getName(),
                    option.getValue(), option.getText(), OptionTypeConstant.getOptionTypeByType(option.getType()),
                    intentEntities);
        }

        return screenComponentService.createOptionOptionGroupComponent(
                optionGroupComponent, optionEntity, option.getSeq(), option.getNum());
    }

    private ScreenScreenComponent processInputScreenComponent(Product product,
            Input input, com.omussd.generic.db.entity.Screen scr) {
        if (input == null) {
            return null;
        }

        input.setName(product.getName() + "_IN_" + getName(input.getName()));

        List<com.omussd.generic.db.entity.Intent> intentEntities = new ArrayList<>();
        List<Intent> intents = input.getIntents();
        for (Intent intent : intents) {
            com.omussd.generic.db.entity.Intent i = createOrGetIntent(product, intent);
            if (i != null) {
                intentEntities.add(i);
            }
        }

        InputComponent ic = screenComponentService.findInputComponentByName(input.getName());
        if (ic == null) {
            ic = screenComponentService.createInputComponent(input.getName(),
                    input.getParameterName(), InputComponentTypeConstant.getInputComponentTypeByType(input.getType()),
                    input.getMinLength(), input.getMaxLength(), intentEntities);
        }
        if (input.getSequence() == null) {
            input.setSequence(999);
        }
        return processScreenScreenComponent(scr, ic, input.getSequence());
    }

    private List<ScreenScreenComponent> processTextComponents(Product product, List<Text> texts, com.omussd.generic.db.entity.Screen scr) {
        if (texts == null || texts.isEmpty()) {
            return new ArrayList<>();
        }

        String productName = product.getName();

        List<ScreenScreenComponent> screenScreenComponents = new ArrayList<>();

        for (Text text : texts) {
            text.setName(productName + "_TXT_" + getName(text.getName()));
            TextComponent textComponent = processText(text);

            ScreenScreenComponent screenScreenComponent = processScreenScreenComponent(scr, textComponent,
                    text.getSequence());
            screenScreenComponents.add(screenScreenComponent);
        }

        return screenScreenComponents;
    }

    private ScreenScreenComponent processScreenScreenComponent(com.omussd.generic.db.entity.Screen scr,
            ScreenComponent component, int sequence) {
        ScreenScreenComponent screenScreenComponent = new ScreenScreenComponent();
        screenScreenComponent.setSequence(sequence);
        screenScreenComponent.setScreenId(scr);
        screenScreenComponent.setScreenComponentId(component);
        screenScreenComponent = screenComponentService.createScreenScreenComponent(screenScreenComponent);
        return screenScreenComponent;
    }

    private TextComponent processText(Text text) {
        TextComponent textComponent = screenComponentService.findTextComponentByName(text.getName());
        if (textComponent == null) {
            textComponent = screenComponentService.createTextComponent(text.getName(), text.getTextString());
        }
        return textComponent;
    }

    private com.omussd.generic.db.entity.Intent createOrGetIntent(Product product, Intent intent) {
        if (intent == null
                || (intent.getDestination() == null
                && intent.getPurpose() == null
                && intent.getValue() == null
                && intent.getScreenCode() == null)) {
            return null;
        }

        if (intent.getScreenCode() != null) {
            intent.setValue(intent.getScreenCode());
            intent.setType(IntentTypeConstant.SUCCESS.getIntentType());
        }

        //convert structure intent to entity intent
        com.omussd.generic.db.entity.Intent intentEntity = intentService.createOrGetIntent(product, IntentTypeConstant.getIntentTypeByType(intent.getType()),
                intent.getDestination(), intent.getPurpose(), intent.getValue());

        if (intentEntity.getIntentTypeId().getType().equals(IntentTypeConstant.DETERMINATE.getIntentType())) {
            createIntentDeterminates(product, intent, intentEntity);
        }
        return intentEntity;
    }

    private void createIntentDeterminates(Product product, Intent intent, com.omussd.generic.db.entity.Intent intentEntity) {
        List<Determinate> determinates = intent.getDeterminates();
        if (determinates != null && !determinates.isEmpty()) {
            for (Determinate determinate : determinates) {
                processDeterminate(product, screenService.getScreenByCodeAndProduct(determinate.getScreenCode(), product),
                        intentEntity, determinate);
            }
        }
    }

    private ScreenScreenDeterminate processDeterminate(Product product, com.omussd.generic.db.entity.Screen screen,
            com.omussd.generic.db.entity.Intent intent, Determinate determinate) {
        ScreenDeterminate screenDeterminate = determinateService.createOrGetScreenDeterminate(product, determinate.getValue());
        return determinateService.createScreenScreenDeterminate(screen, screenDeterminate, intent);
    }

    private com.omussd.generic.db.entity.Intent processRootDeterminates(Product product, RootDeterminates rootDeterminates) {
        //create the intent
        Intent intent = new Intent();
        intent.setDestination(rootDeterminates.getDestination());
        intent.setPurpose(rootDeterminates.getPurpose());
        intent.setValue(rootDeterminates.getIntentValue());
        intent.setDeterminates(rootDeterminates.getDeterminates());
        intent.setType(IntentTypeConstant.DETERMINATE.getIntentType());

        com.omussd.generic.db.entity.Intent intentEntity = null;
        intentEntity = createOrGetIntent(product, intent);
        return intentEntity;
    }

    private Intent getScreenFailureIntent(String failureScreenCode) {
        Intent structureScreenFailureIntent = new Intent();
        structureScreenFailureIntent.setValue(failureScreenCode);
        structureScreenFailureIntent.setType(IntentTypeConstant.FAILURE.getIntentType());
        return structureScreenFailureIntent;
    }

    private String getName(String value) {
        if (value == null) {
            return UUID.randomUUID().toString();
        }
        return value;
    }
}
