/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenDeterminate;
import com.omussd.generic.db.entity.ScreenScreenDeterminate;
import com.omussd.generic.db.repository.ScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenScreenDeterminateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processing regarding determinates
 *
 * @author XY40428
 */
@Component
public class DeterminateService {

    @Autowired
    private ScreenDeterminateRepository screenDeterminateRepository;
    @Autowired
    private ScreenScreenDeterminateRepository screenScreenDeterminateRepository;

    /**
     * Creates the ScreenDeterminate in the database or retrieves the existing
     * determinates based upon the supplied parameters
     *
     * @param product The Product of the ScreenDeterminate
     * @param value The value of the ScreenDeterminate
     * @return
     */
    public ScreenDeterminate createOrGetScreenDeterminate(Product product, String value) {
        ScreenDeterminate sd = screenDeterminateRepository.findByProductIdAndValue(product, value);
        if (sd == null) {
            sd = new ScreenDeterminate();
            sd.setProductId(product);
            sd.setValue(value);
            sd = screenDeterminateRepository.save(sd);
        }

        return sd;
    }

    /**
     * Creates the ScreenScreenDeterminate in the database
     *
     * @param screen The Screen of the ScreenScreenDeterminate
     * @param screenDeterminate The ScreenDeterminate of the
     * ScreenScreenDeterminate
     * @param intent The Intent of the ScreenScreenDeterminate
     * @return The created ScreenScreenDeterminate
     */
    public ScreenScreenDeterminate createScreenScreenDeterminate(Screen screen,
            ScreenDeterminate screenDeterminate, Intent intent) {
        ScreenScreenDeterminate ssd = new ScreenScreenDeterminate();
        ssd.setIntentId(intent);
        ssd.setScreenDeterminateId(screenDeterminate);
        ssd.setScreenId(screen);
        ssd = screenScreenDeterminateRepository.save(ssd);
        return ssd;
    }
}
