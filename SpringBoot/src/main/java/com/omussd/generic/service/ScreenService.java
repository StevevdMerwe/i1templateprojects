package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenDeterminate;
import com.omussd.generic.db.entity.ScreenScreenDeterminate;
import com.omussd.generic.db.entity.ScreenType;
import com.omussd.generic.db.repository.ScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenRepository;
import com.omussd.generic.db.repository.ScreenScreenDeterminateRepository;
import com.omussd.generic.db.repository.ScreenTypeRepository;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processing around screens
 *
 * @author XY40428
 */
@Component
public class ScreenService {

    private static final Logger logger = LoggerFactory.getLogger(ScreenService.class);

    @Autowired
    private ScreenRepository screenRepository;
    @Autowired
    private ScreenTypeRepository screenTypeRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private ScreenDeterminateRepository screenDeterminateRepository;
    @Autowired
    private ScreenScreenDeterminateRepository screenScreenDeterminateRepository;

    public Screen createScreen(Product product, String screenCode, Intent failureIntent, ScreenTypeConstant stc) {
        Screen screen = new Screen();
        screen.setCode(screenCode);
        screen.setIntentId(failureIntent);
        screen.setProductId(product);
        screen.setScreenTypeId(screenTypeRepository.findByCode(stc.getCode()));
        return screenRepository.save(screen);
    }

    public ScreenType getScreenTypeByConstant(ScreenTypeConstant stc) {
        return screenTypeRepository.findByCode(stc.getCode());
    }

    public void deleteAllScreensOfProduct(Product product) {
        Integer rowsAffected = screenRepository.deleteAllByProduct(product);
        LoggingUtil.logInfo(logger, "Screens of product ", product, " was deleted. A total of ", rowsAffected, " records affected");
    }

    public Screen getRootScreenForNullProduct() {
        return getScreenByCodeWithNoProduct(SpecialScreenCodesConstant.UNRECOGNISED_ROOT.getCode());
    }

    public Screen getFailureScreen(Product product) {
        return getScreenByCodeAndProduct(SpecialScreenCodesConstant.PRODUCT_FAILURE.getCode(), product);
    }

    public Screen getScreenByCodeWithNoProduct(String code) {
        return screenRepository.findByCodeAndProductIdIsNull(code);
    }

    public Screen getScreenByCodeAndProduct(String code, Product product) {
        Screen screen = screenRepository.findByCodeAndProductId(code, product);
        if (screen == null) {
            screen = getScreenByCodeWithNoProduct(code);
        }
        return screen;
    }

    public Screen getScreenByCodeAndProductName(String msisdn, String code, String productName) {
        Product product = productService.getProductByName(true, msisdn, productName);
        return getScreenByCodeAndProduct(code, product);
    }

    public Screen getScreenForRedirectIntent(Product product, String intentValue) {
        Screen screen;
        if (product == null) {
            screen = getScreenByCodeWithNoProduct(intentValue);
        } else {
            screen = getScreenByCodeAndProduct(intentValue, product);
        }
        return screen;
    }

    public List<ScreenDeterminate> findScreenDeterminatesByProduct(Product product) {
        return screenDeterminateRepository.findByProductId(product);
    }

    public void deleteScreenScreenDeterminatesHavingScreenDeterminates(List<ScreenDeterminate> screenDeterminates) {
        for (ScreenDeterminate screenDeterminate : screenDeterminates) {
            Collection<ScreenScreenDeterminate> screenScreenDeterminates = screenDeterminate.getScreenScreenDeterminateCollection();
            LoggingUtil.logInfo(logger, "Deleting ScreenScreenDeterminates having ScreenDeterminate: ", screenDeterminate);
            screenScreenDeterminateRepository.delete(screenScreenDeterminates);
        }
    }

    public void deleteScreenDeterminates(List<ScreenDeterminate> screenDeterminates) {
        LoggingUtil.logInfo(logger, "Deleting ScreenDetermiantes: ", screenDeterminates);
        screenDeterminateRepository.delete(screenDeterminates);
    }
}
