/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.InputComponentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.OptionTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.ScreenScreenComponent;
import com.omussd.generic.db.entity.TextComponent;
import com.omussd.generic.db.repository.InputComponentRepository;
import com.omussd.generic.db.repository.InputComponentTypeRepository;
import com.omussd.generic.db.repository.OptionGroupComponentRepository;
import com.omussd.generic.db.repository.OptionGroupComponentTypeRepository;
import com.omussd.generic.db.repository.OptionOptionGroupComponentRepository;
import com.omussd.generic.db.repository.OptionRepository;
import com.omussd.generic.db.repository.OptionTypeRepository;
import com.omussd.generic.db.repository.ScreenComponentRepository;
import com.omussd.generic.db.repository.ScreenScreenComponentRepository;
import com.omussd.generic.db.repository.TextComponentRepository;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.generic.util.InputValidationUtil;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processing around screen components
 *
 * @author XY40428
 */
@Component
public class ScreenComponentService {

    private static final Logger logger = LoggerFactory.getLogger(ScreenComponentService.class);

    @Autowired
    private ScreenComponentRepository screenComponentRepository;
    @Autowired
    private OptionOptionGroupComponentRepository optionOptionGroupComponentRepository;
    @Autowired
    private OptionGroupComponentRepository optionGroupComponentRepository;
    @Autowired
    private InputComponentRepository inputComponentRepository;
    @Autowired
    private OptionRepository optionRepository;
    @Autowired
    private ScreenScreenComponentRepository screenScreenComponentRepository;
    @Autowired
    private TextComponentRepository textComponentRepository;
    @Autowired
    private InputComponentTypeRepository inputComponentTypeRepository;
    @Autowired
    private OptionGroupComponentTypeRepository optionGroupComponentTypeRepository;
    @Autowired
    private OptionTypeRepository optionTypeRepository;

    public void deleteAllOptionOptionGroupComponentsHavingOptionGroupComponents(List<OptionGroupComponent> optionGroupComponents) {
        for (OptionGroupComponent optionGroupComponent : optionGroupComponents) {
            Collection<OptionOptionGroupComponent> coll = optionGroupComponent.getOptionOptionGroupComponentCollection();
            LoggingUtil.logInfo(logger, "Deleting OptionOptionGroupComonents having optionGroupComponent: ", optionGroupComponent);
            optionOptionGroupComponentRepository.delete(coll);
        }
    }

    public void deleteAllOptionGroupComponents(List<OptionGroupComponent> optionGroupComponents) {
        LoggingUtil.logInfo(logger, "Deleting OptionGroupComonents: ", optionGroupComponents);
        optionGroupComponentRepository.delete(optionGroupComponents);
    }

    public void deleteAllInputComponentsOfProduct(Product product) {
        Integer rows = inputComponentRepository.deleteAllInputComponentsWhereNameStartsWith(product.getName());
        LoggingUtil.logInfo(logger, "Deleted all input components of product: ", product, " affecting ", rows, " rows");
    }

    public void deleteAllScreenComponentsByProduct(Product product) {
        Integer rows = screenComponentRepository.deleteAllScreenComponentsWhereNameStartsWith(product.getName());
        LoggingUtil.logInfo(logger, "Deleted all remaining screenComponents of product: ", product, " affecting ", rows, " rows");
    }

    public void deleteScreenScreenComponentsByScreens(Collection<Screen> screens) {
        for (Screen screen : screens) {
            screenScreenComponentRepository.deleteByScreenId(screen);
            LoggingUtil.logInfo(logger, "deleted screenScreenComponents with screen: ", screen);
        }
    }

    public List<OptionGroupComponent> findAllOptionGroupComponentsByProduct(Product product) {
        return optionGroupComponentRepository
                .findAllOptionGroupComponentsByNameWithPrefix(product.getName());
    }

    public void deleteAllOptionsOfProduct(Product product) {
        Integer rows = optionRepository.deleteOptionWhereNameStartsWith(product.getName());
        LoggingUtil.logInfo(logger, "Options deleted for product ", product, " affecting ", rows, " rows");
    }

    public Collection<ScreenComponent> getScreenComponentsByScreen(Screen screen) {
        return screenComponentRepository.findByScreenScreenComponentCollectionScreenId(screen);
    }

    public Collection<OptionGroupComponent> getDynamicOptionGroupComponentsByScreen(Screen screen) {
        return optionGroupComponentRepository
                .findOptionGroupComponentsOfTypeOnScreen(OptionGroupComponentTypeConstant.DYNAMIC.getType(), screen);
    }

    public DynamicOption findDynamicOptionByNumber(DynamicOptionGroup dog, int number) {
        List<DynamicOption> options = dog.getDynamicOptions();
        for (DynamicOption option : options) {
            if (option.getNumber() == number) {
                return option;
            }
        }
        return null;
    }

    public OptionGroupComponent findOptionGroupComponentCotainingOptionWithNumberOnScreen(int number, Screen screen) {
        return optionGroupComponentRepository.findOptionGroupComponentContainingOptionWithNumberOnScreen(number, screen);
    }

    public InputComponent findInputComponentOnScreen(Screen screen) {
        return inputComponentRepository.findInputComponentOnScreen(screen);
    }

    public OptionGroupComponent findDynamicOptionGroupComponentContianingOptionWithNumber(
            String msisdn, int number) {
        //Get all dynamic groups from session
        List<DynamicOptionGroup> dogList = SessionDataCache.INSTANCE.getSessionData(msisdn)
                .getDynamicOptionGroups();
        //See if one of them have the given number value
        for (DynamicOptionGroup dog : dogList) {
            for (DynamicOption dyo : dog.getDynamicOptions()) {
                if (dyo.getNumber() == number) {
                    //found a match!
                    return findOptionGroupComponentByName(dog.getName());
                }
            }
        }
        return null;
    }

    public List<ScreenScreenComponent> findScreenScreenComponentByScreenIdOrderBySequence(Screen screen) {
        return screenScreenComponentRepository.findByScreenIdOrderBySequence(screen);
    }

    public List<OptionOptionGroupComponent> findOptionOptionGroupComponentsByOptionGroupComponent(OptionGroupComponent ogc) {
        return optionOptionGroupComponentRepository.findByOptionGroupComponentId(ogc);
    }

    public Option findOptionOnScreenWithNumber(Screen screen, int number) {
        return optionRepository.findOptionOnScreenWithNumber(screen, number);
    }

    public boolean isInputValid(InputComponent inputComponent, String inputValue) {
        InputComponentTypeConstant type = InputComponentTypeConstant.getInputComponentTypeByType(inputComponent.getInputComponentTypeId().getType());

        if (inputValue == null || inputValue.isEmpty()) {
            return false;
        }

        //check lengths
        int minLength = inputComponent.getMinimumLength();
        int maxLength = inputComponent.getMaximumLength();
        int length = inputValue.length();
        if (length < minLength || length > maxLength) {
            return false;
        }

        switch (type) {
            case ALPHANUMERIC:
                return InputValidationUtil.validateAlphanumeric(inputValue);
            case NUMBER:
                return InputValidationUtil.validateNumber(inputValue);
            case RSA_LEGAL_ID:
                return InputValidationUtil.validateRSAIdNumber(inputValue);
            case CELL_NUMBER:
                return InputValidationUtil.validateCellNumber(inputValue);
        }
        return true;
    }

    public TextComponent createTextComponent(String name, String text) {
        TextComponent tc = new TextComponent();
        tc.setText(text);
        tc.setName(name);
        tc = textComponentRepository.save(tc);
        return tc;
    }

    public TextComponent findTextComponentByName(String name) {
        return textComponentRepository.findByName(name);
    }

    public ScreenScreenComponent createScreenScreenComponent(ScreenScreenComponent ssc) {
        return screenScreenComponentRepository.save(ssc);
    }

    public InputComponent createInputComponent(String name, String paremeterName,
            InputComponentTypeConstant inputType, int minLength, int maxLength,
            List<Intent> intents) {
        InputComponent ic = new InputComponent();
        ic.setMaximumLength(maxLength);
        ic.setMinimumLength(minLength);
        ic.setName(name);
        ic.setParameterName(paremeterName);
        ic.setInputComponentTypeId(inputComponentTypeRepository.findByType(inputType.getType()));
        ic.addIntents(intents);
        return inputComponentRepository.save(ic);
    }

    public InputComponent findInputComponentByName(String name) {
        return inputComponentRepository.findByName(name);
    }

    public OptionGroupComponent createOptionGroupComponent(String name, String parameterName,
            Intent intent, OptionGroupComponentTypeConstant type) {
        OptionGroupComponent ogc = new OptionGroupComponent();
        ogc.setIntentId(intent);
        ogc.setOptionGroupComponentTypeId(optionGroupComponentTypeRepository.findByType(type.getType()));
        ogc.setName(name);
        ogc.setParameterName(parameterName);
        return optionGroupComponentRepository.save(ogc);
    }

    public OptionGroupComponent findOptionGroupComponentByName(String name) {
        return optionGroupComponentRepository.findByName(name);
    }

    public Option createOption(String name, String value, String text, OptionTypeConstant type, List<Intent> intents) {
        Option option = new Option();
        option.setName(name);
        option.setValue(value);
        option.setText(text);
        option.setOptionTypeId(optionTypeRepository.findByType(type.getType()));
        option.addIntents(intents);

        return optionRepository.save(option);
    }

    public Option findOptionByName(String name) {
        return optionRepository.findByName(name);
    }

    public OptionOptionGroupComponent createOptionOptionGroupComponent(OptionGroupComponent optionGroupComponent,
            Option option, int sequence, int number) {
        OptionOptionGroupComponent oogc = new OptionOptionGroupComponent();
        oogc.setOptionId(option);
        oogc.setOptionGroupComponentId(optionGroupComponent);
        oogc.setSequence(sequence);
        oogc.setOptionNumber(number);
        oogc = optionOptionGroupComponentRepository.save(oogc);
        return oogc;
    }

    public String getInputValueFromComponent(Screen screen, ScreenComponent component, String msisdn, String inputValue) {
        if (component instanceof InputComponent) {
            return inputValue;
        } else {
            Option option = findOptionOnScreenWithNumber(screen, Integer.parseInt(inputValue));
            if (option == null) {
                //try dynamic
                return getInputValueFromDynamicOptionsComponent(component, msisdn, inputValue);
            } else {
                return option.getValue();
            }
        }
    }

    private String getInputValueFromDynamicOptionsComponent(ScreenComponent component, String msisdn, String inputValue) {
        List<DynamicOptionGroup> dogs = SessionDataCache.INSTANCE.getSessionData(msisdn).getDynamicOptionGroups();
        for (DynamicOptionGroup dog : dogs) {
            if (dog.getParameter().equals(component.getParameterName())) {
                DynamicOption dynOption = findDynamicOptionByNumber(dog, Integer.parseInt(inputValue));
                if (dynOption != null) {
                    return dynOption.getValue();
                }
            }
        }
        return null;
    }

    public boolean isOptionGroupComponentDynamic(OptionGroupComponent ogc) {
        String componentType = ogc.getOptionGroupComponentTypeId().getType();
        return OptionGroupComponentTypeConstant.DYNAMIC.getType().equals(componentType);
    }
}
