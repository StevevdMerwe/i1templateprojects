/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.common.util.XMLUtil;
import com.omussd.generic.audit.AuditDataEntry;
import com.omussd.generic.constant.AuditDataEntryKey;
import com.omussd.generic.constant.AuditLogEvent;
import com.omussd.generic.constant.NetworkConstant;
import com.omussd.generic.constant.PropertyConstant;
import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.constant.SpecialParametersConstant;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.NetworkRepository;
import com.omussd.generic.db.repository.SessionRepository;
import com.omussd.generic.exception.SessionAlreadyInUseException;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processing around the session
 *
 * @author XY40428
 */
@Component
public class SessionService {

    private static Logger logger = LoggerFactory.getLogger(SessionService.class);

    @Autowired
    private PropertyService propertyService;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private NetworkRepository networkRespository;
    @Autowired
    private AuditService auditService;

    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    public Session getSessionForMsisdnAndNetwork(String msisdn, NetworkConstant network)
            throws SessionAlreadyInUseException {
        Session session = findSession(msisdn, network);
        List<Session> otherSessions = findSessionForOtherNetworks(msisdn, network);

        if (otherSessions != null && !otherSessions.isEmpty()) {
            Session other = otherSessions.get(0); //there can only be one
            auditService.logAudit(AuditLogEvent.SESSION_ALREADY_IN_USE, null,
                    msisdn, network, new AuditDataEntry[]{AuditDataEntry.getEntry(AuditDataEntryKey.NETWORK_OF_SESSION,
                                other.getNetworkId().getName())});
            throw new SessionAlreadyInUseException();
        }

        if (session != null && isSessionExpired(session)) {
            deleteSessionByMsisdn(session.getMsisdn());
            session = null;
        }

        if (session == null) {
            session = createNewSession(msisdn, network);
            auditService.logAudit(AuditLogEvent.CREATED_NEW_SESSION, session,
                    new AuditDataEntry[]{
                        AuditDataEntry.getEntry(AuditDataEntryKey.SESSION_SART_TIME,
                                session.getStartTime().toString())});
        } else if (session.getLocked()) {
            auditService.logAudit(AuditLogEvent.SESSION_ALREADY_IN_USE, null,
                    msisdn, network,
                    new AuditDataEntry[]{AuditDataEntry.getEntry(AuditDataEntryKey.NETWORK_OF_SESSION,
                                session.getNetworkId().getName())});
            throw new SessionAlreadyInUseException();
        } else {
            auditService.logAudit(AuditLogEvent.FOUND_EXISTING_SESSION, session,
                    new AuditDataEntry[]{
                        AuditDataEntry.getEntry(AuditDataEntryKey.SESSION_SART_TIME,
                                session.getStartTime().toString())});
        }
        return session;
    }

    public void lockSession(Session session) {
        session.setLocked(Boolean.TRUE);
        resetSessionTime(session);
        sessionRepository.save(session);
    }

    public void unlockSession(Session session) {
        session.setLocked(Boolean.FALSE);
        resetSessionTime(session);
        sessionRepository.save(session);
    }

    public boolean isSessionExpired(Session session, Long sessionTTL) {
        Date timeNow = GregorianCalendar.getInstance().getTime();
        Date timeInit = session.getStartTime();
        Long difference = timeNow.getTime() - timeInit.getTime();
        return difference > sessionTTL;
    }

    private boolean isSessionExpired(Session session) {
        Long sessionTTL = propertyService.findLongValueByName(PropertyConstant.SESSION_TTL.toString());
        return isSessionExpired(session, sessionTTL);
    }

    private Session findSession(String msisdn, NetworkConstant network) {
        LoggingUtil.logTrace(logger, "findSession entered");
        return sessionRepository.findByMsisdnAndNetworkIdName(msisdn, network.getName());
    }

    private List<Session> findSessionForOtherNetworks(String msisdn, NetworkConstant networkToExclude) {
        return sessionRepository.findByMsisdnNotOnNetwork(msisdn, networkToExclude.getName());
    }

    private Session createNewSession(String msisdn, NetworkConstant network) {
        LoggingUtil.logTrace(logger, "createNewSession entered");
        SessionData data = new SessionData();
        Date startTime = GregorianCalendar.getInstance().getTime();
        Session session = new Session();
        session.setMsisdn(msisdn);
        session.setNetworkId(networkRespository.findByName(network.getName()));
        session.setStartTime(startTime);
        try {
            session.setSessionData(XMLUtil.toXML(false, SessionData.class, data));
        } catch (JAXBException e) {
            LoggingUtil.logError(logger, e, "Failed to parse xml of sessionData");
        }
        session.setLocked(Boolean.TRUE);

        session = sessionRepository.saveAndFlush(session);

        return session;
    }

    public void updateSession(Session session) {
        SessionData data = SessionDataCache.INSTANCE.getSessionData(session.getMsisdn());
        if (data == null) {
            data = new SessionData();
        }
        setSessionData(session, data);
        resetSessionTime(session);
        sessionRepository.save(session);
    }

    private void resetSessionTime(Session session) {
        Date time = GregorianCalendar.getInstance().getTime();
        session.setStartTime(time);
    }

    public SessionData getSessionData(Session session) {
        try {
            return XMLUtil.fromXML(SessionData.class, session.getSessionData());
        } catch (JAXBException e) {
            LoggingUtil.logError(logger, e, "Failed to parse xml of session data");
        }
        return null;
    }

    private void setSessionData(Session session, SessionData sessionData) {
        try {
            String xml = XMLUtil.toXML(false, SessionData.class, sessionData);
            session.setSessionData(xml);
        } catch (JAXBException e) {
            LoggingUtil.logError(logger, e, "Failed to parse xml of session data");
        }
    }

    public Integer deleteSessionByMsisdn(String msisdn) {
        return sessionRepository.deleteByMsisdn(msisdn);
    }

    public void deleteAll() {
        LoggingUtil.logInfo(logger, "Deleting all sessions");
        sessionRepository.deleteAll();
    }

    public Integer deleteSessionsByProduct(Product product) {
        LoggingUtil.logInfo(logger, "Deleting sessions by product: ", product);
        return sessionRepository.deleteByProduct(product);
    }

    public SessionData assignProductNameToSessionMapFromProductChoiceParameter(SessionData sessionData) {
        String productChoice;
        if ((productChoice = sessionData.getParameter(SpecialParametersConstant.PRODUCT_CHOICE.toString())) != null) {
            sessionData.addToSessionMap(SessionDataConstant.PRODUCT_NAME, productChoice);
        }
        return sessionData;
    }
}
