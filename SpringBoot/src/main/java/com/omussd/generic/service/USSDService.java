package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.constant.SpecialParametersConstant;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionType;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.ProductProperty;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.OptionRepository;
import com.omussd.generic.exception.ProductFailureException;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.SessionHistory;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.ussdintegration.request.USSDAppRequest;
import com.omussd.ussdintegration.request.constant.Type;
import com.omussd.ussdintegration.util.GenericIntegrationUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that acts as the endpoint for doing USSD processes within the
 * system.
 *
 * @author XY40428
 */
@Component
public class USSDService {

    private static final Logger logger = LoggerFactory.getLogger(USSDService.class);

    @Autowired
    private SessionService sessionService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ScreenService screenService;
    @Autowired
    private IntentService intentService;
    @Autowired
    private ScreenComponentService screenComponentService;
    @Autowired
    private OptionRepository optionRepository;

    private void submitRange(Product product, String msisdn, int range) {
        productService.callProductRange(product, range, msisdn);
    }

    /**
     * Processes the USSD Request in such a way as to get the next Screen object
     * that is to be used in the USSD process, using the Session, Product name,
     * and the request that was made in order to make a viable consideration.
     *
     * This method will alter or delete the Session as a by-product of the
     * processing.
     *
     * @param session The Session for the current request
     * @param productName The name of the product
     * @param request The request that was made
     * @return The screen to be used next in the process.
     */
    public Screen processUSSDRequest(Session session, String productName, USSDAppRequest request) {
        LoggingUtil.logTrace(logger, "processUSSDRequest entered");
        Integer range = request.getCode() == null ? -1 : Integer.parseInt(request.getCode());
        Product product = productService.getProductByName((request.getType().equals(Type.initial) && range == -1) ? false : true,
                session.getMsisdn(), productName);

        Screen screen;
        if (!isValidRequest(request)) {
            screen = screenService.getFailureScreen(product);
        } else {
            screen = processProductValidity(product);
        }

        if (screen == null) {
            switch (request.getType()) {
                case initial:
                    screen = processInitialRequest(session, product, false, range);
                    break;
                case action:
                    screen = processActionRequest(session, product, request, range);
                    break;
                case abort:
                default:
                    screen = null;
                    //abort request does not continue the conversation
                    /*
                        The reason the session does not get deleted is because:
                        An abort request may be sent upon a network timeout which results in the
                        deletion of the session. The session may live beyond a network timeout if
                        the system's session timeout is longer than the network timeout.
                        For this reason, session cleanup cannot happen at this stage.
                     */
                    break;
            }
        }

        if (screen != null) {
            try {
                //Product could have altered since last check. Ensure correct product
                product = productService.findProductFromSession(session.getMsisdn());

                //Previous screen's dynamic groups must be removed                
                SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(session.getMsisdn());
                sessionData.clearDynamicOptionGroups();

                intentService.processPostIntents(screen, session, product);
                SessionDataCache.INSTANCE.put(session.getMsisdn(), sessionData);
                session.setScreenId(screen);
                if (ScreenTypeConstant.END.getCode().equals(screen.getScreenTypeId().getCode())) {
                    LoggingUtil.logTrace(logger, "Deleting session as END screen is about to be returned");
                    sessionService.deleteSessionByMsisdn(session.getMsisdn());
                } else {
                    sessionService.updateSession(session);
                }
            } catch (ProductFailureException e) {
                LoggingUtil.logError(logger, e, "Product failure. Processing request to ultimately fail. Session update to be halted.");
                LoggingUtil.logTrace(logger, "processUSSDRequest exiting");
                screen = screenService.getScreenByCodeAndProductName(session.getMsisdn(),
                        screen.getIntentId().getIntentValue(), productName);
            }

        }

        LoggingUtil.logTrace(logger, "processUSSDRequest exiting");
        return screen;
    }

    private Screen processProductValidity(Product product) {
        ProductProperty productProperty = productService.findPropductPropertyByProduct(product);
        Screen screen = null;
        if (productProperty != null) {
            if (!productProperty.getEnabled() || !productProperty.getActive()) {
                screen = screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.PRODUCT_DISABLED.getCode(), product);
            } else if (productService.isExpired(productProperty)) {
                screen = screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.PRODUCT_EXPIRED.getCode(), product);
            } else if (!productService.hasStarted(productProperty)) {
                screen = screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.PRODUCT_NOT_STARTED.getCode(), product);
            }
        }
        return screen;
    }

    private Screen processInitialRequest(Session session, Product product, boolean forceRootScreen, Integer range) {
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        if (product != null && !forceRootScreen) {
            sessionData.addToSessionMap(SessionDataConstant.PRODUCT_NAME, product.getName());
            SessionDataCache.INSTANCE.put(msisdn, sessionData);
        }

        Screen screen;
        if (sessionData.hasEmptyHistory() || forceRootScreen) {
            //Brand new request

            //Do work required due to the range value if the product isnt already assigned
            if (sessionData.getFromSessionMap(SessionDataConstant.PRODUCT_NAME) == null || !forceRootScreen) {
                product = processInitialRange(range, product, msisdn);
            }

            //Clear session data if needed
            if (!sessionData.hasEmptyHistory()) {
                sessionData = new SessionData();
                SessionDataCache.INSTANCE.put(msisdn, sessionData);
                sessionService.updateSession(session);
            }

            if (product != null) {
                //Add product to sessionData if it exists after initial range check
                sessionData.addToSessionMap(SessionDataConstant.PRODUCT_NAME, product.getName());
            }

            //Store the riginal range to scross-checking in case of network expiry
            sessionData.addToSessionMap(SessionDataConstant.INITIAL_DIALED_RANGE, String.valueOf(range));

            //Get the root screen for this product (or system root if product is null)
            screen = intentService.processRootDeterminateForProduct(product, session);

            //create and add history to the session
            SessionHistory sessionHistory = new SessionHistory();
            sessionHistory.setProductName(product == null ? null : product.getName());
            sessionHistory.setScreenCode(screen.getCode());
            sessionData.addSessionHistory(sessionHistory);
        } else {
            //Expired on network
            return processInitialExpiredOnNetwork(session, product, range);
        }

        SessionDataCache.INSTANCE.put(msisdn, sessionData);

        return screen;
    }

    private Product processInitialRange(Integer range, Product product, String msisdn) {
        if (range == -1) {
            return null;
        } else if (range > 0 && range < 10) {
            //Get the actual product
            List<Product> products = productService.getAllWorkingProducts();
            if (products.size() >= range) {
                return products.get(range - 1);
            }
        } else if (product != null) {
            //Do range calls
            submitRange(product, msisdn, range);
        }
        return product;
    }

    private Screen processInitialExpiredOnNetwork(Session session, Product product, Integer range) {
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(session.getMsisdn());

        if ((!sessionData.getFromSessionMap(SessionDataConstant.INITIAL_DIALED_RANGE).equals(String.valueOf(range)))) {
            return processInitialRequest(session, product, true, range);
        } else {
            Screen rootScreen = screenService.getScreenByCodeAndProduct(sessionData.getFirstScreenCode(), product);
            if (!session.getScreenId().equals(rootScreen)) {
                Screen screen = screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.NETWORK_SESSION_EXPIRED_ROOT.getCode(), product);
                sessionData.addToSessionMap(SessionDataConstant.NEXT_SCREEN_OVERRIDE, session.getScreenId().getCode());
                return screen;
            } else {
                return processInitialRequest(session, product, true, range);
            }
        }
    }

    private Screen processActionRequest(Session session, Product product, USSDAppRequest request, Integer range) {
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        Screen oldScreen = session.getScreenId();
        String input = GenericIntegrationUtil.getUserInputFromUSSDAppRequest(request);
        Screen screen;

        //get responsible component
        ScreenComponent component = findScreenComponentForInput(msisdn, oldScreen, input);
        if (component != null) {
            String[] paramPair = getParameterFromComponent(msisdn, component, input);
            if (paramPair.length == 2) {
                sessionData.addParameter(paramPair[0], paramPair[1]);
            }

            if (product == null) {
                /*
                check if product is perhaps in the session.
                This should only happen once - when the enterprise root is oldScreen.
                All other instances, the product should already be set before reaching this point.
                 */
                sessionData = sessionService.assignProductNameToSessionMapFromProductChoiceParameter(sessionData);
                product = productService.findProductFromSession(msisdn);

                //InitialRequest for this product
//                sessionData.popSessionHistory(); // pop this recently added history
                SessionDataCache.INSTANCE.put(msisdn, sessionData);
                return processInitialRequest(session, product, true, range); //-1 is root screen range
            }
        } else {
            return processActionRequestComponentNull(product);
        }

        boolean reInitialise = false;
        if (sessionData.getFromSessionMap(SessionDataConstant.NEXT_SCREEN_OVERRIDE) != null) {
            screen = processActionNextScreenOverrideNotNull(sessionData, product);
            if (screen != null) {
                return screen;
            } else {
                reInitialise = true;
            }
        }

        screen = processSystemOptionTypes(session, msisdn, product, oldScreen, input);
        if (screen == null) {
            screen = processActionRequestNonSystemOptionType(oldScreen, session,
                    sessionData, component, product, input, reInitialise, range);
        }

        return screen;
    }

    private Screen processActionRequestNonSystemOptionType(Screen oldScreen,
            Session session, SessionData sessionData, ScreenComponent component,
            Product product, String input, boolean reInitialise, Integer range) {
        Screen screen;
        String msisdn = session.getMsisdn();

        SessionHistory history = new SessionHistory();
        sessionData.addSessionHistory(history);
        sessionData.peekSessionHistory().setProductName(product.getName());

        if (component instanceof InputComponent) {
            boolean isValid = screenComponentService.isInputValid((InputComponent) component, input);
            if (!isValid) {
                return intentService.processValidationFailureIntent(oldScreen, (InputComponent) component, session, product);
            }
        }

        if (sessionData.hasEmptyHistory()) {
            //System session expired before network session expired
            screen = screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.SYSTEM_SESSION_EXPIRED_ROOT.getCode(), product);
        } else {
            screen = intentService.processPreIntents(!reInitialise, session, product, oldScreen,
                    input, component);
            if (reInitialise && screen == null) {
                //force initialisation
                sessionData.clearData();
                SessionDataCache.INSTANCE.put(msisdn, sessionData);
                return processInitialRequest(session, product, true, range);
            }
        }

        sessionData.peekSessionHistory().setScreenCode(screen.getCode());
        SessionDataCache.INSTANCE.put(msisdn, sessionData);
        return screen;
    }

    private Screen processActionRequestComponentNull(Product product) {
        //no such option
        return screenService.getScreenByCodeAndProduct(
                SpecialScreenCodesConstant.WRONG_OPTION.getCode(), product);
    }

    private Screen processActionNextScreenOverrideNotNull(SessionData sessionData, Product product) {
        boolean reInitialise = false;
        if (sessionData.getFromSessionMap(SessionDataConstant.NEXT_SCREEN_OVERRIDE) != null) {
            reInitialise = !Boolean.valueOf(sessionData.getParameter(
                    SpecialParametersConstant.NETWORK_EXPIRED_CONTINUE.toString()));
            if (!reInitialise) {
                String code = sessionData.getFromSessionMap(SessionDataConstant.NEXT_SCREEN_OVERRIDE);
                sessionData.removeFromSessionMap(SessionDataConstant.NEXT_SCREEN_OVERRIDE);
                return screenService.getScreenByCodeAndProduct(code,
                        product);
            }
        }
        return null;
    }

    private String[] getParameterFromComponent(String msisdn,
            ScreenComponent component, String input) {
        String parameter = component.getParameterName();

        String value = null;
        if (component instanceof OptionGroupComponent) {
            OptionGroupComponent ogc = (OptionGroupComponent) component;
            if (OptionGroupComponentTypeConstant.STATIC.getType().equals(ogc.getOptionGroupComponentTypeId().getType())) {
                Option option = optionRepository.findOptionBelongingToOptionGroupComponentWithNumber(ogc, Integer.valueOf(input));
                value = option.getValue();
            } else {
                List<DynamicOptionGroup> dogs = SessionDataCache.INSTANCE.getSessionData(msisdn)
                        .getDynamicOptionGroups();
                for (DynamicOptionGroup dog : dogs) {
                    if (ogc.getName().equals(dog.getName())) {
                        DynamicOption option = screenComponentService.findDynamicOptionByNumber(dog, Integer.valueOf(input));
                        value = option.getValue();
                    }
                }
            }
        } else {
            value = input;
        }

        if (parameter == null || value == null) {
            return new String[0];
        }

        return new String[]{parameter, value};
    }

    private ScreenComponent findScreenComponentForInput(String msisdn,
            Screen screen, String input) {
        /*
            Find the component to which the user's input belongs.
            Group Components get privilage
         */
        if (input.matches("[0-9]{1,10}")) {
            try {
                int number = Integer.parseInt(input);
                OptionGroupComponent optionGroupComponent = screenComponentService
                        .findOptionGroupComponentCotainingOptionWithNumberOnScreen(number, screen);
                if (optionGroupComponent != null) {
                    return optionGroupComponent;
                } else {
                    optionGroupComponent = screenComponentService.findDynamicOptionGroupComponentContianingOptionWithNumber(
                            msisdn, number);
                    if (optionGroupComponent != null) {
                        return optionGroupComponent;
                    }
                }
            } catch (NumberFormatException nfe) {
                LoggingUtil.logError(logger, nfe, "Number could not be converted to integer");
            }
        }

        //By now, only input remains
        return screenComponentService.findInputComponentOnScreen(screen);
    }

    private boolean isValidRequest(USSDAppRequest request) {
        boolean paramValidation = !(request.getParameter() == null || request.getParameter().trim().isEmpty());
        boolean fieldsValidation = !(request.getFields() == null || request.getFields().getFields() == null
                || request.getFields().getFields().isEmpty());
        if (!paramValidation && fieldsValidation) {
            String input = GenericIntegrationUtil.getUserInputFromUSSDAppRequest(request);
            if (input == null || input.isEmpty()) {
                //no paramter, and no field of userResponse. Not valid
                return false;
            }
        }
        return paramValidation || fieldsValidation;
    }

    private Screen processSystemOptionTypes(Session session, String msisdn,
            Product product, Screen oldScreen, String inputValue) {
        String numberRe = "[0-9]{1,10}";
        if (inputValue.matches(numberRe)) {
            try {
                int number = Integer.parseInt(inputValue);
                Option option = screenComponentService.findOptionOnScreenWithNumber(oldScreen, number);
                if (option != null) {
                    OptionType type = option.getOptionTypeId();
                    switch (com.omussd.generic.constant.OptionTypeConstant.getOptionTypeByType(type.getType())) {
                        case RETRY:
                        case BACK:
                            return processBackSystemOptionType(session, product);
                        case EXIT:
                            return processExitSystemOptionType(product);
                        case HOME:
                            return processHomeSystemOptionType(session, product, msisdn);
                        case STATIC:
                        default:
                            return null;
                    }
                }
            } catch (NumberFormatException nfe) {
                LoggingUtil.logError(logger, nfe, "Number could not be converted to integer");
                return null;
            }
        }

        return null;
    }

    private Screen processBackSystemOptionType(Session session, Product product) {
        LoggingUtil.logTrace(logger, "Processing a back/retry command");
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        if (sessionData.hasEmptyHistory()) {
            return intentService.processRootDeterminateForProduct(product, session);
        } else {
            SessionHistory history = sessionData.popSessionHistory();
            if (!sessionData.hasEmptyHistory()) {
                history = sessionData.peekSessionHistory();
            } else {
                LoggingUtil.logDebug(logger, "History stack doesn't have two pops for back. This will result in the same screen being shown. Moving to forced initial request with null product");
                //back on home screen. Revert to NullProduct root
                sessionData.clearData();
                SessionDataCache.INSTANCE.put(msisdn, sessionData);
                return processInitialRequest(session, null, true, -1);
            }
            Product prevProduct = productService.getProductByName(true, msisdn, history.getProductName());
            String screenCode = history.getScreenCode();
            return screenService.getScreenByCodeAndProduct(screenCode, prevProduct);
        }
    }

    private Screen processExitSystemOptionType(Product product) {
        LoggingUtil.logTrace(logger, "Processing an exit command");
        return screenService.getScreenByCodeAndProduct(SpecialScreenCodesConstant.EXIT_SCREEN.getCode(), product);
    }

    private Screen processHomeSystemOptionType(Session session, Product product, String msisdn) {
        LoggingUtil.logTrace(logger, "Processing a home command");
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        sessionData.clearData();
        SessionDataCache.INSTANCE.put(msisdn, sessionData);
        sessionService.updateSession(session);
        return intentService.processRootDeterminateForProduct(product, session);
    }
}
