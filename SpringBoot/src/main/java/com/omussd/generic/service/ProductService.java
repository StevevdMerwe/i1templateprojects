/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.SessionDataConstant;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.IntentDestination;
import com.omussd.generic.db.entity.IntentPurpose;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.ProductProperty;
import com.omussd.generic.db.repository.ProductPropertyRepository;
import com.omussd.generic.db.repository.ProductRepository;
import com.omussd.generic.exception.ProductFailureException;
import com.omussd.generic.request.IntentRequest;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.product.intent.IntentResponse;
import com.omussd.product.intent.IntentResponseType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * A service that handles processes around the Product
 *
 * @author XY40428
 */
@Component
public class ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPropertyRepository productPropertyRepository;

    @Value("${ussd.product.root.name}")
    private String rootProductName;
    @Value("${ussd.product.endpoint.intent}")
    private String productIntentEndpoint;
    @Value("${ussd.product.endpoint.range}")
    private String productRangeEndpoint;

    /**
     * Retrieves the Product by name form the database
     *
     * @param productName The name of the Product
     * @return The Product entity that has the name of the given parameter
     */
    public Product getProductByNameFromDb(String productName) {
        return productRepository.findByName(productName);
    }

    /**
     * Retrieves the Product by name, or none if the name is equivalent to the
     * root name.
     *
     * @param checkSession If true and needed, the session will be checked to
     * see if a product isn't already declared
     * @param msisdn The msisdn of the initial request
     * @param productName The name of the Product
     * @return The Product entity or null if no Product is found, or the Product
     * name is equivalent to the root name
     */
    public Product getProductByName(boolean checkSession, String msisdn, String productName) {
        Product product;
        if (productName.equals(rootProductName)) {
            if (checkSession) {
                product = findProductFromSession(msisdn);
            } else {
                return null;
            }
        } else {
            product = productRepository.findByName(productName);
            if (product == null) {
                if (checkSession) {
                    product = findProductFromSession(msisdn);
                } else {
                    return null;
                }
            }
        }
        return product;
    }

    /**
     * Retrieves all the Active, Enabled, Started, Non-Expired products from the
     * database.
     *
     * @return A list of all the working products in the database.
     */
    public List<Product> getAllWorkingProducts() {
        return productRepository.getAllWorkingProducts();
    }

    /**
     * Retrieves the ProductProperty entity of a given product
     *
     * @param product The Product to which the ProductProperty belongs
     * @return The ProductProperty entity
     */
    public ProductProperty findPropductPropertyByProduct(Product product) {
        if (product == null) {
            return null;
        } else {
            return productPropertyRepository.findByProductCollection(product);
        }
    }

    /**
     * Retrieves the URL of the Product to be used in order to reach the Product
     * implementation
     *
     * @param product The Product to which the URL belongs
     * @return The URL of the Product's implementation
     */
    public String findProductUrlByProduct(Product product) {
        ProductProperty productProperty = findPropductPropertyByProduct(product);
        return productProperty.getUrl();
    }

    /**
     * Checks whether a Product is expired or not.
     *
     * @param productProperty The ProductProperty of the Product that needs to
     * be checked
     * @return True if expired, False otherwise
     */
    public boolean isExpired(ProductProperty productProperty) {
        Date then = productProperty.getEndTime();
        return then == null ? false : nowAfterThen(then);
    }

    /**
     * Checks whether a Product has started or not.
     *
     * @param productProperty The ProductProperty of the Product that needs to
     * be checked
     * @return True if expired, False otherwise
     */
    public boolean hasStarted(ProductProperty productProperty) {
        Date then = productProperty.getStartTime();
        return then == null ? true : nowAfterThen(then);
    }

    private boolean nowAfterThen(Date then) {
        Date now = GregorianCalendar.getInstance().getTime();
        return now.after(then);
    }

    /**
     * Builds a request and sends the IntentRequest to the Product's
     * implementation
     *
     * @param product The Product to which the request belongs
     * @param intent The Intent that needs to be used to build the request
     * @param msisdn The msisdn of the original request
     * @return The IntentResponse that is the result of the call
     * @throws ProductFailureException If the call to the Product implementation
     * failed
     */
    public IntentResponse callProductIntent(Product product, Intent intent, String msisdn)
            throws ProductFailureException {
        IntentPurpose purpose = intent.getIntentPurposeId();
        IntentDestination destination = intent.getIntentDestinationId();
        String value = intent.getIntentValue();

        IntentRequest request = new IntentRequest();
        request.setDestination(destination.getName());
        request.setPurpose(purpose.getPurpose());
        request.setMsisdn(msisdn);
        request.setIntentValue(value);
        request.setParameterMap(SessionDataCache.INSTANCE.getSessionData(msisdn).getParamtersAsMap());

        StringBuilder url = new StringBuilder();
        url.append(findProductUrlByProduct(product));
        url.append(productIntentEndpoint);
        url.append(intent.getIntentTypeId().getType());

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.add("Accept", MediaType.APPLICATION_XML_VALUE);
        HttpEntity entity = new HttpEntity(request, headers);

        ResponseEntity<IntentResponse> respEntity = template.exchange(url.toString(), HttpMethod.POST, entity, IntentResponse.class, "");
        IntentResponse intentResponse = respEntity == null ? null : respEntity.getBody();

        if (!isIntentResponseSuccess(intentResponse)) {
            throw new ProductFailureException(intentResponse == null ? null : intentResponse.getIntentResponseType(),
                    intentResponse == null ? null : intentResponse.getMessage());
        }

        SessionDataCache.INSTANCE.getSessionData(msisdn).overrideParameters(intentResponse.getAdditionalParameters());

        return intentResponse;
    }

    /**
     * Calls the Product implementation to notify of a range selection of the
     * USSD request
     *
     * @param product The Product that needs to be called
     * @param range The range value that the Product must be notified of
     * @param msisdn The msisdn of the initial request
     */
    public void callProductRange(Product product, int range, String msisdn) {
        if (range == -1) {
            return;
        }

        StringBuilder url = new StringBuilder();
        url.append(findProductUrlByProduct(product));
        url.append(productRangeEndpoint);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url.toString())
                .queryParam("msisdn", msisdn)
                .queryParam("range", range);

        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        HttpEntity entity = new HttpEntity(headers);

        template.exchange(builder.build().encode().toUri(),
                HttpMethod.GET,
                entity,
                String.class);
    }

    private boolean isIntentResponseSuccess(IntentResponse ir) {
        if (ir == null) {
            return false;
        } else {
            return IntentResponseType.SUCCESS.equals(ir.getIntentResponseType());
        }
    }

    public Product findProductFromSession(String msisdn) {
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        if (sessionData != null) {
            String sessionProduct = sessionData.getFromSessionMap(SessionDataConstant.PRODUCT_NAME);
            if (sessionProduct != null) {
                LoggingUtil.logDebug(logger, "Product found in session: ", sessionProduct);
                return getProductByName(false, msisdn, sessionProduct);
            }
        }
        return null;
    }
}
