package com.omussd.generic.service;

import com.omussd.common.log.LoggingUtil;
import com.omussd.common.util.XMLUtil;
import com.omussd.generic.audit.AuditDataEntry;
import com.omussd.generic.constant.AuditDataEntryKey;
import com.omussd.generic.constant.AuditLogEvent;
import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.IntentDestination;
import com.omussd.generic.db.entity.IntentPurpose;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.ScreenDeterminate;
import com.omussd.generic.db.entity.ScreenScreenDeterminate;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.entity.TextPlaceholder;
import com.omussd.generic.db.repository.IntentDestinationRepository;
import com.omussd.generic.db.repository.IntentPurposeRepository;
import com.omussd.generic.db.repository.IntentRepository;
import com.omussd.generic.db.repository.IntentTypeRepository;
import com.omussd.generic.db.repository.ScreenRepository;
import com.omussd.generic.db.repository.TextPlaceholderRepository;
import com.omussd.generic.exception.ProductFailureException;
import com.omussd.generic.product.GenericProduct;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.bean.DynamicDeterminate;
import com.omussd.generic.session.bean.DynamicIntent;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.product.intent.IntentResponse;
import com.omussd.product.intent.IntentResponseType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A service that handles processes regarding intents as well as processes that
 * returns screens after intent processes
 *
 * @author XY40428
 */
@Component
public class IntentService {

    private static final Logger logger = LoggerFactory.getLogger(IntentService.class);

    @Autowired
    private AuditService auditService;
    @Autowired
    private IntentRepository intentRepository;
    @Autowired
    private ScreenRepository screenRepository;
    @Autowired
    private ScreenService screenService;
    @Autowired
    private ScreenComponentService screenComponentService;
    @Autowired
    private ProductService productService;
    @Autowired
    private TextPlaceholderRepository textPlaceholderRepository;
    @Autowired
    private GenericProduct genericProduct;
    @Autowired
    private IntentDestinationRepository destinationRepository;
    @Autowired
    private IntentPurposeRepository purposeRepository;
    @Autowired
    private IntentTypeRepository intentTypeRepository;

    /**
     * Processes an intent of type redirect that results in a Screen. A redirect
     * intent is either of type SUCCESS, FAILURE, or VALIDATION_FAILURE
     *
     * @param intent The Intent to be processes
     * @param session The session that instigated the processing
     * @param product The product to which instigated the Intent and to which
     * the Screen will belong.
     * @return The Screen that is the result of processing the Intent
     */
    public Screen processRedirectIntent(Intent intent, Session session, Product product) {
        logIntentAudit(intent, session, product);
        return processSuccessOrFailureIntent(intent, product);
    }

    /**
     * Processes an intent of type VALIDATION_FAILURE that results in a Screen.
     *
     * @param screen The to which the Intent belongs
     * @param inputComponent The InputComponent which is attached to the Intent
     * @param session The session that instigated the processing
     * @param product The product to which instigated the Intent and to which
     * the Screen will belong.
     * @return The Screen that is the result of processing the Intent
     */
    public Screen processValidationFailureIntent(Screen screen, InputComponent inputComponent, Session session, Product product) {
        Intent intent = intentRepository.findNonSubmitIntentOfInputComponentOfType(
                inputComponent, IntentTypeConstant.VALIDATION_FAILURE);
        if (intent == null) {
            intent = screen.getIntentId();
        }
        return processRedirectIntent(intent, session, product);
    }

    /**
     * Processes the intents that should fire upon a certain action being taken,
     * such as the user selecting an option or submitting input. These intents
     * must ultimately lead to the next Screen that must be displayed.
     *
     * @param session The session to track and change for this request
     * @param product The product that was chosen for this request
     * @param oldScreen The screen containing the responsible component that
     * caused for action to be taken
     * @param inputValue The value the user inputted
     * @param component The component responsible for action being taken
     * @param considerScreenIntent Will ignore the screen intent if false
     * @return The Screen that is to be displayed next
     */
    public Screen processPreIntents(boolean considerScreenIntent, Session session,
            Product product, Screen oldScreen, String inputValue, ScreenComponent component) {

        String msisdn = session.getMsisdn();
        IntentTypeConstant intentOverride = null;

        List<Intent> submitIntents = getSubmitIntentsFromScreenComponent(msisdn,
                oldScreen, inputValue, component);
        if (!submitIntents.isEmpty()) {
            intentOverride = processSubmitIntents(oldScreen, submitIntents,
                    session, msisdn, product, inputValue, component);
        }

        Intent intent = findNonSubmitActionIntent(considerScreenIntent, msisdn, oldScreen, inputValue, component, intentOverride);
        if (intent == null) {
            return null;
        }
        logIntentAudit(intent, session, product);
        return processNonSubmitActionIntent(oldScreen, session, intent, product, component, inputValue);
    }

    /**
     * Finds an intent of Determinate, Success, or Failure, attempting to find
     * them in order of Determinate, Success, Failure.
     *
     * @param msisdn The msisdn of the request
     * @param screen The screen holding the responsible component
     * @param inputValue The value the user inputted
     * @param component The component responsible for intents to be fired
     * @param includeDeterminates <b>TRUE</b> if Determinate intent types should
     * be included in the search, <b>FALSE</b> otherwise
     * @param override If only a specific intentType must be found, it can be
     * specified here. If override type is not found, null will be returned.
     * This ignores <i>includeDeterminates</i> parameter
     * @return The first available intent of type determinate, success, or
     * failure, null if none can be found.
     */
    private Intent findNonSubmitActionIntent(boolean considerScreenIntent, String msisdn, Screen screen,
            String inputValue, ScreenComponent component, IntentTypeConstant override) {
        Intent intent = null;

        if (override != null && !override.equals(IntentTypeConstant.SUCCESS)) {
            intent = getIntentFromScreenComponentOfIntentType(msisdn, screen,
                    inputValue, component, override);
        } else {
            IntentTypeConstant[] types = new IntentTypeConstant[]{IntentTypeConstant.DETERMINATE,
                IntentTypeConstant.SUCCESS, IntentTypeConstant.FAILURE};
            for (IntentTypeConstant type : types) {
                intent = getIntentFromScreenComponentOfIntentType(msisdn, screen,
                        inputValue, component, type);

                if (intent != null) {
                    break;
                }
            }
        }

        if (intent == null && considerScreenIntent) {
            intent = screen.getIntentId();
        }

        return intent;
    }

    /**
     * Processes the intents that must be fired in order to construct a screen
     * correctly, such as dynamic menu or text variable intents.
     *
     * @param screen The screen holding the components that require intents to
     * be constructed correctly
     * @param session The session to track and change for this request
     * @param product The product that was chosen for this request
     * @throws ProductFailureException
     */
    public void processPostIntents(Screen screen, Session session, Product product)
            throws ProductFailureException {
        String msisdn = session.getMsisdn();

        //Options
        SessionData sessionData = processOptionIntents(screen, session, product);
        SessionDataCache.INSTANCE.put(msisdn, sessionData);

        //Text variables
        sessionData = processTextVariableIntents(screen, session, product);
        SessionDataCache.INSTANCE.put(msisdn, sessionData);

    }

    /**
     * Processes intents of type option to retrieve Dynamic Options.
     *
     * @param screen The screen on which the options reside on
     * @param session The session that instigated the request
     * @param product The product to which the screen and options belong
     * @return The modified SessionData that now contains the Dynamic Options
     * @throws ProductFailureException If the call to the product service failed
     * or the intent is not found.
     */
    private SessionData processOptionIntents(Screen screen, Session session, Product product) throws ProductFailureException {
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);

        Collection<OptionGroupComponent> ogcs = screenComponentService
                .getDynamicOptionGroupComponentsByScreen(screen);
        for (OptionGroupComponent ogc : ogcs) {
            Intent intent = ogc.getIntentId();
            if (intent == null) {
                throw new ProductFailureException(IntentResponseType.ERROR, "Dynamic option group exists without intent");
            }
            DynamicOptionGroup dynamicOptionGroup = processOptionIntent(session, screen, ogc, product, intent);
            sessionData.addDynamicOptionGroup(dynamicOptionGroup);
        }

        return sessionData;
    }

    /**
     * Processes a single option intent to retrieve a DynamicOptionGroup.
     *
     * @param session The session that instigated the request
     * @param screen The screen on which the options reside
     * @param ogc The OptionGroupComponent in which the DynamicOptions must
     * reside
     * @param product The product to which the screen belongs
     * @param intent The intent of the OptionGroupComponent
     * @return The DynamicOptionGroup containing the DynamicOptions
     * @throws ProductFailureException When the call to the product failed
     */
    private DynamicOptionGroup processOptionIntent(Session session, Screen screen,
            OptionGroupComponent ogc, Product product, Intent intent) throws ProductFailureException {
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);
        DynamicOptionGroup dynamicOptionGroup;
        IntentResponse response;
        if (product != null) {
            auditService.logAudit(AuditLogEvent.PROCESS_INTENT, intent, session, new AuditDataEntry[]{
                AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME, product.getName()),
                AuditDataEntry.getEntry(AuditDataEntryKey.SCREEN_CODE, screen.getCode())});

            response = productService.callProductIntent(product, intent, msisdn);
        } else {
            auditService.logAudit(AuditLogEvent.PROCESS_INTENT, intent, session, new AuditDataEntry[]{
                AuditDataEntry.getEntry(AuditDataEntryKey.SCREEN_CODE, screen.getCode())});
            IntentDestination destination = intent.getIntentDestinationId();

            response = genericProduct.processIntent(intent.getIntentPurposeId().getPurpose(),
                    destination == null ? null : destination.getName(),
                    sessionData.getParamtersAsMap(),
                    intent.getIntentValue(),
                    com.omussd.product.constant.IntentTypeConstant.OPTIONS);
        }

        if (!isIntentResponseSuccess(response)) {
            throw new ProductFailureException(IntentResponseType.ERROR, "Failed to retrieve options from generic product");
        }

        String xml = response.getValue();
        try {
            dynamicOptionGroup = XMLUtil.fromXML(DynamicOptionGroup.class, xml);
        } catch (JAXBException e) {
            throw new ProductFailureException(IntentResponseType.ERROR, "Failed to parse dynamic options");
        }
        dynamicOptionGroup.setName(ogc.getName());
        dynamicOptionGroup.setParameter(ogc.getParameterName());

        //update addtional data from the submit
        Map<String, String> additionalParams = response.getAdditionalParameters();
        for (Entry<String, String> entry : additionalParams.entrySet()) {
            SessionDataCache.INSTANCE.getSessionData(msisdn)
                    .addParameter(entry.getKey(), entry.getValue());
        }

        return dynamicOptionGroup;
    }

    /**
     * Processes intents of type TEXT_PARAMETER_VALUE
     *
     * @param screen The screen on which the text variables reside
     * @param session The session that instigated the request
     * @param product The product to which the Screen belongs
     * @return The modified SessionData that contains the text variables
     * @throws ProductFailureException When the call to the product
     * implementation failed
     */
    private SessionData processTextVariableIntents(Screen screen, Session session, Product product) throws ProductFailureException {
        String msisdn = session.getMsisdn();
        SessionData sessionData = SessionDataCache.INSTANCE.getSessionData(msisdn);

        List<TextPlaceholder> placeholders = textPlaceholderRepository.findByScreenId(screen);
        for (TextPlaceholder placeholder : placeholders) {
            Intent intent = placeholder.getIntentId();
            IntentResponse response;
            if (product != null) {
                auditService.logAudit(AuditLogEvent.PROCESS_INTENT, intent, session, new AuditDataEntry[]{
                    AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME, product.getName()),
                    AuditDataEntry.getEntry(AuditDataEntryKey.SCREEN_CODE, screen.getCode()),
                    AuditDataEntry.getEntry(AuditDataEntryKey.TEXT_VARIABLE_NAME, placeholder.getVariable())});
                intent.setIntentValue(placeholder.getVariable());
                response = productService.callProductIntent(product, intent, msisdn);
            } else {
                response = genericProduct.processIntent(intent.getIntentPurposeId().getPurpose(),
                        intent.getIntentDestinationId().getName(),
                        sessionData.getParamtersAsMap(),
                        intent.getIntentValue(),
                        com.omussd.product.constant.IntentTypeConstant.TEXT_PARAMETER_VALUE);
                if (!isIntentResponseSuccess(response)) {
                    throw new ProductFailureException(response.getIntentResponseType(),
                            "Failed to retrieve text variable from generic product");
                }
            }
            String value = response.getValue();
            sessionData.addTextVariable(placeholder.getVariable(), value);

            //update addtional data from the submit
            Map<String, String> additionalParams = response.getAdditionalParameters();
            for (Entry<String, String> entry : additionalParams.entrySet()) {
                SessionDataCache.INSTANCE.getSessionData(msisdn)
                        .addParameter(entry.getKey(), entry.getValue());
            }
        }

        return sessionData;
    }

    /**
     * Processes intents of type SUBMIT
     *
     * @param oldScreen The screen which contains the ScreenComponent that holds
     * the Intent(s)
     * @param submitIntents The list of Intents that need processing
     * @param session The session that instigated the request
     * @param msisdn The msisdn to which the Session belongs
     * @param product The Product to which the Screen belongs
     * @param inputValue The value that the user inputted
     * @param component The ScreenComponent that holds the Intent(s)
     * @return SUCCESS if all Intents are processes successfully, DETERMINATE if
     * at least one SUBMIT Intent calls for the next Intent processed to
     * explicitly be of type DETERMINATE, and FAILURE or VALIDATION_FAILURE if a
     * SUBMIT fails to process successfully. A FAILURE or VALIDATION_FAILURE
     * will cause the processes to seize instead of continuing processing the
     * remaining SUBMIT Intents
     */
    private IntentTypeConstant processSubmitIntents(Screen oldScreen, List<Intent> submitIntents, Session session,
            String msisdn, Product product,
            String inputValue, ScreenComponent component) {
        IntentTypeConstant type = null;
        for (Intent intent : submitIntents) {
            logIntentAudit(intent, session, product);
            IntentResponse submitResponse;
            try {
                submitResponse = processSubmitIntent(oldScreen, product, intent,
                        msisdn, component, inputValue);
            } catch (ProductFailureException e) {
                LoggingUtil.logError(logger, e, "Product failure on processing submit intent");
                auditService.logAudit(AuditLogEvent.INTENT_PROCESSING_ERROR, intent,
                        session, new AuditDataEntry[]{AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME,
                                    product == null ? null : product.getName())});
                return IntentTypeConstant.FAILURE;
            }

            //update addtional data from the submit
            Map<String, String> additionalParams = submitResponse.getAdditionalParameters();
            for (Entry<String, String> entry : additionalParams.entrySet()) {
                SessionDataCache.INSTANCE.getSessionData(msisdn)
                        .addParameter(entry.getKey(), entry.getValue());
            }

            if (submitResponse.getType().getIntentType().equals(IntentTypeConstant.FAILURE.getIntentType())
                    || submitResponse.getType().getIntentType().equals(IntentTypeConstant.VALIDATION_FAILURE.getIntentType())) {
                return IntentTypeConstant.getIntentTypeByType(submitResponse.getType().getIntentType());
            } else if (submitResponse.getType().getIntentType().equals(IntentTypeConstant.DETERMINATE.getIntentType())) {
                type = IntentTypeConstant.DETERMINATE;
            }
        }
        return type == null ? IntentTypeConstant.SUCCESS : type;
    }

    /**
     * Processes a single SUBMIT Intent.
     *
     * @param oldScreen The screen which contains the ScreenComponent that holds
     * the Intent(s)
     * @param product The Product to which the Screen belongs
     * @param intent The Intent that requires processing
     * @param msisdn The msisdn belonging to the Session that instigated the
     * request
     * @param component The ScreenComponent that holds the Intent
     * @param inputValue The value that the user inputted
     * @return The IntentResponse as retrieved from the Product implementation
     * @throws ProductFailureException When the call to the Product
     * implementation failed
     */
    private IntentResponse processSubmitIntent(Screen oldScreen, Product product, Intent intent,
            String msisdn, ScreenComponent component, String inputValue) throws ProductFailureException {
        Map<String, String> parameterMap = SessionDataCache.INSTANCE.getSessionData(msisdn).getParamtersAsMap();
        if (component.getParameterName() != null) {
            String componentValue = screenComponentService.getInputValueFromComponent(oldScreen, component, msisdn, inputValue);
            if (componentValue != null) {
                parameterMap.put(component.getParameterName(), componentValue);
            }
        }

        return productService.callProductIntent(product, intent, msisdn);
    }

    /**
     * Processes an Intent of type DETERMINATE, SUCCESS, FAILURE or
     * VALIDATION_FAILURE
     *
     * @param oldScreen The screen which contains the ScreenComponent that holds
     * the Intent(s)
     * @param session The session that instigated the request
     * @param intent The Intent that requires processing
     * @param product The Product to which the Screen belongs
     * @param screenComponent The ScreenComponent that holds the Intent
     * @param inputValue The value that the user inputted
     * @return The Screen that is the result of the processing of the Intent,
     * null if the Intent is not of the expected types, or if no appropriate
     * screen can be found
     */
    private Screen processNonSubmitActionIntent(Screen oldScreen, Session session,
            Intent intent, Product product, ScreenComponent screenComponent, String inputValue) {
        if (intent == null) {
            return null;
        }

        IntentTypeConstant type = IntentTypeConstant.getIntentTypeByType(intent.getIntentTypeId().getType());
        Screen screen;
        switch (type) {
            case DETERMINATE:
                try {
                    screen = processDeterminateIntent(oldScreen, product, intent, session.getMsisdn(), screenComponent, inputValue);
                } catch (ProductFailureException e) {
                    LoggingUtil.logError(logger, e, "Failed processing determinate intent");
                    auditService.logAudit(AuditLogEvent.INTENT_PROCESSING_ERROR, intent,
                            session, new AuditDataEntry[]{
                                AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME,
                                        product == null ? null : product.getName())});
                    if (oldScreen == null) {
                        screen = screenService.getFailureScreen(product);
                    } else {
                        screen = processSuccessOrFailureIntent(oldScreen.getIntentId(), product);
                    }
                }
                break;
            case SUCCESS:
            case FAILURE:
            case VALIDATION_FAILURE:
                screen = processSuccessOrFailureIntent(intent, product);
                break;
            default:
                screen = null;
        }
        return screen;
    }

    /**
     * Processes an Intent of type SUCCES or FAILURE. Normally called by another
     * method within the Service instead of directly.
     *
     * Processing a SUCCESS or FAILURE attempt means retrieving the screen from
     * the database.
     *
     * @param intent The Intent that needs to be processed
     * @param product The Product that is currently is use for the original
     * request
     * @return The screen that is the result of the processing.
     */
    private Screen processSuccessOrFailureIntent(Intent intent, Product product) {
        /*
            In the case of success / fail, it is always a redirect to another screen
         */
        return screenService.getScreenByCodeAndProduct(intent.getIntentValue(), product);
    }

    /**
     * Processes the initial DETERMINATE Intent for the product. This Intent
     * does not have an origin screen.
     *
     * @param product The Product to which this root DETERMINATE Intent belongs
     * @param session The Session that instigated the request
     * @return The screen that is the result of the processing
     */
    public Screen processRootDeterminateForProduct(Product product, Session session) {
        if (product == null) {
            return screenService.getRootScreenForNullProduct();
        } else {
            Intent intent = product.getIntentId();
            if (intent == null || !intent.getIntentTypeId().getType().equals(IntentTypeConstant.DETERMINATE.getIntentType())) {
                LoggingUtil.logWarn(logger, "Product ", product, " not set up correctly");
                return screenService.getFailureScreen(product);
            } else {
                try {
                    logIntentAudit(intent, session, product);
                    return processDeterminateIntent(null, product, intent, session.getMsisdn(), null, null);
                } catch (ProductFailureException e) {
                    LoggingUtil.logError(logger, e, "Error attemptign to retrieve product's root menu via determinate");
                    return screenService.getFailureScreen(product);
                }
            }
        }
    }

    /**
     * Processes a DETERMINATE Intent.
     *
     * @param oldScreen The screen which contains the ScreenComponent that holds
     * the Intent(s)
     * @param product The Product to which this root DETERMINATE Intent belongs
     * @param intent The Intent that needs to be processed
     * @param msisdn The msisdn belonging to the Session that instigated the
     * request
     * @param component The ScreenComponent that holds the Intent
     * @param inputValue The value that the user inputted
     * @return The Screen that is the result of the processing
     * @throws ProductFailureException When the call to the Product
     * implementation failed
     */
    private Screen processDeterminateIntent(Screen oldScreen, Product product, Intent intent,
            String msisdn, ScreenComponent component, String inputValue) throws ProductFailureException {
        Map<String, String> parameterMap = SessionDataCache.INSTANCE.getSessionData(msisdn).getParamtersAsMap();
        if (component != null && component.getParameterName() != null && oldScreen != null) {
            String componentValue = screenComponentService.getInputValueFromComponent(oldScreen, component, msisdn, inputValue);
            if (componentValue != null) {
                parameterMap.put(component.getParameterName(), componentValue);
            }
        }

        IntentResponse intentResponse = productService.callProductIntent(product, intent, msisdn);

        //update addtional data from the submit
        Map<String, String> additionalParams = intentResponse.getAdditionalParameters();
        for (Entry<String, String> entry : additionalParams.entrySet()) {
            SessionDataCache.INSTANCE.getSessionData(msisdn)
                    .addParameter(entry.getKey(), entry.getValue());
        }

        if (!isIntentResponseSuccess(intentResponse)) {
            String code = SpecialScreenCodesConstant.PRODUCT_FAILURE.getCode();
            if (oldScreen != null) {
                code = oldScreen.getCode();
            }
            screenService.getScreenByCodeAndProduct(code, product);
        }

        String determinateValue = intentResponse.getValue();

        if ((component instanceof OptionGroupComponent)
                && (screenComponentService.isOptionGroupComponentDynamic((OptionGroupComponent) component))) {
            return processDynamicDeterminateIntent(intent, determinateValue);
        } else {
            return screenRepository.
                    findByScreenScreenDeterminateCollectionIntentIdAndScreenScreenDeterminateCollectionScreenDeterminateIdProductIdAndScreenScreenDeterminateCollectionScreenDeterminateIdValue(
                            intent, product, determinateValue);
        }
    }

    /**
     * Processes a DynamicIntent of type DETERMINATE. A dynamic Intent is not
     * stored in the database, but built or retrieved at runtime.
     *
     * @param intent The Intent that needs to be processed
     * @param determinateValue The value of what Determinate to find
     * @return The Screen within the ScreenScreenDeterminate that corresponds
     * with the ScreenDeterminate value
     */
    private Screen processDynamicDeterminateIntent(Intent intent, String determinateValue) {
        Collection<ScreenScreenDeterminate> ssds = intent.getScreenScreenDeterminateCollection();
        if (ssds == null) {
            return null;
        }

        for (ScreenScreenDeterminate ssd : ssds) {
            ScreenDeterminate sd = ssd.getScreenDeterminateId();
            if (determinateValue.equals(sd.getValue())) {
                return ssd.getScreenId();
            }
        }

        return null;
    }

    /**
     * Converts a DynamicIntent into an Intent Entity in order to have it
     * undergo further processing.
     *
     * @param product The Product to which the DynamicIntent belongs
     * @param dynamicIntent The DynamicIntent that requires conversion
     * @return The Entity Intent that is the result of the conversion
     */
    private Intent getIntentFromDynamicIntent(Product product, DynamicIntent dynamicIntent) {
        IntentPurpose purpose = new IntentPurpose();
        purpose.setPurpose(dynamicIntent.getPurpose());

        IntentDestination destination = new IntentDestination();
        destination.setName(dynamicIntent.getDestination());

        com.omussd.generic.db.entity.IntentType type = new com.omussd.generic.db.entity.IntentType();
        type.setType(dynamicIntent.getIntentType());

        Intent intent = new Intent();
        intent.setIntentValue(dynamicIntent.getValue());
        intent.setIntentDestinationId(destination);
        intent.setIntentPurposeId(purpose);
        intent.setIntentTypeId(type);

        List<ScreenScreenDeterminate> ssDeterminates = new ArrayList<>();
        if (dynamicIntent.getDeterminates() != null) {
            for (DynamicDeterminate dd : dynamicIntent.getDeterminates()) {
                ScreenScreenDeterminate ssd = new ScreenScreenDeterminate();
                ScreenDeterminate sd = new ScreenDeterminate();
                sd.setValue(dd.getValue());
                ssd.setScreenId(screenService.getScreenByCodeAndProduct(dd.getScreenCode(), product));
                ssd.setIntentId(intent);
                ssd.setScreenDeterminateId(sd);
                ssDeterminates.add(ssd);
            }
        }
        intent.setScreenScreenDeterminateCollection(ssDeterminates);
        return intent;
    }

    /**
     * Gets all the Intents of type SUBMIT linked to a given ScreenComponent
     *
     * @param msisdn The msisdn belonging to the Session that instigated the
     * request
     * @param screen The Screen to which the ScreenComponent belongs
     * @param userInput The input the user gave
     * @param screenComponent The ScreenComponent that is linked to the Intents
     * @return A List of all the SUBMIT Intents linked to the given
     * ScreenComponent
     */
    private List<Intent> getSubmitIntentsFromScreenComponent(String msisdn,
            Screen screen, String userInput, ScreenComponent screenComponent) {
        /*
            Options have preference. Find the option with with the number matching input.
            If UserInput isnt a number, skip this step.
         */
        if (screenComponent instanceof OptionGroupComponent) {
            int number = Integer.parseInt(userInput);
            List<Intent> intents;
            if (OptionGroupComponentTypeConstant.STATIC.getType().equals(((OptionGroupComponent) screenComponent).getOptionGroupComponentTypeId().getType())) {
                intents = intentRepository.findSubmitIntentsOfOptionOnScreen(screen,
                        number);
                if (intents != null) {
                    return intents;
                }
            } else {
                /*
                    Option may be dynamic
                 */
                List<DynamicIntent> dynamicIntents = findDynamicIntentsFromDynamicOptionGroupContianingOptionWithNumber(
                        msisdn, number);
                if (!dynamicIntents.isEmpty()) {
                    intents = new ArrayList<>();
                    for (DynamicIntent di : dynamicIntents) {
                        if (IntentTypeConstant.SUBMIT.getIntentType().equals(di.getIntentType())) {
                            Intent intent = getIntentFromDynamicIntent(screen.getProductId(), di);
                            intents.add(intent);
                        }
                    }
                    return intents;
                }
            }
        } else {
            //must be input component
            List<Intent> intents = intentRepository.findSubmitIntentsOfInputComponentOnScreen(screen);
            if (intents != null) {
                return intents;
            }
        }

        //Intent not found
        return new ArrayList<>();
    }

    /**
     * Retrieves an Intent linked to the ScreenComponent that is NOT of type
     * SUBMIT
     *
     * @param msisdn The msisdn belonging to the Session that instigated the
     * request
     * @param screen The Screen to which the ScreenComponent belongs
     * @param userInput The input the user gave
     * @param screenComponent The ScreenComponent that is linked to the Intents
     * @param intentType The type of the Intent that must be retrieved, and is
     * not of type SUBMIT
     * @return The Intent that is of the given type and linked to the given
     * ScreenComponent
     */
    private Intent getIntentFromScreenComponentOfIntentType(String msisdn,
            Screen screen, String userInput, ScreenComponent screenComponent, IntentTypeConstant intentType) {

        Intent intent;
        /*
            Options have preference. Find the option with with the number matching input.
            If UserInput isnt a number, skip this step.
         */
        if (screenComponent instanceof OptionGroupComponent) {
            intent = findIntentFromOptionGroupComponentOfIntentType(screen, msisdn, userInput, intentType);
        } else {
            //must be input component
            intent = findIntentFromInputComponentOfIntentType(screen, intentType);
        }

        return intent;
    }

    /**
     * Retrieves an Intent of a given type that is not SUBMIT from specifically
     * an OptionGroupCmponent
     *
     * @param screen The Screen to which the OptionGroupComponent belongs
     * @param userInput The input the user gave
     * @param intentType The type of the Intent that must be retrieved, and is
     * not of type SUBMIT
     * @return The Intent that is of the given type and linked to the given
     * OptionGroupComponent
     */
    private Intent findIntentFromOptionGroupComponentOfIntentType(Screen screen, String msisdn,
            String userInput, IntentTypeConstant intentType) {
        int number = Integer.parseInt(userInput);
        Intent intent = intentRepository.findIntentOfOptionOnScreenOfType(screen,
                number, intentType);
        if (intent != null) {
            return intent;
        } else {
            /*
                Option may be dynamic
             */
            List<DynamicIntent> dynamicIntents = findDynamicIntentsFromDynamicOptionGroupContianingOptionWithNumber(
                    msisdn, number);
            if (!dynamicIntents.isEmpty()) {
                for (DynamicIntent di : dynamicIntents) {
                    if (di.getIntentType().equals(intentType.getIntentType())) {
                        return getIntentFromDynamicIntent(screen.getProductId(), di);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Retrieves an Intent of a given type that is not SUBMIT from specifically
     * an InputComponent
     *
     * @param screen The Screen to which the InputComponent belongs
     * @param intentType The type of the Intent that must be retrieved, and is
     * not of type SUBMIT
     * @return The Intent that is of the given type and linked to the given
     * InputComponent
     */
    private Intent findIntentFromInputComponentOfIntentType(Screen screen, IntentTypeConstant intentType) {
        Intent intent = intentRepository.findIntentOfInputComponentOnScreenOfType(screen,
                intentType);
        if (intent != null) {
            return intent;
        }
        return null;
    }

    /**
     * Retrieves all the DynamicIntents from a a DynamicOptionGroup that
     * contains an option with the given number. Only the current Screen's
     * DynamicOptionGroups are stored within the SessionData
     *
     * @param msisdn The msisdn which belongs to the Session that instigated the
     * request
     * @param number The number of the option
     * @return A list of DynamicIntents that are linked to the Option within the
     * DynamicOptionGroup
     */
    private List<DynamicIntent> findDynamicIntentsFromDynamicOptionGroupContianingOptionWithNumber(
            String msisdn, int number) {
        //Get all dynamic groups from session
        List<DynamicOptionGroup> dogList = SessionDataCache.INSTANCE.getSessionData(msisdn).getDynamicOptionGroups();
        //See if one of them have the given number value
        for (DynamicOptionGroup dog : dogList) {
            for (DynamicOption dyo : dog.getDynamicOptions()) {
                if (dyo.getNumber() == number) {
                    //found a match!
                    return dyo.getDynamicIntents();
                }
            }
        }
        return new ArrayList<>();
    }

    /**
     * Checks is the IntentResponse has an IntentResponseType of SUCCESS
     *
     * @param ir The IntentResponse to check
     * @return True is the IntentResponseType is SUCCESS, False otherwise
     */
    private boolean isIntentResponseSuccess(IntentResponse ir) {
        return IntentResponseType.SUCCESS.equals(ir.getIntentResponseType());
    }

    /**
     * Convenience method to perform an Audit log for processing an Intent.
     *
     * @param intent The Intent that is being processes
     * @param session The session that instigated the request
     * @param product The Product to which the Session belongs
     */
    private void logIntentAudit(Intent intent, Session session, Product product) {
        if (intent.getId() != null) {
            auditService.logAudit(AuditLogEvent.PROCESS_INTENT, intent, session,
                    new AuditDataEntry[]{
                        AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME,
                                product == null ? null : product.getName())});
        } else {
            auditService.logAudit(AuditLogEvent.PROCESS_INTENT, null, session, new AuditDataEntry[]{
                AuditDataEntry.getEntry(AuditDataEntryKey.PRODUCT_NAME, product == null ? null : product.getName()),
                AuditDataEntry.getEntry(AuditDataEntryKey.DYNAMIC_INTENT_DESTINATION, intent.getIntentDestinationId() == null ? null : intent.getIntentDestinationId().getName()),
                AuditDataEntry.getEntry(AuditDataEntryKey.DYNAMIC_INTENT_PURPOSE, intent.getIntentPurposeId() == null ? null : intent.getIntentPurposeId().getPurpose()),
                AuditDataEntry.getEntry(AuditDataEntryKey.DYNAMIC_INTENT_TYPE, intent.getIntentTypeId().getType()),
                AuditDataEntry.getEntry(AuditDataEntryKey.DYNAMIC_INTENT_VALUE, intent.getIntentValue())}
            );
        }
    }

    /**
     * Creates an Intent record in the database or retrieves the existing one
     * based upon the given parameters
     *
     * @param product The Product of the Intent
     * @param intentTypeConstant The Intent's type
     * @param destination The destination of the Intent
     * @param purpose The purpose of the Intent
     * @param value The additional and optional value of the Intent
     * @return The Intent that is created or retrieved
     */
    public Intent createOrGetIntent(Product product, IntentTypeConstant intentTypeConstant, String destination,
            String purpose, String value) {
        Intent intent = intentRepository.findIntentByProductAndDestinationAndPurposeAndValueAndType(product,
                destination, purpose, value, intentTypeConstant);

        if (intent == null) {
            IntentDestination intentDestination = null;
            if (destination != null) {
                intentDestination = destinationRepository.findByName(destination);
                if (intentDestination == null) {
                    intentDestination = createIntentDestination(destination);
                }
            }

            IntentPurpose intentPurpose = null;
            if (purpose != null) {
                intentPurpose = purposeRepository.findByPurpose(purpose);
                if (intentPurpose == null) {
                    intentPurpose = createIntentPurpose(purpose);
                }
            }

            intent = new Intent();
            intent.setIntentValue(value);
            intent.setIntentDestinationId(intentDestination);
            intent.setIntentPurposeId(intentPurpose);
            intent.setIntentTypeId(intentTypeRepository.findByType(intentTypeConstant.getIntentType()));
            intent.setProductId(product);
            return intentRepository.save(intent);
        } else {
            return intent;
        }
    }

    /**
     * Creates an IntentDestination Entity and saves it to the database
     *
     * @param destination The Destination value of the IntentDestination Entity
     * @return The persisted IntentDestination Entity
     */
    private IntentDestination createIntentDestination(String destination) {
        IntentDestination intentDestination = new IntentDestination();
        intentDestination.setName(destination);
        return destinationRepository.save(intentDestination);
    }

    /**
     * Creates an IntentPurpose Entity and saves it to the database
     *
     * @param purpose The Purpose value of the IntentPurpose Entity
     * @return The persisted IntentPurpose Entity
     */
    private IntentPurpose createIntentPurpose(String purpose) {
        IntentPurpose intentPurpose = new IntentPurpose();
        intentPurpose.setPurpose(purpose);
        return purposeRepository.save(intentPurpose);
    }
}
