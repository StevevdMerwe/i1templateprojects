/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum OptionTypeConstant {
    STATIC("STATIC"),
    BACK("BACK"),
    HOME("HOME"),
    RETRY("RETRY"),
    EXIT("EXIT");

    private final String type;

    private OptionTypeConstant(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static OptionTypeConstant getOptionTypeByType(String type) {
        for (OptionTypeConstant ot : values()) {
            if (ot.getType().equals(type)) {
                return ot;
            }
        }
        return null;
    }
}
