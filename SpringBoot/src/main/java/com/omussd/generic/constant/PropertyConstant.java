/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum PropertyConstant {
    OPTION_PROMPT_DELIMINATOR,
    SESSION_TTL,
    SESSION_CLEANUP_DELAY_PERIOD;
}
