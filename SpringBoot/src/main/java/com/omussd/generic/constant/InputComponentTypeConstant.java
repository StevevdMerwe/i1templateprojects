/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum InputComponentTypeConstant {
    ALPHANUMERIC("Alphanumeric"),
    NUMBER("Number"),
    RSA_LEGAL_ID("RSALegalId"),
    CELL_NUMBER("CellNumber");
    
    private final String type;
    
    private InputComponentTypeConstant(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
    
    public static InputComponentTypeConstant getInputComponentTypeByType(String type) {
        for (InputComponentTypeConstant typeC: values()) {
            if (typeC.getType().equals(type)) {
                return typeC;
            }
        }
        return null;
    }
}