/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum ScreenTypeConstant {
    ACTION("ACTION"),
    END("END");
    
    private final String code;
    
    private ScreenTypeConstant(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    
    public static ScreenTypeConstant getScreenTypeByCode(String code) {
        for (ScreenTypeConstant type: values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return null;
    }
}
