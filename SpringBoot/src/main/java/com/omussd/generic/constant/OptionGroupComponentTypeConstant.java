/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum OptionGroupComponentTypeConstant {
    STATIC("STATIC"),
    DYNAMIC("DYNAMIC");

    private final String type;

    private OptionGroupComponentTypeConstant(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static OptionGroupComponentTypeConstant findByType(String type) {
        for (OptionGroupComponentTypeConstant c : values()) {
            if (c.getType().equals(type)) {
                return c;
            }
        }
        return null;
    }
}
