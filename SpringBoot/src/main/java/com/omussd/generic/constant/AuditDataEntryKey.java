/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum AuditDataEntryKey {
    NETWORK_OF_SESSION,
    SESSION_SART_TIME,
    PRODUCT_NAME,
    PROMPT,
    SCREEN_CODE,
    TEXT_VARIABLE_NAME,
    DYNAMIC_INTENT_DESTINATION,
    DYNAMIC_INTENT_PURPOSE,
    DYNAMIC_INTENT_VALUE,
    DYNAMIC_INTENT_TYPE;
}
