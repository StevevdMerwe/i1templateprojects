package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum AuditLogEvent {
    INCOMING_REQUEST,
    SESSION_ALREADY_IN_USE,
    OUTGOING_RESPONSE,
    PREPARE_ABORT_RESPONSE,
    FOUND_EXISTING_SESSION,
    CREATED_NEW_SESSION,
    PROCESS_INTENT,
    INTENT_PROCESSING_ERROR;

}
