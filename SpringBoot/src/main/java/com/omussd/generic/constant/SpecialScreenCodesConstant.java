package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum SpecialScreenCodesConstant {
    RECOGNISED_ROOT("0-0"),
    UNRECOGNISED_ROOT("0-1"),
    NETWORK_SESSION_EXPIRED_ROOT("8-1"),
    SYSTEM_SESSION_EXPIRED_ROOT("8-2"),
    EXIT_SCREEN("0-4"),
    SESSION_IN_USE("0-5"),
    PRODUCT_FAILURE("9-9"),
    PRODUCT_DISABLED("9-8"),
    PRODUCT_EXPIRED("9-7"),
    PRODUCT_NOT_STARTED("9-6"),
    WRONG_OPTION("7-1");

    private final String code;
    
    private SpecialScreenCodesConstant(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
