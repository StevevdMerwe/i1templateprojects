/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum IntentTypeConstant {
    SUCCESS("SUCCESS"),
    FAILURE("FAILURE"),
    VALIDATION_FAILURE("VALIDATION_FAILURE"),
    SUBMIT("SUBMIT"),
    DETERMINATE("DETERMINATE"),
    OPTIONS("OPTIONS"),
    TEXT_PARAMETER_VALUE("TEXT_PARAMETER_VALUE");
    
    private final String value;
    
    private IntentTypeConstant(String value) {
        this.value = value;
    }

    public String getIntentType() {
        return value;
    }
    
    public static IntentTypeConstant getIntentTypeByType(String type) {
        for (IntentTypeConstant tc : values()) {
            if (tc.getIntentType().equals(type)) {
                return tc;
            }
        }
        return null;
    }    
}
