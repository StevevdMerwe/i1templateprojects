/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum NetworkConstant {
    CELL_C("Cell C"),
    MTN("MTN"),
    VODACOM("Vodacom"),
    TELKOM("Telkom"),
    OTHER("Other");
    
    private final String name;
    
    private NetworkConstant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static NetworkConstant getNetworkByName(String name) {
        for (NetworkConstant network: values()) {
            if (network.getName().equals(name)) {
                return network;
            }
        }
        return null;
    }
}
