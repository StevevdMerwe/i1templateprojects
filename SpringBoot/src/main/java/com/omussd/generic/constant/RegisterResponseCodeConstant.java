/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.constant;

/**
 *
 * @author XY40428
 */
public enum RegisterResponseCodeConstant {
    SUCCESS(0),
    FAILURE(1),
    ALREADY_REGISTERED(2);

    private int code;

    private RegisterResponseCodeConstant(Integer code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static RegisterResponseCodeConstant getRegisterResponseCodeConstantFromCode(int code) {
        for (RegisterResponseCodeConstant constant : values()) {
            if (code == constant.code) {
                return constant;
            }
        }
        return null;
    }
}
