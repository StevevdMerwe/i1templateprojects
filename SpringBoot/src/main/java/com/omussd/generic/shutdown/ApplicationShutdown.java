/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.shutdown;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.startup.ScheduleService;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author XY40428
 */
@Component
public class ApplicationShutdown {

    private Logger logger = LoggerFactory.getLogger(ApplicationShutdown.class);
    
    @Autowired
    private ScheduleService scheduleService;
    
    @PreDestroy
    public void onShutdown() {
        LoggingUtil.logInfo(logger, "onShutdown called");
        scheduleService.cancelGlobalSchedulers();
        LoggingUtil.logInfo(logger, "onShutdown complete");
    }
}