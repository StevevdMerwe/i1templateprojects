/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SCREEN_DETERMINATE")
@NamedQueries({
    @NamedQuery(name = "ScreenDeterminate.findAll", query = "SELECT s FROM ScreenDeterminate s")})
public class ScreenDeterminate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "VALUE")
    private String value;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "screenDeterminateId")
    private Collection<ScreenScreenDeterminate> screenScreenDeterminateCollection;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Product productId;
}
