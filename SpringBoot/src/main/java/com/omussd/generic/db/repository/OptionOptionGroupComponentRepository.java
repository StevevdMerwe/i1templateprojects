/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface OptionOptionGroupComponentRepository extends JpaRepository<OptionOptionGroupComponent, Integer> {
    List<OptionOptionGroupComponent> findByOptionGroupComponentId(OptionGroupComponent groupComponent);
}
