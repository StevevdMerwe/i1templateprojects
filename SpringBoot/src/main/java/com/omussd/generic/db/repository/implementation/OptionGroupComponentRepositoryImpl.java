package com.omussd.generic.db.repository.implementation;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.repository.OptionGroupComponentRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author XY40428
 */
public class OptionGroupComponentRepositoryImpl implements OptionGroupComponentRepositoryCustom {

    private final Logger logger = LoggerFactory.getLogger(OptionGroupComponentRepositoryImpl.class);

    @Autowired
    private EntityManager em;

    @Override
    public List<OptionGroupComponent> findOptionGroupComponentsOfTypeOnScreen(String type, Screen screen) {
        return (List<OptionGroupComponent>) em.createNamedQuery(OptionGroupComponent.FindOptionGroupComponentsOfTypeOnScreen.NAME)
                .setParameter(OptionGroupComponent.FindOptionGroupComponentsOfTypeOnScreen.PARAM_SCREEN, screen)
                .setParameter(OptionGroupComponent.FindOptionGroupComponentsOfTypeOnScreen.PARAM_TYPE, type)
                .getResultList();
    }

    @Override
    public OptionGroupComponent findOptionGroupComponentContainingOptionWithNumberOnScreen(int number, Screen screen) {
        Query q = em.createNamedQuery(OptionGroupComponent.FindOptionGroupComponentContainingOptionWithNumberOnScreen.NAME)
                .setParameter(OptionGroupComponent.FindOptionGroupComponentContainingOptionWithNumberOnScreen.PARAM_NUMBER, number)
                .setParameter(OptionGroupComponent.FindOptionGroupComponentContainingOptionWithNumberOnScreen.PARAM_SCREEN, screen);

        try {
            return (OptionGroupComponent) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logDebug(logger, "No result found for findOptionGroupComponentContainingOptionWithNumberOnScreen with number: ", number);
        }

        return null;
    }

    @Override
    public List<OptionGroupComponent> findAllOptionGroupComponentsByNameWithPrefix(String prefix) {
        Query q = em.createNamedQuery(OptionGroupComponent.FindAllOptionGroupComponentsByNameWithPrefix.NAME)
                .setParameter(OptionGroupComponent.FindAllOptionGroupComponentsByNameWithPrefix.PARAM_PREFIX, prefix + "_%");
        return (List<OptionGroupComponent>) q.getResultList();
    }
}
