/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "OPTION_ITEM")
@NamedQueries({
    @NamedQuery(
            name = Option.DeleteOptionWhereNameStartsWith.NAME,
            query = Option.DeleteOptionWhereNameStartsWith.QUERY)
    ,
    @NamedQuery(
            name = Option.FindOptionBelongingToOptionGroupComponentWithNumber.NAME,
            query = Option.FindOptionBelongingToOptionGroupComponentWithNumber.QUERY)
    ,
    @NamedQuery(
            name = Option.FindOptionByNumberInOptionGroupComponents.NAME,
            query = Option.FindOptionByNumberInOptionGroupComponents.QUERY)
    ,
    @NamedQuery(
            name = Option.FindOptionOnScreenWithNumber.NAME,
            query = Option.FindOptionOnScreenWithNumber.QUERY)
})
public class Option implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "TEXT")
    private String text;
    @Basic(optional = true)
    @Column(name = "VALUE")
    private String value;
    @Column(name = "NAME")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "optionId")
    private Collection<OptionOptionGroupComponent> optionOptionGroupComponentCollection;
    @JoinColumn(name = "OPTION_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private OptionType optionTypeId;
    @ManyToMany(mappedBy = "optionCollection", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Collection<Intent> intentCollection = new ArrayList<>();

    public void addIntent(Intent intent) {
        intentCollection.add(intent);
        intent.getOptionCollection().add(this);
    }

    public void addIntents(List<Intent> intents) {
        if (intents == null || intents.isEmpty()) {
            return;
        }
        intentCollection.addAll(intents);
        for (Intent intent : intents) {
            intent.getOptionCollection().add(this);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Option)) {
            return false;
        }
        Option other = (Option) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.omussd.dal.db.entity.Option[ id=" + id + " ]";
    }

    public static class DeleteOptionWhereNameStartsWith {

        private DeleteOptionWhereNameStartsWith() {
        }
        public static final String NAME = "Option.deleteOptionWhereNameStartsWith";
        public static final String QUERY = "DELETE FROM Option o WHERE o.name LIKE :prefix";
        public static final String PARAM_PREFIX = "prefix";
    }

    public static class FindOptionByNumberInOptionGroupComponents {

        private FindOptionByNumberInOptionGroupComponents() {
        }
        public static final String NAME = "Option.findOptionByNumberInOptionGroupComponents";
        public static final String QUERY = "SELECT opt FROM Option opt "
                + "INNER JOIN opt.optionOptionGroupComponentCollection oogc "
                + "INNER JOIN oogc.optionGroupComponentId ogc "
                + "WHERE oogc.optionNumber = :number "
                + "AND ogc IN :ogcList";
        public static final String PARAM_NUMBER = "number";
        public static final String PARAM_GROUP_COMPONENT_LIST = "ogcList";
    }

    public static class FindOptionBelongingToOptionGroupComponentWithNumber {

        private FindOptionBelongingToOptionGroupComponentWithNumber() {
        }
        public static final String NAME = "Option.findOptionBelongingToOptionGroupComponentWithNumber";
        public static final String QUERY = "SELECT opt FROM Option opt "
                + "INNER JOIN opt.optionOptionGroupComponentCollection oogc "
                + "INNER JOIN oogc.optionGroupComponentId ogc "
                + "WHERE oogc.optionNumber = :number "
                + "AND ogc.id = :ogcId";
        public static final String PARAM_NUMBER = "number";
        public static final String PARAM_OPTION_GROUP_COMPONENT = "ogcId";
    }

    public static class FindOptionOnScreenWithNumber {

        private FindOptionOnScreenWithNumber() {
        }
        public static final String NAME = "Option.findOptionOnScreenWithNumber";
        public static final String QUERY = "SELECT opt FROM Option opt "
                + "INNER JOIN opt.optionOptionGroupComponentCollection oogc "
                + "INNER JOIN oogc.optionGroupComponentId ogc "
                + "INNER JOIN ogc.screenScreenComponentCollection sscc "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr = :scr "
                + "AND oogc.optionNumber = :number";
        public static final String PARAM_SCREEN = "scr";
        public static final String PARAM_NUMBER = "number";
    }

}
