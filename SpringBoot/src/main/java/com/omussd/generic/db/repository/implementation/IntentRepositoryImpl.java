package com.omussd.generic.db.repository.implementation;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.repository.IntentRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author XY40428
 */
public class IntentRepositoryImpl implements IntentRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(IntentRepositoryImpl.class);

    @Autowired
    private EntityManager em;

    @Override
    public Intent findIntentOfInputComponentOnScreenOfType(Screen screen, IntentTypeConstant intentType) {
        Query q = em.createNamedQuery(Intent.FindIntentOfInputComponentOnScreenOfType.NAME)
                .setParameter(Intent.FindIntentOfInputComponentOnScreenOfType.PARAM_SCREEN_ID, screen.getId())
                .setParameter(Intent.FindIntentOfInputComponentOnScreenOfType.PARAM_INTENT_TYPE, intentType.getIntentType());

        Intent intent = null;
        try {
            intent = (Intent) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logTrace(logger, "No result for findIntentOfInputComponentOnScreenOfType where intentType: ",
                    intentType);
        }
        return intent;
    }

    @Override
    public Intent findIntentOfOptionOnScreenOfType(Screen screen, int number, IntentTypeConstant intentType) {
        Query q = em.createNamedQuery(Intent.FindIntentOfOptionOnScreenOfType.NAME)
                .setParameter(Intent.FindIntentOfOptionOnScreenOfType.PARAM_SCREEN_ID, screen.getId())
                .setParameter(Intent.FindIntentOfOptionOnScreenOfType.PARAM_OPTION_NUMBER, number)
                .setParameter(Intent.FindIntentOfOptionOnScreenOfType.PARAM_INTENT_TYPE, intentType.getIntentType());

        Intent intent = null;
        try {
            intent = (Intent) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logTrace(logger, "No result for findIntentOfOptionOnScreenOfType where intentType: ",
                    intentType);
        }
        return intent;
    }

    @Override
    public List<Intent> findSubmitIntentsOfInputComponentOnScreen(Screen screen) {
        Query q = em.createNamedQuery(Intent.FindSubmitIntentsOfInputComponentOnScreen.NAME)
                .setParameter(Intent.FindSubmitIntentsOfInputComponentOnScreen.PARAM_SCREEN_ID, screen.getId())
                .setParameter(Intent.FindSubmitIntentsOfInputComponentOnScreen.PARAM_INTENT_TYPE,
                        IntentTypeConstant.SUBMIT.getIntentType());

        return q.getResultList();
    }

    @Override
    public List<Intent> findSubmitIntentsOfOptionOnScreen(Screen screen, int number) {
        Query q = em.createNamedQuery(Intent.FindSubmitIntentsOfOptionOnScreen.NAME)
                .setParameter(Intent.FindSubmitIntentsOfOptionOnScreen.PARAM_SCREEN_ID, screen.getId())
                .setParameter(Intent.FindSubmitIntentsOfOptionOnScreen.PARAM_OPTION_NUMBER, number)
                .setParameter(Intent.FindSubmitIntentsOfOptionOnScreen.PARAM_INTENT_TYPE, IntentTypeConstant.SUBMIT.getIntentType());

        return q.getResultList();
    }

    @Override
    public Intent findIntentByProductAndDestinationAndPurposeAndValueAndType(Product product,
            String destination, String purpose, String value, IntentTypeConstant intentType) {

        Query q = em.createQuery(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.getQuery(destination, purpose, value))
                .setParameter(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.PARAM_INTENT_TYPE_TYPE, intentType.getIntentType())
                .setParameter(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.PARAM_PRODUCT, product);

        if (value != null) {
            q.setParameter(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.PARAM_INTENT_VALUE, value);
        }

        if (destination != null) {
            q.setParameter(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.PARAM_DESTINATION_NAME, destination);
        }

        if (purpose != null) {
            q.setParameter(Intent.FindIntentByProductAndDestinationAndPurposeAndValueAndType.PARAM_PURPOSE_PURPOSE, purpose);
        }

        try {
            return (Intent) q.getSingleResult();
        } catch (NoResultException e) {
            LoggingUtil.logDebug(logger, "No result found for intent during findIntentByProductAndDestinationAndPurposeAndValueAndType");
            return null;
        }
    }

    @Override
    public Intent findNonSubmitIntentOfInputComponentOfType(InputComponent inputComponent, IntentTypeConstant itc) {
        Query q = em.createNamedQuery(Intent.FindNonSubmitIntentOfInputComponentOfType.NAME)
                .setParameter(Intent.FindNonSubmitIntentOfInputComponentOfType.PARAM_INTENT_TYPE, itc.getIntentType())
                .setParameter(Intent.FindNonSubmitIntentOfInputComponentOfType.PARAM_INPUT_COMPONENT, inputComponent);
        try {
            return (Intent) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logDebug(logger, "No result during findIntentOfInputComponentOfType for inputComponent ", inputComponent, " and type: ", itc);
        }
        return null;
    }
}
