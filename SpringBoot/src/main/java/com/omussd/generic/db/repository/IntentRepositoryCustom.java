/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.constant.IntentTypeConstant;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import java.util.List;

/**
 *
 * @author XY40428
 */
public interface IntentRepositoryCustom {
    Intent findIntentOfInputComponentOnScreenOfType(Screen screen, IntentTypeConstant intentType);
    Intent findIntentOfOptionOnScreenOfType(Screen screen, int number, IntentTypeConstant intentType);
    List<Intent> findSubmitIntentsOfInputComponentOnScreen(Screen screen);
    List<Intent> findSubmitIntentsOfOptionOnScreen(Screen screen, int number);
    Intent findIntentByProductAndDestinationAndPurposeAndValueAndType(Product product, String destination, String purpose, String value, IntentTypeConstant intentType);
    Intent findNonSubmitIntentOfInputComponentOfType(InputComponent inputComponent, IntentTypeConstant intentType);
}
