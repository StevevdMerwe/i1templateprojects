/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true, of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "TEXT_COMPONENT")
@DiscriminatorValue("TEXT")
public class TextComponent extends ScreenComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "TEXT")
    private String text;

}
