/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface ProductRepository extends JpaRepository<Product, Integer>, ProductRepositoryCustom {
    Product findByName(String productName);
}