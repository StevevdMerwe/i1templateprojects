/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import java.util.Collection;

/**
 *
 * @author XY40428
 */
public interface OptionRepositoryCustom {
    Option findOptionBelongingToOptionGroupComponentWithNumber(OptionGroupComponent component, int number);
    Option findOptionByNumberInOptionGroupComponents(int number, Collection<OptionGroupComponent> groupComponents);
    Option findOptionOnScreenWithNumber(Screen screen, int number);
    Integer deleteOptionWhereNameStartsWith(String prefix);
}
