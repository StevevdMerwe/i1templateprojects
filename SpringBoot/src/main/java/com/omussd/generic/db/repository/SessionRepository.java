/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public interface SessionRepository extends JpaRepository<Session, Integer>, SessionRepositoryCustom {

    @Transactional
    Integer deleteByMsisdn(String msisdn);

    Session findByMsisdnAndNetworkIdName(String msisdn, String networkName);

}
