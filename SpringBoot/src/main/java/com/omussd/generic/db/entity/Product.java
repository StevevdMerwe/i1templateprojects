/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "PRODUCT")
@NamedQueries({
    @NamedQuery(
            name = Product.FindAllWorkingProducts.NAME,
            query = Product.FindAllWorkingProducts.QUERY)
})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "PRODUCT_KEY")
    private String productKey;
    @OneToMany(mappedBy = "productId")
    private Collection<Screen> screenCollection;
    @OneToMany(mappedBy = "productId")
    private Collection<ScreenDeterminate> screenDeterminateCollection;
    @OneToMany(mappedBy = "productId")
    private Collection<Intent> intentCollection;
    @JoinColumn(name = "PRODUCT_PROPERTY_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ProductProperty productPropertyId;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Intent intentId;

    public static class FindAllWorkingProducts {

        private FindAllWorkingProducts() {
        }
        public static final String NAME = "Product.findAllWorkingProducts";
        public static final String QUERY = "SELECT o FROM Product o "
                + "INNER JOIN o.productPropertyId pp "
                + "WHERE pp.enabled = :enabled "
                + "AND (pp.startTime >= :today OR pp.startTime IS NULL) "
                + "AND (pp.endTime <= :today OR pp.endTime IS NULL) "
                + "AND pp.active = :active";
        public static final String PARAMETER_TODAY = "today";
        public static final String PARAMETER_ACTIVE = "active";
        public static final String PARAMETER_ENABLED = "enabled";
    }
}
