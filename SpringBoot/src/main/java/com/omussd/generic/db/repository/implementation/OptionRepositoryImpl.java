/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository.implementation;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.repository.OptionRepositoryCustom;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public class OptionRepositoryImpl implements OptionRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(OptionRepositoryImpl.class);

    @Autowired
    private EntityManager em;

    @Override
    public Option findOptionOnScreenWithNumber(Screen screen, int number) {
        Query q = em.createNamedQuery(Option.FindOptionOnScreenWithNumber.NAME)
                .setParameter(Option.FindOptionOnScreenWithNumber.PARAM_NUMBER, number)
                .setParameter(Option.FindOptionOnScreenWithNumber.PARAM_SCREEN, screen);

        try {
            return (Option) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logDebug(logger, "No result for findOptionOnScreenWithNumber with number: ", number);
        }
        return null;
    }

    @Override
    public Option findOptionBelongingToOptionGroupComponentWithNumber(OptionGroupComponent component, int number) {
        Query q = em.createNamedQuery(Option.FindOptionBelongingToOptionGroupComponentWithNumber.NAME)
                .setParameter(Option.FindOptionBelongingToOptionGroupComponentWithNumber.PARAM_OPTION_GROUP_COMPONENT, component.getId())
                .setParameter(Option.FindOptionBelongingToOptionGroupComponentWithNumber.PARAM_NUMBER, number);
        try {
            return (Option) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logDebug(logger, "No result found during findOptionBelongingToOptionGroupComponentWithNumber");
        }
        return null;
    }

    @Override
    public Option findOptionByNumberInOptionGroupComponents(int number, Collection<OptionGroupComponent> groupComponents) {
        Query q = em.createNamedQuery(Option.FindOptionByNumberInOptionGroupComponents.NAME)
                .setParameter(Option.FindOptionByNumberInOptionGroupComponents.PARAM_NUMBER, number)
                .setParameter(Option.FindOptionByNumberInOptionGroupComponents.PARAM_GROUP_COMPONENT_LIST, groupComponents);
        try {
            return (Option) q.getSingleResult();
        } catch (NoResultException e) {
            LoggingUtil.logDebug(logger, "No result found during findOptionByNumberInOptionGroupComponents");
        }
        return null;
    }

    @Override
    @Transactional
    public Integer deleteOptionWhereNameStartsWith(String prefix) {
        Query q = em.createNamedQuery(Option.DeleteOptionWhereNameStartsWith.NAME)
                .setParameter(Option.DeleteOptionWhereNameStartsWith.PARAM_PREFIX, prefix + "_%");
        return q.executeUpdate();
    }
}
