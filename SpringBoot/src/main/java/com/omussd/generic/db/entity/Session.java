/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SESSION")
@NamedQueries({
    @NamedQuery(
            name = Session.DeleteByProduct.NAME,
            query = Session.DeleteByProduct.QUERY)
    ,
    @NamedQuery(
            name = Session.FindByMsisdnNotOnNetwork.NAME,
            query = Session.FindByMsisdnNotOnNetwork.QUERY)
})
public class Session implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "MSISDN")
    private String msisdn;
    @Basic(optional = false)
    @Column(name = "LOCKED")
    private Boolean locked;
    @Basic(optional = false)
    @Column(name = "SESSION_DATA")
    private String sessionData;
    @Basic(optional = false)
    @Column(name = "START_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @JoinColumn(name = "NETWORK_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Network networkId;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "ID")
    @ManyToOne
    private Screen screenId;

    public static class DeleteByProduct {

        private DeleteByProduct() {
        }
        public static final String NAME = "Session.deleteByProduct";
        public static final String QUERY = "DELETE FROM Session o "
                + "WHERE o.screenId IN ("
                + "SELECT scr FROM Screen scr WHERE scr.productId = :product)";
        public static final String PARAM_PRODUCT = "product";
    }

    public static class FindByMsisdnNotOnNetwork {

        private FindByMsisdnNotOnNetwork() {
        }
        public static final String NAME = "Session.findByMsisdnNotOnNetwork";
        public static final String QUERY = "SELECT o FROM Session o "
                + "INNER JOIN o.networkId net "
                + "WHERE net.name <> :networkName "
                + "AND o.msisdn = :msisdn";
        public static final String PARAM_NETWORK_NAME = "networkName";
        public static final String PARAM_MSISDN = "msisdn";
    }
}
