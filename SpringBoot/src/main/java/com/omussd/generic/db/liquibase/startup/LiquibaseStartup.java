/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.liquibase.startup;

import com.omussd.common.log.LoggingUtil;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author XY40428
 */
@Component
public class LiquibaseStartup {

    @Value("${liquibase.contexts}")
    private String liquiContexts;

    private final Logger logger = LoggerFactory.getLogger(LiquibaseStartup.class);
    private static final String CHANGELOG_FILE = "liquibase/changelog/db.changelog-master.xml";

    @Autowired
    private DataSource dataSource;

    public void initialiseLiquibaseUpdates() throws SQLException, LiquibaseException {
        LoggingUtil.logTrace(logger, "initialiseLiquibaseUpdates entered");

        ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
        Connection connection = dataSource.getConnection();
        JdbcConnection jdbcConnection = new JdbcConnection(connection);
        Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        Liquibase liquibase = new Liquibase(CHANGELOG_FILE, resourceAccessor, db);
        liquibase.update(liquiContexts);
        LoggingUtil.logTrace(logger, "Liquibase successfully ran");
    }
}
