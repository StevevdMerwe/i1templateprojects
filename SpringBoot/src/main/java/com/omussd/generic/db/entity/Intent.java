/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "INTENT")
@NamedQueries({
    @NamedQuery(
            name = Intent.FindIntentOfInputComponentOnScreenOfType.NAME,
            query = Intent.FindIntentOfInputComponentOnScreenOfType.QUERY)
    ,
    @NamedQuery(
            name = Intent.FindIntentOfOptionOnScreenOfType.NAME,
            query = Intent.FindIntentOfOptionOnScreenOfType.QUERY)
    ,
    @NamedQuery(
            name = Intent.FindSubmitIntentsOfInputComponentOnScreen.NAME,
            query = Intent.FindSubmitIntentsOfInputComponentOnScreen.QUERY)
    ,
    @NamedQuery(
            name = Intent.FindSubmitIntentsOfOptionOnScreen.NAME,
            query = Intent.FindSubmitIntentsOfOptionOnScreen.QUERY)
    ,
    @NamedQuery(
            name = Intent.FindNonSubmitIntentOfInputComponentOfType.NAME,
            query = Intent.FindNonSubmitIntentOfInputComponentOfType.QUERY)
})
public class Intent implements Serializable {

    public static final String PARAMETER_SCREEN_ID = "scrId";
    public static final String PARAMETER_INTENT_TYPE = "intentType";
    public static final String PARAMETER_OPTION_NUMBER = "number";

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "INTENT_VALUE")
    private String intentValue;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<AuditLog> auditLogCollection;
    @OneToMany(mappedBy = "intentId")
    private Collection<Screen> screenCollection;
    @JoinColumn(name = "INTENT_DESTINATION_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private IntentDestination intentDestinationId;
    @JoinColumn(name = "INTENT_PURPOSE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private IntentPurpose intentPurposeId;
    @JoinColumn(name = "INTENT_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private IntentType intentTypeId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Product productId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<OptionGroupComponent> optionGroupComponentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<ScreenScreenDeterminate> screenScreenDeterminateCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<TextPlaceholder> textPlaceholderCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<Product> productCollection;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "INPUT_COMPONENT_INTENT",
            joinColumns = @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "INPUT_COMPONENT_ID", referencedColumnName = "ID"))
    private Collection<InputComponent> inputComponentCollection = new ArrayList<>();
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "OPTION_INTENT",
            joinColumns = @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "OPTION_ID", referencedColumnName = "ID"))
    private Collection<Option> optionCollection = new ArrayList<>();

    public static class FindIntentOfInputComponentOnScreenOfType {

        private FindIntentOfInputComponentOnScreenOfType() {
        }

        public static final String NAME = "Intent.findIntentOfInputComponentOnScreenOfType";
        public static final String QUERY = "SELECT intc FROM InputComponent ic "
                + "INNER JOIN ic.screenScreenComponentCollection sscc "
                + "INNER JOIN ic.intentCollection intc "
                + "INNER JOIN intc.intentTypeId type "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr.id = :scrId "
                + "AND type.type = :intentType";
        public static final String PARAM_SCREEN_ID = PARAMETER_SCREEN_ID;
        public static final String PARAM_INTENT_TYPE = PARAMETER_INTENT_TYPE;
        public static final String PARAM_OPTION_NUMBER = PARAMETER_OPTION_NUMBER;
    }

    public static class FindIntentOfOptionOnScreenOfType {

        private FindIntentOfOptionOnScreenOfType() {
        }

        public static final String NAME = "Intent.findIntentOfOptionOnScreenOfType";
        public static final String QUERY = "SELECT int FROM Option opt "
                + "INNER JOIN opt.intentCollection int "
                + "INNER JOIN int.intentTypeId type "
                + "INNER JOIN opt.optionOptionGroupComponentCollection oogc "
                + "INNER JOIN oogc.optionGroupComponentId ogc "
                + "INNER JOIN ogc.screenScreenComponentCollection sscc "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr.id = :scrId "
                + "AND oogc.optionNumber = :number "
                + "AND type.type = :intentType";
        public static final String PARAM_SCREEN_ID = PARAMETER_SCREEN_ID;
        public static final String PARAM_INTENT_TYPE = PARAMETER_INTENT_TYPE;
        public static final String PARAM_OPTION_NUMBER = PARAMETER_OPTION_NUMBER;
    }

    public static class FindSubmitIntentsOfInputComponentOnScreen {

        private FindSubmitIntentsOfInputComponentOnScreen() {
        }

        public static final String NAME = "Intent.findSubmitIntentsOfInputComponentOnScreen";
        public static final String QUERY = "SELECT intc FROM InputComponent ic "
                + "INNER JOIN ic.screenScreenComponentCollection sscc "
                + "INNER JOIN ic.intentCollection intc "
                + "INNER JOIN intc.intentTypeId type "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr.id = :scrId "
                + "AND type.type = :intentType";
        public static final String PARAM_SCREEN_ID = PARAMETER_SCREEN_ID;
        public static final String PARAM_INTENT_TYPE = PARAMETER_INTENT_TYPE;
    }

    public static class FindSubmitIntentsOfOptionOnScreen {

        private FindSubmitIntentsOfOptionOnScreen() {
        }

        public static final String NAME = "Intent.findSubmitIntentsOfOptionOnScreen";
        public static final String QUERY = "SELECT int FROM Option opt "
                + "INNER JOIN opt.intentCollection int "
                + "INNER JOIN int.intentTypeId type "
                + "INNER JOIN opt.optionOptionGroupComponentCollection oogc "
                + "INNER JOIN oogc.optionGroupComponentId ogc "
                + "INNER JOIN ogc.screenScreenComponentCollection sscc "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr.id = :scrId "
                + "AND oogc.optionNumber = :number "
                + "AND type.type = :intentType";
        public static final String PARAM_SCREEN_ID = PARAMETER_SCREEN_ID;
        public static final String PARAM_INTENT_TYPE = PARAMETER_INTENT_TYPE;
        public static final String PARAM_OPTION_NUMBER = PARAMETER_OPTION_NUMBER;
    }

    public static class FindIntentByProductAndDestinationAndPurposeAndValueAndType {

        private FindIntentByProductAndDestinationAndPurposeAndValueAndType() {
        }

        public static final String NAME = "Intent.findIntentByProductAndDestinationAndPurposeAndValueAndType";
        public static final String PARAM_DESTINATION_NAME = "destinationName";
        public static final String PARAM_PURPOSE_PURPOSE = "purposeName";
        public static final String PARAM_INTENT_TYPE_TYPE = "intentTypeType";
        public static final String PARAM_INTENT_VALUE = "intentValue";
        public static final String PARAM_PRODUCT = "product";

        public static String getQuery(String destination, String purpose, String value) {
            String query = "SELECT int FROM Intent int ";
            if (destination != null) {
                query += "INNER JOIN int.intentDestinationId dest ";
            }

            if (purpose != null) {
                query += "INNER JOIN int.intentPurposeId purp ";
            }

            query += "INNER JOIN int.intentTypeId type "
                    + "WHERE type.type = :intentTypeType ";

            if (destination != null) {
                query += "AND dest.name = :destinationName ";
            }

            if (purpose != null) {
                query += "AND purp.purpose = :purposeName ";

            }

            if (value != null) {
                query += "AND int.intentValue = :intentValue ";
            }

            query += "AND int.productId = :product";

            return query;
        }
    }

    public static class FindNonSubmitIntentOfInputComponentOfType {

        private FindNonSubmitIntentOfInputComponentOfType() {
        }

        public static final String NAME = "Intent.findNonSubmitIntentOfInputComponentOfType";
        public static final String PARAM_INTENT_TYPE = PARAMETER_INTENT_TYPE;
        public static final String PARAM_INPUT_COMPONENT = "inputComponent";
        public static final String QUERY = "SELECT int FROM InputComponent ic "
                + "INNER JOIN ic.intentCollection int "
                + "WHERE int.intentTypeId.type = :" + PARAM_INTENT_TYPE + " "
                + "AND ic = :inputComponent";
    }
}
