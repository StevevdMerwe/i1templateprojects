/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SCREEN_SCREEN_COMPONENT")
@NamedQueries({
    @NamedQuery(name = "ScreenScreenComponent.findAll", query = "SELECT s FROM ScreenScreenComponent s")})
public class ScreenScreenComponent implements Serializable, Comparable<ScreenScreenComponent> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "SEQUENCE")
    private int sequence;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Screen screenId;
    @JoinColumn(name = "SCREEN_COMPONENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ScreenComponent screenComponentId;

    @Override
    public int compareTo(ScreenScreenComponent o) {
        return this.sequence - o.sequence;
    }
}
