/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import java.util.List;

/**
 *
 * @author XY40428
 */
public interface OptionGroupComponentRepositoryCustom {
    List<OptionGroupComponent> findOptionGroupComponentsOfTypeOnScreen(String type, Screen screen);
    OptionGroupComponent findOptionGroupComponentContainingOptionWithNumberOnScreen(int number, Screen screen);
    List<OptionGroupComponent> findAllOptionGroupComponentsByNameWithPrefix(String prefix);
}
