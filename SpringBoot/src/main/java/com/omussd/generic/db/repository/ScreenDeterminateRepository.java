package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.ScreenDeterminate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface ScreenDeterminateRepository extends JpaRepository<ScreenDeterminate, Integer> {

    List<ScreenDeterminate> findByProductId(Product product);
    ScreenDeterminate findByProductIdAndValue(Product product, String value);
}
