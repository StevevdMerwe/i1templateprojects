/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.OptionGroupComponentType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface OptionGroupComponentTypeRepository extends JpaRepository<OptionGroupComponentType, Integer> {
    OptionGroupComponentType findByType(String type);
}
