package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true, of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "INPUT_COMPONENT")
@DiscriminatorValue("INPUT")
@NamedQueries({
    @NamedQuery(
            name = InputComponent.DeleteAllInputComponentsWhereNameStartsWith.NAME,
            query = InputComponent.DeleteAllInputComponentsWhereNameStartsWith.QUERY)
    ,
    @NamedQuery(
            name = InputComponent.FindInputComponentOnScreen.NAME,
            query = InputComponent.FindInputComponentOnScreen.QUERY)
})
public class InputComponent extends ScreenComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "MINIMUM_LENGTH")
    private int minimumLength;
    @Basic(optional = false)
    @Column(name = "MAXIMUM_LENGTH")
    private int maximumLength;
    @JoinColumn(name = "INPUT_COMPONENT_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private InputComponentType inputComponentTypeId;
    @ManyToMany(mappedBy = "inputComponentCollection", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Collection<Intent> intentCollection = new ArrayList<>();

    public void addIntent(Intent intent) {
        intentCollection.add(intent);
        intent.getInputComponentCollection().add(this);
    }

    public void addIntents(List<Intent> intents) {
        if (intents == null || intents.isEmpty()) {
            return;
        }
        intentCollection.addAll(intents);
        for (Intent intent : intents) {
            intent.getInputComponentCollection().add(this);
        }
    }

    public static class DeleteAllInputComponentsWhereNameStartsWith {

        private DeleteAllInputComponentsWhereNameStartsWith() {
        }

        public static final String NAME = "InputComponent.deleteAllInputComponentsWhereNameStartsWith";
        public static final String QUERY = "DELETE FROM InputComponent o "
                + "WHERE o.name LIKE :prefix";
        public static final String PARAM_PREFIX = "prefix";
    }

    public static class FindInputComponentOnScreen {

        private FindInputComponentOnScreen() {
        }
        
        public static final String NAME = "InputComponent.findInputComponentOnScreen";
        public static final String QUERY = "SELECT input FROM InputComponent input "
                + "INNER JOIN input.screenScreenComponentCollection sscc "
                + "INNER JOIN sscc.screenId scr "
                + "WHERE scr = :scr";
        public static final String PARAM_SCREEN = "scr";
    }
}
