/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.IntentType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface IntentTypeRepository extends JpaRepository<IntentType, Integer> {
    IntentType findByType(String type);
}
