/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Screen;

/**
 *
 * @author XY40428
 */
public interface InputComponentRepositoryCustom {
    InputComponent findInputComponentOnScreen(Screen screen);
    Integer deleteAllInputComponentsWhereNameStartsWith(String prefix);
}
