package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "AUDIT_LOG")
@NamedQueries({
    @NamedQuery(name = "AuditLog.findAll", query = "SELECT a FROM AuditLog a")})
public class AuditLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "MSISDN")
    private String msisdn;
    @Basic(optional = false)
    @Column(name = "LOG_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;
    @Basic(optional = false)
    @Column(name = "ADDITIONAL_DATA")
    private String additionalData;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Intent intentId;
    @JoinColumn(name = "AUDIT_LOG_EVENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private AuditLogEvent auditLogEventId;
    @JoinColumn(name = "NETWORK_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Network networkId;
}
