/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "OPTION_OPTION_GROUP_COMPONENT")
@NamedQueries({
    @NamedQuery(name = "OptionOptionGroupComponent.findAll", query = "SELECT o FROM OptionOptionGroupComponent o")})
public class OptionOptionGroupComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "OPTION_NUMBER")
    private int optionNumber;
    @Basic(optional = false)
    @Column(name = "SEQUENCE")
    private int sequence;
    @JoinColumn(name = "OPTION_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Option optionId;
    @JoinColumn(name = "OPTION_GROUP_COMPONENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private OptionGroupComponent optionGroupComponentId;
}
