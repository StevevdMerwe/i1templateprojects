/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository.implementation;

import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.repository.ScreenComponentRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public class ScreenComponentRepositoryImpl implements ScreenComponentRepositoryCustom {

    @Autowired
    private EntityManager em;
    
    @Override
    @Transactional
    public Integer deleteAllScreenComponentsWhereNameStartsWith(String prefix) {
        Query q = em.createNamedQuery(ScreenComponent.DeleteAllScreenComponentsWhereNameStartsWith.NAME)
                .setParameter(ScreenComponent.DeleteAllScreenComponentsWhereNameStartsWith.PARAM_PREFIX, prefix + "_%");
        return q.executeUpdate();
    }
}
