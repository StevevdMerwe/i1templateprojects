/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SCREEN")
@NamedQueries({
    @NamedQuery(
            name = Screen.DeleteByProduct.NAME,
            query = Screen.DeleteByProduct.QUERY)
})
public class Screen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne
    private Intent intentId;
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Product productId;
    @JoinColumn(name = "SCREEN_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ScreenType screenTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "screenId")
    private Collection<ScreenScreenComponent> screenScreenComponentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "screenId")
    private Collection<ScreenScreenDeterminate> screenScreenDeterminateCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intentId")
    private Collection<TextPlaceholder> textPlaceholderCollection;

    public static class DeleteByProduct {

        private DeleteByProduct() {
        }
        public static final String NAME = "Screen.deleteByProduct";
        public static final String QUERY = "DELETE FROM Screen scr "
                + "WHERE scr.productId = :product";
        public static final String PARAM_PRODUCT = "product";
    }
}
