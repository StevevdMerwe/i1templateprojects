/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Intent;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Screen;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author XY40428
 */
public interface ScreenRepository extends JpaRepository<Screen, Integer>, ScreenRepositoryCustom {
    Screen findByCodeAndProductId(String code, Product product);
    Screen findByCodeAndProductIdIsNull(String code);
    Screen findByScreenScreenDeterminateCollectionIntentIdAndScreenScreenDeterminateCollectionScreenDeterminateIdProductIdAndScreenScreenDeterminateCollectionScreenDeterminateIdValue(
            Intent intent, Product product, String determinateValue);
    List<Screen> findByProductId(Product product);
}
