/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

/**
 *
 * @author XY40428
 */
public interface ScreenComponentRepositoryCustom {
    Integer deleteAllScreenComponentsWhereNameStartsWith(String prefix);
}
