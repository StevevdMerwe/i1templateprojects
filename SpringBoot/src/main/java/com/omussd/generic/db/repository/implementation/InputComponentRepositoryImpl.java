/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository.implementation;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.entity.InputComponent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.repository.InputComponentRepositoryCustom;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public class InputComponentRepositoryImpl implements InputComponentRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(InputComponentRepositoryImpl.class);

    @Autowired
    private EntityManager em;

    @Override
    public InputComponent findInputComponentOnScreen(Screen screen) {
        Query q = em.createNamedQuery(InputComponent.FindInputComponentOnScreen.NAME)
                .setParameter(InputComponent.FindInputComponentOnScreen.PARAM_SCREEN, screen);

        try {
            return (InputComponent) q.getSingleResult();
        } catch (NoResultException nre) {
            LoggingUtil.logDebug(logger, "findInputComponentOnScreen: No result found trying to get input component on screen ", screen);
            return null;
        }
    }

    @Override
    @Transactional
    public Integer deleteAllInputComponentsWhereNameStartsWith(String prefix) {
        Query q = em.createNamedQuery(InputComponent.DeleteAllInputComponentsWhereNameStartsWith.NAME)
                .setParameter(InputComponent.DeleteAllInputComponentsWhereNameStartsWith.PARAM_PREFIX, prefix + "_%");
        return q.executeUpdate();
    }

}
