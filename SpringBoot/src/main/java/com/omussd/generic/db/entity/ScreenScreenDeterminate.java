package com.omussd.generic.db.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SCREEN_SCREEN_DETERMINATE")
@NamedQueries({
    @NamedQuery(name = "ScreenScreenDeterminate.findAll", query = "SELECT s FROM ScreenScreenDeterminate s")})
public class ScreenScreenDeterminate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "SCREEN_DETERMINATE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ScreenDeterminate screenDeterminateId;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Screen screenId;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Intent intentId;
}
