package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true, of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "OPTION_GROUP_COMPONENT")
@DiscriminatorValue("OPTION_GROUP")
@NamedQueries({
    @NamedQuery(
            name = OptionGroupComponent.FindAllOptionGroupComponentsByNameWithPrefix.NAME,
            query = OptionGroupComponent.FindAllOptionGroupComponentsByNameWithPrefix.QUERY)
    ,
    @NamedQuery(
            name = OptionGroupComponent.FindOptionGroupComponentContainingOptionWithNumberOnScreen.NAME,
            query = OptionGroupComponent.FindOptionGroupComponentContainingOptionWithNumberOnScreen.QUERY)
    ,
    @NamedQuery(
            name = OptionGroupComponent.FindOptionGroupComponentsOfTypeOnScreen.NAME,
            query = OptionGroupComponent.FindOptionGroupComponentsOfTypeOnScreen.QUERY)
})
public class OptionGroupComponent extends ScreenComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "optionGroupComponentId")
    private Collection<OptionOptionGroupComponent> optionOptionGroupComponentCollection;
    @JoinColumn(name = "OPTION_GROUP_COMPONENT_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private OptionGroupComponentType optionGroupComponentTypeId;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Intent intentId;

    public static class FindAllOptionGroupComponentsByNameWithPrefix {

        private FindAllOptionGroupComponentsByNameWithPrefix() {
        }
        public static final String NAME = "OptionGroupComponent.findAllOptionGroupComponentsByNameWithPrefix";
        public static final String QUERY = "SELECT o FROM OptionGroupComponent o "
                + "WHERE o.name LIKE :prefix";
        public static final String PARAM_PREFIX = "prefix";
    }

    public static class FindOptionGroupComponentContainingOptionWithNumberOnScreen {

        private FindOptionGroupComponentContainingOptionWithNumberOnScreen() {
        }
        public static final String NAME = "OptionGroupComponent.findOptionGroupComponentContainingOptionWithNumberOnScreen";
        public static final String QUERY = "SELECT ogc FROM OptionGroupComponent ogc "
                + "INNER JOIN ogc.optionOptionGroupComponentCollection oogcc "
                + "INNER JOIN ogc.screenScreenComponentCollection sscc "
                + "WHERE sscc.screenId = :scrId "
                + "AND oogcc.optionNumber = :number ";
        public static final String PARAM_SCREEN = "scrId";
        public static final String PARAM_NUMBER = "number";
    }

    public static class FindOptionGroupComponentsOfTypeOnScreen {

        private FindOptionGroupComponentsOfTypeOnScreen() {
        }
        public static final String NAME = "OptionGroupComponent.findOptionGroupComponentsOfTypeOnScreen";
        public static final String QUERY = "SELECT ogc FROM OptionGroupComponent ogc "
                + "INNER JOIN ogc.optionGroupComponentTypeId type "
                + "INNER JOIN ogc.screenScreenComponentCollection sscc "
                + "WHERE sscc.screenId = :scrId "
                + "AND type.type = :type";
        public static final String PARAM_SCREEN = "scrId";
        public static final String PARAM_TYPE = "type";
    }
}
