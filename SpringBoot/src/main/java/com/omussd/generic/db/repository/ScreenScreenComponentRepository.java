package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenScreenComponent;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public interface ScreenScreenComponentRepository extends JpaRepository<ScreenScreenComponent, Integer> {

    List<ScreenScreenComponent> findByScreenIdOrderBySequence(Screen screen);
    @Transactional
    Integer deleteByScreenId(Screen screen);
}
