package com.omussd.generic.db.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "TEXT_PLACEHOLDER")
public class TextPlaceholder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "VARIABLE")
    private String variable;
    @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Intent intentId;
    @JoinColumn(name = "SCREEN_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Screen screenId;
}
