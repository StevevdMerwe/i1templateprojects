/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(of = "id")
@ToString(of = {"id"})
@Entity
@Table(name = "SCREEN_COMPONENT")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "screenComponentType")
@NamedQueries({
    @NamedQuery(
            name = ScreenComponent.DeleteAllScreenComponentsWhereNameStartsWith.NAME,
            query = ScreenComponent.DeleteAllScreenComponentsWhereNameStartsWith.QUERY)
})
public class ScreenComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = true)
    @Column(name = "PARAMETER_NAME")
    private String parameterName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "screenComponentId")
    private Collection<ScreenScreenComponent> screenScreenComponentCollection;

    public static class DeleteAllScreenComponentsWhereNameStartsWith {

        private DeleteAllScreenComponentsWhereNameStartsWith() {
        }
        public static final String NAME = "ScreenComponent.deleteAllScreenComponentsWhereNameStartsWith";
        public static final String QUERY = "DELETE FROM ScreenComponent o "
                + "WHERE o.name LIKE :prefix";
        public static final String PARAM_PREFIX = "prefix";
    }
}
