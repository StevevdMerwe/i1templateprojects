/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository;

import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.TextPlaceholder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public interface TextPlaceholderRepository extends JpaRepository<TextPlaceholder, Integer> {
    List<TextPlaceholder> findByScreenId(Screen screen);
    @Transactional
    Integer deleteByScreenId(Screen screen);
}
