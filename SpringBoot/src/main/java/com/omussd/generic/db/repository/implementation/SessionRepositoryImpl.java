/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository.implementation;

import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.repository.SessionRepositoryCustom;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author XY40428
 */
public class SessionRepositoryImpl implements SessionRepositoryCustom {

    @Autowired
    private EntityManager em;

    @Override
    public List<Session> findByMsisdnNotOnNetwork(String msisdn, String networkName) {
        return (List<Session>) em.createNamedQuery(Session.FindByMsisdnNotOnNetwork.NAME)
                .setParameter(Session.FindByMsisdnNotOnNetwork.PARAM_NETWORK_NAME, networkName)
                .setParameter(Session.FindByMsisdnNotOnNetwork.PARAM_MSISDN, msisdn)
                .getResultList();
    }

    @Override
    @Transactional
    public Integer deleteByProduct(Product product) {
        Query q = em.createNamedQuery(Session.DeleteByProduct.NAME)
                .setParameter(Session.DeleteByProduct.PARAM_PRODUCT, product);
        return q.executeUpdate();
    }
}
