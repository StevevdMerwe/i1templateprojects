/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.db.repository.implementation;

import com.omussd.generic.db.entity.Product;
import com.omussd.generic.db.repository.ProductRepositoryCustom;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author XY40428
 */
public class ProductRepositoryImpl implements ProductRepositoryCustom {

    @Autowired
    private EntityManager em;

    @Override
    public List<Product> getAllWorkingProducts() {
        Date today = GregorianCalendar.getInstance().getTime();
        return (List<Product>) em.createNamedQuery(Product.FindAllWorkingProducts.NAME)
                .setParameter(Product.FindAllWorkingProducts.PARAMETER_ACTIVE, Boolean.TRUE)
                .setParameter(Product.FindAllWorkingProducts.PARAMETER_ENABLED, Boolean.TRUE)
                .setParameter(Product.FindAllWorkingProducts.PARAMETER_TODAY, today, TemporalType.DATE)
                .getResultList();
    }

}
