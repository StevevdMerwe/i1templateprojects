package com.omussd.generic.audit;

import com.omussd.generic.constant.AuditDataEntryKey;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XY40428
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AuditDataEntry {

    @XmlElement
    private String key;
    @XmlElement
    private String value;

    public AuditDataEntry() {
        //constructor required for JAXB implementation
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AuditDataEntry{" + "key=" + key + ", value=" + value + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.key);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuditDataEntry other = (AuditDataEntry) obj;
        return Objects.equals(this.key, other.key);
    }

    public static AuditDataEntry getEntry(AuditDataEntryKey key, String value) {
        AuditDataEntry ade = new AuditDataEntry();
        ade.setKey(key.toString());
        ade.setValue(value);
        return ade;
    }
}
