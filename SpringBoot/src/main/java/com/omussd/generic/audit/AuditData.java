package com.omussd.generic.audit;

import com.omussd.generic.constant.AuditDataEntryKey;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XY40428
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AuditData {
    @XmlElement
    private List<AuditDataEntry> auditDataEntries;

    public AuditData() {
        auditDataEntries = new ArrayList<>();
    }

    public void addAuditDataEntry(AuditDataEntryKey auditDataEntryKey, String value) {
        AuditDataEntry entry = AuditDataEntry.getEntry(auditDataEntryKey, value);
        addAuditDataEntry(entry);
    }

    public void addAuditDataEntry(AuditDataEntry entry) {
        auditDataEntries.add(entry);
    }

    public void addAuditDataEntries(AuditDataEntry... entries) {
        for (AuditDataEntry entry : entries) {
            addAuditDataEntry(entry);
        }
    }

    @Override
    public String toString() {
        return "AuditData{" + "auditDataEntries=" + auditDataEntries + '}';
    }
}
