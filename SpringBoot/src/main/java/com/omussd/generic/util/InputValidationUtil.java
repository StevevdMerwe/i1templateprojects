/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.util;

import com.omussd.common.log.LoggingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author XY40428
 */
public class InputValidationUtil {

    private InputValidationUtil() {
        
    }
    
    private static final Logger logger = LoggerFactory.getLogger(InputValidationUtil.class);

    public static boolean validateAlphanumeric(String input) {
        return input != null && !input.isEmpty();
    }

    public static boolean validateNumber(String number) {
        String re = "[0-9]+";
        if (!number.matches(re)) {
            return false;
        } else {
            try {
                Long.valueOf(number);
            } catch (NumberFormatException e) {
                LoggingUtil.logTrace(logger, "Long couldn't be cast: ", number);
                return false;
            }
        }
        return true;
    }

    public static boolean validateRSAIdNumber(String idNumber) {
        if (!idNumber.matches("[0-9]{13}")) {
            return false;
        }
        
        char zero = '0';
        char[] idCharAr = idNumber.toCharArray();
        int allOdds = 0;
        for (int i = 0; i < idCharAr.length - 1; i += 2) {
            allOdds += (idCharAr[i] - zero);
        }

        StringBuilder evensTemp = new StringBuilder();
        for (int i = 1; i < idCharAr.length; i += 2) {
            evensTemp.append(idCharAr[i]);
        }
        String allEvens = String.valueOf(Integer.valueOf(evensTemp.toString()) * 2);

        int evensAdded = 0;
        for (int i = 0; i < allEvens.length(); i++) {
            evensAdded += (allEvens.charAt(i) - zero);
        }

        String oddsAddEvens = String.valueOf(allOdds + evensAdded);
        char lastDigit = oddsAddEvens.charAt(oddsAddEvens.length() - 1);
        int validationAnswer = 10 - (lastDigit - zero);

        return validationAnswer == (idCharAr[idCharAr.length - 1] - zero);
    }
    
    public static boolean validateCellNumber(String cellNumber) {
        String re = "[0-9]+";
        return cellNumber != null && cellNumber.matches(re);
    }
}
