package com.omussd.generic.util;

import com.omussd.common.log.LoggingUtil;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import org.bouncycastle.crypto.generators.OpenBSDBCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author XY40428
 */
public class SecurityUtil {

    private SecurityUtil() {

    }

    public static final Logger logger = LoggerFactory.getLogger(SecurityUtil.class);

    public static boolean checkBcrypt(String plain, String bcryptString) {
        return OpenBSDBCrypt.checkPassword(bcryptString, plain.toCharArray());
    }

    public static String getBcryptRandomSalted(String plain, int cost) {
        byte[] salt = getRandomSalt(16);
        return OpenBSDBCrypt.generate(plain.toCharArray(), salt, cost);
    }

    private static byte[] getRandomSalt(int byteCount) {
        byte[] salt;
        try {
            SecureRandom secureRandom = SecureRandom.getInstanceStrong();
            salt = secureRandom.generateSeed(byteCount);
        } catch (NoSuchAlgorithmException e) {
            LoggingUtil.logError(logger, e, "Could not find algorithm for SecureRandom");
            salt = getRandomSaltWeak(byteCount);
        }
        return salt;
    }

    private static byte[] getRandomSaltWeak(int byteCount) {
        byte[] salt = new byte[byteCount];
        for (int i = 0; i < byteCount; i++) {
            byte b = (byte) (Math.random() * 256);
            salt[i] = b;
        }
        return salt;
    }

    public static String[] getUsernamePasswordCombinationFromHttpBasicAuthHeader(String authHeader) {
        String base64 = authHeader.substring(authHeader.indexOf("Basic ") + "Basic ".length());
        byte[] authBytes = Base64.getDecoder().decode(base64);
        String auth;
        try {
            auth = new String(authBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            auth = new String(authBytes);
        }

        return auth.split(":");
    }
    
    public static void main(String[] args) {
        System.out.println(SecurityUtil.getBcryptRandomSalted(",;LChPa6*ydT>7.+", 10));
    }
}
