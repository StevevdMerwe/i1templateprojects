/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import javax.xml.bind.annotation.XmlElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Text extends ScreenComponent {

    @XmlElement(name = "text")
    private String textString;
}
