package com.omussd.generic.product.structure;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement(name = "product-structure")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductStructure implements Serializable {
    @XmlElement
    private Screens screens;
    @XmlElement(name = "root-determinates")
    private RootDeterminates rootDeterminates;
}
