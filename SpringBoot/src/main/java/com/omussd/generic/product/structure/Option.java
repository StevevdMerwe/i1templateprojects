/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Option implements Serializable {

    @XmlAttribute
    private String name;
    @XmlAttribute
    private int num;
    @XmlAttribute
    private int seq;
    @XmlAttribute
    private String type;
    @XmlAttribute
    private String value;
    @XmlElement
    private String text;
    @XmlElementWrapper
    @XmlElement(name = "intent")
    private List<Intent> intents;

    public Option() {
        intents = new ArrayList<>();
    }
}
