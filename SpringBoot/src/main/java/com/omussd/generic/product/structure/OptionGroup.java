/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OptionGroup extends ScreenComponent {

    @XmlAttribute
    private String type;
    @XmlElement(name = "option")
    private List<Option> options;
    @XmlAttribute(name = "param")
    private String parameterName;
    @XmlAttribute
    private String destination;
    @XmlAttribute
    private String purpose;
    @XmlAttribute(name = "intent-value")
    private String intentValue;

    public OptionGroup() {
        options = new ArrayList<>();
    }
}
