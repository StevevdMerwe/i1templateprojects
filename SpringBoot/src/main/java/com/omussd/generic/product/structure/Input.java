/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author XY40428
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Input extends ScreenComponent {

    @XmlElementWrapper
    @XmlElement(name = "intent")
    private List<Intent> intents;
    @XmlAttribute(name = "parameter-name")
    private String parameterName;
    @XmlAttribute
    private String type;
    @XmlAttribute(name = "min-length")
    private int minLength;
    @XmlAttribute(name = "max-length")
    private int maxLength;

    public Input() {
        intents = new ArrayList<>();
    }
}
