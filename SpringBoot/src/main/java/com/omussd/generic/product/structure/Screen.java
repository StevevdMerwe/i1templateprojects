/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.product.structure;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Screen implements Serializable {

    @XmlAttribute
    private String code;
    @XmlAttribute
    private String type;
    @XmlAttribute(name = "failure-screen")
    private String failureScreenCode;
    @XmlElement
    private Components components;
    @XmlElementWrapper(name = "text-placeholders")
    @XmlElement(name = "text-placeholder")
    private List<TextPlaceholder> textPlaceholders;
}
