package com.omussd.generic.product.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RootDeterminates implements Serializable {

    @XmlAttribute
    private String destination;
    @XmlAttribute
    private String purpose;
    @XmlAttribute(name = "intent-value")
    private String intentValue;
    @XmlElement(name = "determinate")
    private List<Determinate> determinates;

    public RootDeterminates() {
        determinates = new ArrayList<>();
    }
}
