package com.omussd.generic.product;

import com.omussd.common.log.LoggingUtil;
import com.omussd.common.util.XMLUtil;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.service.ProductService;
import com.omussd.generic.session.bean.DynamicIntent;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.product.constant.IntentTypeConstant;
import com.omussd.product.intent.IntentResponse;
import com.omussd.product.intent.IntentResponseType;
import com.omussd.product.interfaces.IProduct;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The GenericProduct is a Product that represents the System in such cases
 * where no Product implementation is used.
 *
 * @author XY40428
 */
@Component
public class GenericProduct implements IProduct {

    private static final Logger logger = LoggerFactory.getLogger(GenericProduct.class);

    @Autowired
    private ProductService productService;

    /**
     * Processes a given Intent in a way that is appropriate for the System in
     * the case the System needs to stand in where a product would normally be
     * required
     *
     * @param purpose The purpose of the Intent
     * @param destination The destination of the Intent
     * @param parameterMap The parameter map of the Session
     * @param intentValue The value of the Intent
     * @param intentType The type of the Intent
     * @return An appropriate IntentResponse after processing the Intent
     */
    @Override
    public IntentResponse processIntent(String purpose,
            String destination, Map<String, String> parameterMap, String intentValue, IntentTypeConstant intentType) {
        LoggingUtil.logTrace(logger, "processIntent entered. Purpose: ", purpose);
        IntentResponse response = new IntentResponse();
        switch (intentType) {
            case OPTIONS:
                LoggingUtil.logDebug(logger, "processIntent with intentType Options");
                String xml = getOptions(purpose);
                if (xml == null) {
                    response.setIntentResponseType(IntentResponseType.ERROR);
                } else {
                    response.setType(IntentTypeConstant.SUCCESS);
                    response.setValue(xml);
                    response.setMessage("Success");
                    response.setIntentResponseType(IntentResponseType.SUCCESS);
                }
                break;
            case TEXT_PARAMETER_VALUE:
            case DETERMINATE:
            case SUBMIT:
            case SUCCESS:
            case FAILURE:
            default:
                response.setIntentResponseType(IntentResponseType.ERROR);
                break;
        }
        LoggingUtil.logTrace(logger, "processIntent exiting. Purpose: ", purpose, " response: ", response);
        return response;
    }

    public String getOptions(String purpose) {
        LoggingUtil.logTrace(logger, "getOptions entered. Purpose: ", purpose);
        if ("UREC_OPTIONS".equals(purpose)) {
            LoggingUtil.logDebug(logger, "getOptions found supported purpose: ", purpose);
            DynamicOptionGroup dog = new DynamicOptionGroup();
            List<DynamicOption> dynamicOptions = dog.getDynamicOptions();
            List<Product> products = productService.getAllWorkingProducts();
            LoggingUtil.logDebug(logger, "getOptions products for purpose ", purpose, ": ", products);
            for (int i = 0; i < products.size(); i++) {
                DynamicOption option = new DynamicOption();
                option.setNumber(i + 1);
                option.setSequence(i + 1);
                option.setText(products.get(i).getName());
                option.setValue(products.get(i).getName());

                DynamicIntent intent = new DynamicIntent();
                intent.setIntentType(IntentTypeConstant.SUCCESS.getIntentType());
                intent.setValue(SpecialScreenCodesConstant.UNRECOGNISED_ROOT.getCode());

                option.getDynamicIntents().add(intent);
                dynamicOptions.add(option);
            }
            LoggingUtil.logDebug(logger, "getOptions DynamicOptionGroup is formed: ", dog);
            try {
                return XMLUtil.toXML(false, DynamicOptionGroup.class, dog);
            } catch (JAXBException e) {
                LoggingUtil.logError(logger, e, "Failed to parse xml when getting generic options for: GEN_UREC_DYNAMIC_OPTIONS");
                return null;
            }
        }
        LoggingUtil.logTrace(logger, "getOptions exiting. Purpose: ", purpose, ". No Options");
        return null;
    }

    @Override
    public boolean isRecognised(String msisdn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
