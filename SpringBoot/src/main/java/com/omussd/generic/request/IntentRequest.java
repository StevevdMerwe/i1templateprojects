/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.request;

import com.omussd.common.xml.MapAdapter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IntentRequest implements Serializable {

    @XmlElement
    private String msisdn;
    @XmlElement
    private String purpose;
    @XmlElement
    private String destination;
    @XmlElement
    private String intentValue;
    @XmlJavaTypeAdapter(MapAdapter.class)
    private Map<String, String> parameterMap;

    public IntentRequest() {
        parameterMap = new HashMap<>();
    }
}
