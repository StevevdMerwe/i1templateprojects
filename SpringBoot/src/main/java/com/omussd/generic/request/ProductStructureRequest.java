/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.request;

import com.omussd.generic.product.structure.ProductStructure;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author XY40428
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductStructureRequest implements Serializable {

    @XmlElement(name = "product-structure")
    private ProductStructure productStructure;
}
