package com.omussd.generic.controller;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.audit.AuditDataEntry;
import com.omussd.generic.constant.AuditDataEntryKey;
import com.omussd.generic.constant.AuditLogEvent;
import com.omussd.generic.constant.NetworkConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.constant.SpecialScreenCodesConstant;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.exception.SessionAlreadyInUseException;
import com.omussd.generic.parser.ScreenParser;
import com.omussd.generic.service.AuditService;
import com.omussd.generic.service.ScreenService;
import com.omussd.generic.service.SessionService;
import com.omussd.generic.service.USSDService;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.ussdintegration.request.USSDAppRequest;
import com.omussd.ussdintegration.request.constant.ResponseState;
import com.omussd.ussdintegration.request.constant.ResponseType;
import com.omussd.ussdintegration.response.USSDAppResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The RESTful Services class for use in order to process USSD requests along
 * with using sessions and potentially Products.
 *
 * @author XY40428
 */
@RestController
@Api(value = "ussd")
public class USSDController {

    private static final Logger logger = LoggerFactory.getLogger(USSDController.class);

    @Autowired
    private USSDService ussdService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private ScreenParser parser;
    @Autowired
    private ScreenService screenService;
    @Autowired
    private AuditService auditService;

    /**
     * Retrieves an appropriate USSD Application response for a given USSD
     * Application request and product. Product may be the Root product name.
     *
     * @param ussdAppRequest The USSD Application request to consider for the
     * appropriate USSD Application response
     * @param productName The name of the product
     * @return The USSD Application Response appropriate for the given
     * parameters and session considerations.
     */
    @ApiOperation(value = "Retrieves an appropriate USSD Application response for a given USSD Application request and product. Product may be the Root product name.",
            response = USSDAppResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully processed the request and retrieved an appropriate USSD Application response")
    })
    @RequestMapping(path = "ussd/{productName}", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public USSDAppResponse getUSSdAppResponse(@RequestBody USSDAppRequest ussdAppRequest,
            @PathVariable(value = "productName") String productName) {
        LoggingUtil.logTrace(logger, "getUSSDAppResponse entered. MSISDN: ",
                ussdAppRequest.getMsisdn());

        NetworkConstant network = getNetworkFromRequest(ussdAppRequest);
        Session session = null;
        USSDAppResponse response;
        Screen screen = null;

        auditService.logAudit(AuditLogEvent.INCOMING_REQUEST, null, ussdAppRequest.getMsisdn(), network, null);
        try {
            session = getLockedSession(ussdAppRequest.getMsisdn(), network);
            SessionData sessionData = sessionService.getSessionData(session);

            LoggingUtil.logDebug(logger, "session data added to cache for session: ", session);
            SessionDataCache.INSTANCE.put(session.getMsisdn(), sessionData);

            screen = ussdService.processUSSDRequest(session, productName, ussdAppRequest);
            response = getUSSDAppResponseFromScreen(ussdAppRequest, network, session, screen);
        } catch (SessionAlreadyInUseException e) {
            LoggingUtil.logError(logger, e, "Session with msisdn: ", ussdAppRequest.getMsisdn(), " is already in use");
            screen = screenService.getScreenByCodeAndProductName(ussdAppRequest.getMsisdn(),
                    SpecialScreenCodesConstant.SESSION_IN_USE.getCode(), productName);
            response = getUSSDAppResponseFromScreen(ussdAppRequest, network, null, screen);
        } catch (Exception e) {
            LoggingUtil.logError(logger, e, "System failure: Unknown Exception");
            screen = screenService.getFailureScreen(null); //no guarantee of product
            response = getUSSDAppResponseFromScreen(ussdAppRequest, network, null, screen);
        } finally {
            if (session != null) {
                LoggingUtil.logDebug(logger, "session unlocked: ", session);
                sessionService.unlockSession(session);
                SessionDataCache.INSTANCE.removeSessionData(ussdAppRequest.getMsisdn());
                LoggingUtil.logDebug(logger, "session data removed from cache for session (msisdn): ", ussdAppRequest.getMsisdn());
                if (screen != null && ScreenTypeConstant.END.getCode().equals(screen.getScreenTypeId().getCode())) {
                    sessionService.deleteSessionByMsisdn(ussdAppRequest.getMsisdn());
                    LoggingUtil.logDebug(logger, "session data removed from database for session (msisdn): ", ussdAppRequest.getMsisdn());
                }
            }
        }

        auditService.logAudit(AuditLogEvent.OUTGOING_RESPONSE, null, ussdAppRequest.getMsisdn(), network,
                new AuditDataEntry[]{AuditDataEntry.getEntry(AuditDataEntryKey.PROMPT, response.getPrompt())});
        LoggingUtil.logTrace(logger, "getUSSDAppResponse exiting. MSISDN: ", ussdAppRequest.getMsisdn());
        return response;
    }

    private USSDAppResponse getUSSDAppResponseFromScreen(USSDAppRequest ussdAppRequest,
            NetworkConstant network, Session session, Screen screen) {
        if (screen != null) {
            return parser.getAppResponseFromScreen(session, screen);
        } else {
            auditService.logAudit(AuditLogEvent.PREPARE_ABORT_RESPONSE, null,
                    ussdAppRequest.getMsisdn(), network, new AuditDataEntry[0]);
            return getAbortResponse();
        }
    }

    private Session getLockedSession(String msisdn, NetworkConstant network) throws SessionAlreadyInUseException {
        Session session = sessionService.getSessionForMsisdnAndNetwork(msisdn,
                network);
        LoggingUtil.logDebug(logger, "Locking session: ", session);
        sessionService.lockSession(session);
        return session;
    }

    private NetworkConstant getNetworkFromRequest(USSDAppRequest request) {
        switch (request.getNetwork()) {
            case CELLC:
                return NetworkConstant.CELL_C;
            case MTN:
                return NetworkConstant.MTN;
            case Telkom:
                return NetworkConstant.TELKOM;
            case VOD:
                return NetworkConstant.VODACOM;
            default:
                return NetworkConstant.OTHER;
        }
    }

    private USSDAppResponse getAbortResponse() {
        USSDAppResponse response = new USSDAppResponse();
        response.setState(ResponseState.End);
        response.setPrompt("END");
        response.setResponseType(ResponseType.String);
        return response;
    }
}
