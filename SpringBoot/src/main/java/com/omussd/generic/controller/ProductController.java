package com.omussd.generic.controller;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.db.entity.Product;
import com.omussd.generic.request.ProductStructureRequest;
import com.omussd.generic.response.ProductStructureResponse;
import com.omussd.generic.service.ProductService;
import com.omussd.generic.service.ProductStructureService;
import com.omussd.generic.util.SecurityUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * The RESTful Services class for use in order to process Product
 * configurations.
 *
 * @author XY40428
 */
@RestController
@Api(value = "product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductStructureService productStructureService;

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    /**
     * Called to change the product structure, or add the product structure to
     * the database for an already defined product.
     *
     * @param productStructureRequest The request to analyze in order to alter
     * or add the product structure. Must contain the Product structure.
     * @return Response that describes whether the process was a success or not.
     */
    @ApiOperation(value = "Changes or adds the product structure for an already defined product",
            response = ProductStructureResponse.class, authorizations = {
                @Authorization(value = "basicAuth")})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully processed the request. Success of failure of the process may be viwed within the response.")
        ,
        @ApiResponse(code = 401, message = "Authorization failed or product is not defined", response = Void.class),})
    @RequestMapping(path = "product/structure",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ProductStructureResponse productStuctureChange(HttpServletRequest request,
            HttpServletResponse response,
            @RequestBody ProductStructureRequest productStructureRequest) {
        LoggingUtil.logInfo(logger, "Product structure call was made");
        String authHeader = request.getHeader("Authorization");
        String[] productPass = SecurityUtil.getUsernamePasswordCombinationFromHttpBasicAuthHeader(authHeader);

        String productName = productPass[0];
        String password = productPass[1];

        Product product = productService.getProductByNameFromDb(productName);
        if (product == null) {
            LoggingUtil.logWarn(logger, "Unknown product: ", productName, " attempted structure change");
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        } else {
            String productKey = product.getProductKey();
            if (!SecurityUtil.checkBcrypt(password, productKey)) {
                LoggingUtil.logWarn(logger, "Unauthorised product: ", productName, " attempted structure change");
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
            } else {
                LoggingUtil.logInfo(logger, "Authorised request to change product structure for product: ", productName);
                ProductStructureResponse psr = new ProductStructureResponse();
                try {
                    productStructureService.replaceProductStructure(product, productStructureRequest.getProductStructure());
                    psr.setSuccess(Boolean.TRUE);
                } catch (Exception e) {
                    LoggingUtil.logError(logger, e, "Unexpected Exception");
                    psr.setSuccess(Boolean.FALSE);
                }
                return psr;
            }
        }

        ProductStructureResponse psr = new ProductStructureResponse();
        psr.setSuccess(Boolean.FALSE);
        return psr;
    }
}
