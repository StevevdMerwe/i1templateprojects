package com.omussd.generic.parser;

import com.omussd.common.log.LoggingUtil;
import com.omussd.generic.constant.OptionGroupComponentTypeConstant;
import com.omussd.generic.constant.PropertyConstant;
import com.omussd.generic.constant.ScreenTypeConstant;
import com.omussd.generic.db.entity.Option;
import com.omussd.generic.db.entity.OptionGroupComponent;
import com.omussd.generic.db.entity.OptionOptionGroupComponent;
import com.omussd.generic.db.entity.Screen;
import com.omussd.generic.db.entity.ScreenComponent;
import com.omussd.generic.db.entity.ScreenScreenComponent;
import com.omussd.generic.db.entity.Session;
import com.omussd.generic.db.entity.TextComponent;
import com.omussd.generic.service.PropertyService;
import com.omussd.generic.service.ScreenComponentService;
import com.omussd.generic.session.SessionData;
import com.omussd.generic.session.SessionDataCache;
import com.omussd.generic.session.bean.DynamicOption;
import com.omussd.generic.session.bean.DynamicOptionGroup;
import com.omussd.ussdintegration.request.constant.ResponseState;
import com.omussd.ussdintegration.request.constant.ResponseType;
import com.omussd.ussdintegration.response.USSDAppResponse;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A Parser that handles converting objects into a Generic Response
 *
 * @author XY40428
 */
@Component
public class ScreenParser {

    private static final Logger logger = LoggerFactory.getLogger(ScreenParser.class);

    private static final String VARIABLE_START = "${";
    private static final String VARIABLE_END = "}";

    @Autowired
    private ScreenComponentService screenComponentService;
    @Autowired
    private PropertyService propertyService;

    /**
     * Creates a Generic USSDAppResponse from the given Screen and Session. The
     * prompt within the response will conform to normal USSD standards.
     *
     * @param session The Session that instigated the request and to who this
     * USSDAppResponse is destined. Used to gather information such as text
     * variables
     * @param screen The Screen to build the response from
     * @return The USSDAppResponse
     */
    public USSDAppResponse getAppResponseFromScreen(Session session, Screen screen) {
        com.omussd.generic.db.entity.ScreenType screenType = screen.getScreenTypeId();

        USSDAppResponse response = new USSDAppResponse();
        response.setResponseType(ResponseType.String);

        switch (ScreenTypeConstant.getScreenTypeByCode(screenType.getCode())) {
            case ACTION:
                response.setState(ResponseState.NextState);
                break;
            case END:
            default:
                response.setState(ResponseState.End);
                break;
        }

        String prompt = getPromptFromScreenComponents(session, screenComponentService
                .findScreenScreenComponentByScreenIdOrderBySequence(screen));
        response.setPrompt(prompt);

        return response;
    }

    private String getPromptFromScreenComponents(Session session,
            List<ScreenScreenComponent> screenScreenComponentsCollection) {
        SessionData sessionData = null;
        if (session != null) {
            sessionData = SessionDataCache.INSTANCE.getSessionData(session.getMsisdn());
        }
        StringBuilder prompt = new StringBuilder();

        for (ScreenScreenComponent ssc : screenScreenComponentsCollection) {
            ScreenComponent sc = ssc.getScreenComponentId();
            if (sc instanceof TextComponent) {
                prompt.append(getPromptForTextComponent((TextComponent) sc, sessionData));
            } else if ((sc instanceof OptionGroupComponent) && sessionData != null) {
                prompt.append(getPromptForOptionGroupComponent(sessionData, (OptionGroupComponent) sc));
            }
            prompt.append("\n\n");
        }

        return prompt.toString().trim();
    }

    private String getPromptForTextComponent(TextComponent textComponent, SessionData sessionData) {
        String text = textComponent.getText().replaceAll("<br>", "\n").trim();
        return replaceVariables(text, sessionData);
    }

    private String getPromptForOptionGroupComponent(SessionData sessionData, OptionGroupComponent ogc) {
        String deliminator = propertyService.findValueByName(PropertyConstant.OPTION_PROMPT_DELIMINATOR.toString());
        if (OptionGroupComponentTypeConstant.STATIC.getType().equals(ogc.getOptionGroupComponentTypeId().getType())) {
            StringBuilder sb = new StringBuilder();

            List<OptionOptionGroupComponent> oogcs = screenComponentService
                    .findOptionOptionGroupComponentsByOptionGroupComponent(ogc);
            for (OptionOptionGroupComponent oogc : oogcs) {
                int number = oogc.getOptionNumber();
                Option option = oogc.getOptionId();
                String text = replaceVariables(option.getText(), sessionData);
                sb.append(number);
                sb.append(deliminator);
                sb.append(text);
                sb.append("\n");
            }
            return sb.toString().trim();
        } else {
            List<DynamicOptionGroup> dogs = sessionData.getDynamicOptionGroups();
            StringBuilder sb = new StringBuilder();
            for (DynamicOptionGroup dog : dogs) {
                if (ogc.getName().equals(dog.getName())) {
                    sb.append(getPromptForDynamicOptionGroupComponent(sessionData, dog, deliminator));
                    sb.append("\n");
                }
            }
            return sb.toString().trim();
        }
    }

    private String getPromptForDynamicOptionGroupComponent(SessionData sessionData, DynamicOptionGroup dog, String deliminator) {
        StringBuilder sb = new StringBuilder();
        List<DynamicOption> options = dog.getDynamicOptions();
        for (DynamicOption option : options) {
            int number = option.getNumber();
            String text = replaceVariables(option.getText(), sessionData);
            sb.append(number);
            sb.append(deliminator);
            sb.append(text);
            sb.append("\n");
        }
        return sb.toString();
    }

    private static String replaceVariables(String text, SessionData sessionData) {
        if (sessionData == null) {
            return text;
        }

        Map<String, String> variables = sessionData.getTextVariables();
        if (!(text.contains(VARIABLE_START) && text.contains(VARIABLE_END))) {
            return text;
        }
        int currentIdx = 0;
        int toIdx = 0;
        while (currentIdx != -1) {
            currentIdx = text.indexOf(VARIABLE_START, currentIdx + 1);
            toIdx = text.indexOf(VARIABLE_END, toIdx);
            if (currentIdx == -1 || toIdx == -1) {
                break;
            } else {
                //find what is between the indexes
                String variable = text.substring(currentIdx,
                        toIdx + VARIABLE_END.length());
                String variableWord = variable.substring(VARIABLE_START.length(),
                        variable.length() - (VARIABLE_END.length()));
                String replacement = variables.get(variableWord);
                if (replacement != null) {
                    text = text.replace(variable, replacement);
                } else {
                    LoggingUtil.logWarn(logger, "text variable's value is missing: ", variable);
                    currentIdx++; //bypass this variable
                    toIdx++;
                }
            }
        }

        return text;
    }
}
