package com.omussd.generic.exception;

import com.omussd.product.intent.IntentResponseType;

/**
 *
 * @author XY40428
 */
public class ProductFailureException extends Exception {
    private final IntentResponseType intentResponseType;

    public ProductFailureException(IntentResponseType intentResponseType, String message) {
        super(message);
        this.intentResponseType = intentResponseType;
    }

    public IntentResponseType getIntentResponseType() {
        return intentResponseType;
    }
}
