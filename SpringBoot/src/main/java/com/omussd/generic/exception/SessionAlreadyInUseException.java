/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omussd.generic.exception;

/**
 *
 * @author XY40428
 */
public class SessionAlreadyInUseException extends Exception {

    public SessionAlreadyInUseException() {
    }

    public SessionAlreadyInUseException(String message, Throwable cause) {
        super(message, cause);
    }

    public SessionAlreadyInUseException(Throwable cause) {
        super(cause);
    }
}
