# **==============TEMPLATE==============** #

## **INTRO:** ##

* Project type: Maven web project
* Web server: WAS Liberty
* Database: MySQL
* Database name: templatedb
* Database script management: Liquibase

## **HOW TO RUN/INSTALL:** ##

This project has been automated to build,deploy and run by executing the following scripts(MAVEN):

*Yes... it does download a was Liberty server for you as well :)

* mvn clean
* mvn liberty:install-server (Downloads and install Liberty in the <target> directory)
* mvn liberty:create-server (Creates a new instance of the server)
* mvn liberty:install-feature (Installs the features defined in the POM - takes a long time...)
* mvn liberty:deploy (Deploys the war file - Some quirks with this as it may say it fails but still works)
* mvn liberty:run-server (Runs the server in interactive mode)

## **Cool Maven scripts:** ##

mvn liquibase:generateChangeLog (this will generate liquibase scripts from your current connected db)

## **EXPOSED RESTFUL ENDPOINTS:** ##

http://***IP***:***PORT***/templateProject/rest/user/createUserr (GET)


## **CREATORS:** ##

This is a repository created by Dante van der Merwe and Steven Van der Merwe