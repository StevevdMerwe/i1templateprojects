/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.template.user;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.template.user.User;
import za.co.template.user.UserService;
import za.co.template.util.LoggingUtil;

/**
 *
 * @author Dante
 */
@Path("/user")
public class UserResource {

	@Inject
	UserService userService;

	public static final String className = UserService.class.getSimpleName();
	final static Logger logger = LogManager.getLogger(className);
	
	
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@POST
	@Path("/login")
	public Response login(User u) {
		LoggingUtil.methodEnter(logger, className,"createUser()","createUser Entered: " + u.toString());
		User n = userService.login(u);
		UserDto udto = new UserDto(n.getUsername());
		return Response.ok(udto).build();
	}
}
