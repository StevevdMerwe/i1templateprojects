/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.template.user;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import za.co.template.util.LoggingUtil;

/**
 *
 * @author Dante
 */
@Stateless
public class UserService {
	@PersistenceContext
	private EntityManager em;

	public static final String className = UserService.class.getSimpleName();
	final static Logger logger = LogManager.getLogger(className);

	public User login(User u) {
		LoggingUtil.methodEnter(logger, className,"createUser()","createUser Entered: " + u.toString());
		User result = new User();
		try {
			TypedQuery<User> q =
					em.createQuery("SELECT u FROM User u WHERE u.username = :value", User.class)
							.setParameter("value", u.getUsername());
			result = q.getSingleResult();

		} catch (PersistenceException pe) {
			LoggingUtil.error(logger, className + ": Persistence error: " + pe);
		} catch (Exception e) {
			LoggingUtil.error(logger, className + ": Persistence error: " + e);
		}
		LoggingUtil.methodExit(logger, className , ": createUser()", "createUser Exited");
		return result;
	}
}
