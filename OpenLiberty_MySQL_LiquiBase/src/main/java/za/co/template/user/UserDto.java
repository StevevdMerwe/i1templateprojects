package za.co.template.user;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Dante on 3/23/2017.
 */
@XmlRootElement
public class UserDto {

    private String username;

    public UserDto() {
    }

    public UserDto(String username) {
        this.username = username;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                '}';
    }
}
