package za.co.template.util;


import org.apache.logging.log4j.Logger;

public class LoggingUtil {

    public static void methodEnter(Logger logger, String className, String methodName, String msg) {
        logger.trace("class{}, method ={}, msg{}", className, methodName, msg);
    }

    public static void methodEnter(Logger logger, String className, String methodName, String msg, String data) {
        logger.trace("class{}, method ={}, msg{}, obj{}", className, methodName, msg, data);
    }

    public static void methodExit(Logger logger, String className, String methodName, String msg) {
        logger.trace("class{}, method ={}, msg{}", className, methodName, msg);
    }

    public static void methodExit(Logger logger, String methodName, String className, String msg, String resultSet) {
        logger.trace("class{}, method ={}, msg{}, obj{}", className, methodName, msg, resultSet);
    }

    public static void trace(Logger logger, String msg) {
        logger.trace(msg);
    }

    public static void info(Logger logger, String msg) {
        logger.info(msg);
    }

    public static void error(Logger logger, String msg) {
        logger.error(msg);
    }
}
