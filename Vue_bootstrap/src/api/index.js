/* eslint-disable no-console */

import axios from "axios";

export default {
  request(method, uri, data = null) {
    var serverURI = "http://127.0.0.1:4567";

    if (!method) {
      console.error("API function call requires method argument");
      return;
    }

    if (!uri) {
      console.error("API function call requires uri argument");
      return;
    }

    var url = serverURI + uri;
    return axios({ method, url, data });
  }
};
