const dashboard = [
  {
    type: "warning",
    icon: "ti-server",
    title: "Events",
    value: "14M",
    footerText: "Updated now",
    footerIcon: "ti-reload"
  },
  {
    type: "success",
    icon: "ti-wallet",
    title: "Response times",
    value: "535ms",
    footerText: "Last day",
    footerIcon: "ti-calendar"
  },
  {
    type: "danger",
    icon: "ti-pulse",
    title: "Errors",
    value: "3",
    footerText: "In the last hour",
    footerIcon: "ti-timer"
  },
  {
    type: "info",
    icon: "ti-pie-chart",
    title: "Users",
    value: "+4",
    footerText: "Updated now",
    footerIcon: "ti-reload"
  }
];

export { dashboard };
