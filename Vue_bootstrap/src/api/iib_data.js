export const iib_data = [
  {
    timeperiod: "2018-07-11T22:00:00Z",
    num: 3525
  },
  {
    timeperiod: "2018-07-10T22:00:00Z",
    num: 14213
  },
  {
    timeperiod: "2018-07-09T22:00:00Z",
    num: 5311
  },
  {
    timeperiod: "2018-07-08T22:00:00Z",
    num: 4385
  },
  {
    timeperiod: "2018-07-07T22:00:00Z",
    num: 1033
  },
  {
    timeperiod: "2018-07-06T22:00:00Z",
    num: 1643
  },
  {
    timeperiod: "2018-07-05T22:00:00Z",
    num: 4202
  },
  {
    timeperiod: "2018-07-04T22:00:00Z",
    num: 21743
  },
  {
    timeperiod: "2018-07-03T22:00:00Z",
    num: 23090
  },
  {
    timeperiod: "2018-07-02T22:00:00Z",
    num: 3591
  },
  {
    timeperiod: "2018-07-01T22:00:00Z",
    num: 19752
  },
  {
    timeperiod: "2018-06-30T22:00:00Z",
    num: 3892
  },
  {
    timeperiod: "2018-06-29T22:00:00Z",
    num: 1934
  },
  {
    timeperiod: "2018-06-28T22:00:00Z",
    num: 4290
  },
  {
    timeperiod: "2018-06-27T22:00:00Z",
    num: 20780
  },
  {
    timeperiod: "2018-06-26T22:00:00Z",
    num: 5773
  },
  {
    timeperiod: "2018-06-25T22:00:00Z",
    num: 3923
  },
  {
    timeperiod: "2018-06-24T22:00:00Z",
    num: 19306
  },
  {
    timeperiod: "2018-06-23T22:00:00Z",
    num: 609
  },
  {
    timeperiod: "2018-06-22T22:00:00Z",
    num: 2902
  },
  {
    timeperiod: "2018-06-21T22:00:00Z",
    num: 4736
  },
  {
    timeperiod: "2018-06-20T22:00:00Z",
    num: 7284
  },
  {
    timeperiod: "2018-06-19T22:00:00Z",
    num: 22975
  }
];
