const ftp_events = [
  {
    timeperiod: "2018-07-11T22:00:00Z",
    num: 120332
  },
  {
    timeperiod: "2018-07-10T22:00:00Z",
    num: 310380
  },
  {
    timeperiod: "2018-07-09T22:00:00Z",
    num: 310887
  },
  {
    timeperiod: "2018-07-08T22:00:00Z",
    num: 105835
  },
  {
    timeperiod: "2018-07-07T22:00:00Z",
    num: 752
  },
  {
    timeperiod: "2018-07-06T22:00:00Z",
    num: 206500
  },
  {
    timeperiod: "2018-07-05T22:00:00Z",
    num: 309949
  },
  {
    timeperiod: "2018-07-04T22:00:00Z",
    num: 310324
  },
  {
    timeperiod: "2018-07-03T22:00:00Z",
    num: 309849
  },
  {
    timeperiod: "2018-07-02T22:00:00Z",
    num: 310882
  },
  {
    timeperiod: "2018-07-01T22:00:00Z",
    num: 105259
  },
  {
    timeperiod: "2018-06-30T22:00:00Z",
    num: 298
  },
  {
    timeperiod: "2018-06-29T22:00:00Z",
    num: 206341
  },
  {
    timeperiod: "2018-06-28T22:00:00Z",
    num: 310534
  },
  {
    timeperiod: "2018-06-27T22:00:00Z",
    num: 310458
  },
  {
    timeperiod: "2018-06-26T22:00:00Z",
    num: 310931
  },
  {
    timeperiod: "2018-06-25T22:00:00Z",
    num: 310676
  },
  {
    timeperiod: "2018-06-24T22:00:00Z",
    num: 105019
  },
  {
    timeperiod: "2018-06-23T22:00:00Z",
    num: 362
  },
  {
    timeperiod: "2018-06-22T22:00:00Z",
    num: 1774
  },
  {
    timeperiod: "2018-06-21T22:00:00Z",
    num: 11053
  },
  {
    timeperiod: "2018-06-20T22:00:00Z",
    num: 12019
  },
  {
    timeperiod: "2018-06-19T22:00:00Z",
    num: 1438
  },
  {
    timeperiod: "2018-06-18T22:00:00Z",
    num: 1090
  }
];

export { ftp_events };
