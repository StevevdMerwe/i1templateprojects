const stages = ["on-hold", "in-progress", "needs-review", "approved"];
const blocks = [
  {
    id: 1,
    status: "on-hold",
    title: "Testing"
  },
  {
    id: 2,
    status: "on-hold",
    title: "Steve lorem ipsum dasd  sada this work ???"
  },
  {
    id: 3,
    status: "in-progress",
    title: "Work"
  }
];

export { stages, blocks };
