import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import Overview from "@/pages/Overview.vue";
import Events from "@/pages/Events.vue";
import Tracking from "@/pages/Tracking.vue";
import Administer from "@/pages/Administer.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      },
      {
        path: "stats",
        name: "stats",
        component: UserProfile
      },
      {
        path: "overview",
        name: "overview",
        component: Overview
      },
      {
        path: "events",
        name: "events",
        component: Events
      },
      {
        path: "admin",
        name: "admin",
        component: Administer
      },
      {
        path: "tracking",
        name: "tracking",
        component: Tracking
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
