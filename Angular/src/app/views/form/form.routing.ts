import {Routes} from '@angular/router';

import {AppFormComponent} from './app-form/app-form.component';


export const FormRoutes: Routes = [
  {
    path: '',
    component: AppFormComponent,
    data: {title: 'Forms'}
  }
];
