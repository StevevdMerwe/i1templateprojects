import {Routes} from '@angular/router';


import {AppChartComponent} from './app-chart/app-chart.component';


export const ChartRoutes: Routes = [

  {
    path: '',
    component: AppChartComponent,
    data: {title: 'Charts'}
  }
];
