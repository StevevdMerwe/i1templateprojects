import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MatInputModule} from '@angular/material';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {AppBlankComponent} from './app-blank/app-blank.component';

import {OthersRoutes} from "./others.routing";

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    NgxDatatableModule,
    RouterModule.forChild(OthersRoutes)
  ],
  declarations: [AppBlankComponent]
})
export class OthersModule {
}
